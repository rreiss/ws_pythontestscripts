#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 7/25/17

import json

import requests

from Utils.util_dbTools import dbTools
from Utils.util_udfTools import udfTools


#name:  sampleSubmissionWST  -  sample metadata submission  Web Service Test, test Post command  to create new samples
# for now: this code will submit only one sample at a time
# per requirements doc: https://docs.google.com/document/d/176U28KIaaLP7YwMEUeE0mGBVnZXMj5FqxTct3ZZlBQ0/edit#heading=h.xyn1ei2ebwtm

#1.  that a valid status is returned by WS request and new sample is created and the new record is populated with passed attributes
#2.  Create and set same batch id, for all the samples in a submission,  which will be the uss.dt_sample.sample_id of the first sample in the submission batch(similar to sow item submission id)
#3.  update the samples in PLUSS with the request information
#4. create the corresponding LIMS samples as submitted samples, follow rules in requirements spec


class  sampleSubmissionWST():

    def __init__(self):
        self.printOn = True

        self.errorCnt = 0
        #these are all the attributes that can be updated on put command. Use this or subset
        self.submissionAttributes = {   "submitted-by-cid": 13,
                                        "samples":
                                            [
                                                    {
                                                        #"sample-id": 293514,
                                                        #"sample-name": "sample293514",
                                                        "bio-sample-id": 1,
                                                        "risk-group-level": 1,
                                                        "pathogenicity": "something",
                                                        "collaborator-concentration-ngul": 100.29,
                                                        "collaborator-volume-ul": 54.78,
                                                        "absorbance-260-280": .63,
                                                        "absorbance-260-230": 0.5887899211,
                                                        #"tube-plate-label": "plate293514",
                                                        #"plate-location": "A2",
                                                        "sample-format": "Aqueous",
                                                        "storage-solution": "Water",
                                                        "shipment-storage-comments": "no comments",
                                                        "dnase-treated": "Y",
                                                        "starting-culture-axenic-strain-pure": "Y",
                                                        "purity-evidence": "qPCR",
                                                        "known-or-contaminating-organisms": "no known contamination",
                                                        "small-subunit-rna-sequence": "ACGTACGTCCTAATCC",
                                                        "spacer-sequence": "GGTACTTGCAACGTGATACGATCGAGA",
                                                        #?"vector-name": "pbr32",
                                                        "biosafety-material-category": "Bacteria",
                                                        "sample-isolation-method": "ethanol extraction",
                                                        "sample-collection-date": "2010-02-27",
                                                        "latitude-of-sample-collection": 23.89,
                                                        "longitude-of-sample-collection": 32.67,
                                                        "altitude-or-depth-of-sample-collection": -23.99,
                                                        "collection-isolation-site-or-growth-conditions": "optimal temperature 20C",
                                                        "sample-isolated-from":"new England",
                                                        "group-name": 1,
                                                        #"control-type(positive/negative)"  for single cell only
                                                        #"control-organism-name"  for single cell only
                                                        #"control-organism-tax-id" for single cell only
                                                        #"pool-number" for single cell only
                                                        #"destination-container-name" for single cell only
                                                        #"destination-container-location" for internal single cell
                                                        #"internal-collaborator-sample-name"for internal single cell
                                                        #"source-container-barcode" for internal single cell

                                                    },

                                            ]
                                    }




        # Dict that maps UDF to the DB query that gets the value that populates the field
        # key = UDF name
        # value =  query string to get the value from DB, can be None
        #
        self.UDFdbQueryMap = {
            'Collaborator Concentration (ng/ul)': 'select collabor_concentration_ngul from uss.dt_sample where SAMPLE_ID =',
            'Format':                       'select cvTable.SAMPLE_FORMAT from uss.dt_sample sam '
                                            'left join uss.DT_SAMPLE_FORMAT_CV cvTable on cvTable.SAMPLE_FORMAT_ID = sam.SAMPLE_FORMAT_ID '
                                            'where SAMPLE_ID =  ',
            'Biosafety Material Category': 'select cvTable.BIOSAFETY_MATERIAL_CATEGORY from uss.dt_sample sam '
                                           'left join uss.DT_BIOSAFETY_MATERIAL_CATEG_CV cvTable '
                                           'on cvTable.BIOSAFETY_MATERIAL_CATEGORY_ID = sam.BIOSAFETY_MATERIAL_CATEGORY_ID '
                                           'where SAMPLE_ID = ',
            'Estimated Genome Size (mb)': 'select tax.GENOME_SIZE_ESTIMATED_MB from uss.dt_sequencing_project sp '
                                          'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id '
                                          'left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id '
                                          'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id '
                                          'left join uss.DT_TAXONOMY_INFO tax on tax.TAXONOMY_INFO_ID = sp.TAXONOMY_INFO_ID '
                                          'where sam.SAMPLE_ID = ',
            'Collaborator Volume (ul)': 'select collabor_volume_ul from uss.dt_sample where SAMPLE_ID = ',
            'Stop at Receipt': "SELECT CASE WHEN MAX(s.SAMPLE_ID) IS NULL THEN 'false' ELSE 'true' END "  #returns true if related sow type id is 5,6
                               "FROM  uss.dt_sample s,uss.dt_sow_item sow, USS.DT_M2M_SAMPLESOWITEM ssow  "
                               "WHERE s.SAMPLE_ID=ssow.SAMPLE_ID and ssow.SOW_ITEM_ID=sow.sow_item_id and "
                               "sow.SOW_ITEM_TYPE_ID in (5,6) and s.SAMPLE_ID=",
            'Collaborator Sample Name': 'select SAMPLE_NAME from uss.dt_sample where SAMPLE_ID = ',
            }
        # Dict that maps the ws Request attributes to the DB query that gets the value that populates the field
        # key = UDF name
        # value =  query string to get the value from DB, can be None
        #
        self.wsAttrDbQueryMap = {"sample-id":   'select SAMPLE_ID from uss.dt_sample where SAMPLE_ID = ',
                              "sample-name": 'select SAMPLE_NAME from uss.dt_sample where SAMPLE_ID = ',
                              "bio-sample-id": 'select BIO_SAMPLE_ID from uss.dt_sample where SAMPLE_ID = ',
                              "risk-group-level": 'select RISK_GROUP_LEVEL from uss.dt_sample where SAMPLE_ID = ',
                              "pathogenicity": 'select PATHOGENICITY from uss.dt_sample where SAMPLE_ID = ',
                              "collaborator-concentration-ngul": 'select COLLABOR_CONCENTRATION_NGUL from uss.dt_sample where SAMPLE_ID = ',
                              "collaborator-volume-ul": 'select COLLABOR_VOLUME_UL from uss.dt_sample where SAMPLE_ID = ',
                              "absorbance-260-280": 'select ABSORBANCE_260_280 from uss.dt_sample where SAMPLE_ID = ',
                              "absorbance-260-230": 'select ABSORBANCE_260_230 from uss.dt_sample where SAMPLE_ID = ',
                              "tube-plate-label": 'select TUBE_PLATE_LABEL from uss.dt_sample where SAMPLE_ID = ',
                              "plate-location": 'select PLATE_LOCATION from uss.dt_sample where SAMPLE_ID = ',
                              "sample-format": 'select cvTable.SAMPLE_FORMAT from uss.dt_sample sam '
                                               'left join uss.DT_SAMPLE_FORMAT_CV cvTable on cvTable.SAMPLE_FORMAT_ID = sam.SAMPLE_FORMAT_ID where SAMPLE_ID =  ',
                              "storage-solution": 'select cvTable.storage_solution from uss.dt_sample sam '
                                                  'left join uss.DT_storage_solution_CV cvTable on cvTable.storage_solution_ID = sam.storage_solution_ID where SAMPLE_ID = ',
                              "shipment-storage-comments": 'select SHIPMENT_STORAGE_COMMENTS from uss.dt_sample where SAMPLE_ID = ',
                              "dnase-treated": 'select DNA_TREATED from uss.dt_sample where SAMPLE_ID = ',
                              "starting-culture-axenic-strain-pure": 'select STARTING_CULTURE_PURE from uss.dt_sample where SAMPLE_ID = ',
                              "purity-evidence": 'select PURITY_EVIDENCE from uss.dt_sample where SAMPLE_ID = ',
                              "known-or-contaminating-organisms": 'select KNOWN_OR_CONTAM_ORGANISMS from uss.dt_sample where SAMPLE_ID = ',
                              "small-subunit-rna-sequence": 'select SMALL_SUBUNIT_RRNA_SEQ from uss.dt_sample where SAMPLE_ID = ',
                              "spacer-sequence": 'select INTERN_TRANSCR_SPACER_SEQ from uss.dt_sample where SAMPLE_ID = ',
                              # ?"vector-name": 'select VECTOR_NAME from uss.dt_sample where SAMPLE_ID = ',
                              "biosafety-material-category": 'select cvTable.BIOSAFETY_MATERIAL_CATEGORY from uss.dt_sample sam '
                                                             'left join uss.DT_BIOSAFETY_MATERIAL_CATEG_CV cvTable on cvTable.BIOSAFETY_MATERIAL_CATEGORY_ID = sam.BIOSAFETY_MATERIAL_CATEGORY_ID '
                                                             'where SAMPLE_ID = ',
                              "sample-isolation-method": 'select DNA_ISOLATION_METHOD from uss.dt_sample where SAMPLE_ID = ',
                              "sample-collection-date": 'select COLLECTION_TIME from uss.dt_sample where SAMPLE_ID = ',
                              "latitude-of-sample-collection": 'select COLLECTION_LATITUDE from uss.dt_sample where SAMPLE_ID = ',
                              "longitude-of-sample-collection": 'select COLLECTION_LONGITUDE from uss.dt_sample where SAMPLE_ID = ',
                              "altitude-or-depth-of-sample-collection":'select COLLECTION_ALTIT_DEPTH_METERS from uss.dt_sample where SAMPLE_ID = ',
                              "collection-isolation-site-or-growth-conditions": 'select COLLECTION_SITE_GROWTH from uss.dt_sample where SAMPLE_ID = ',
                              "sample-isolated-from":'select SAMPLE_ISOLATED_FROM from uss.dt_sample where SAMPLE_ID = ',
                              "group-name": 'select GROUP_NAME from uss.dt_sample where SAMPLE_ID = ',
                              "control-type(positive/negative)" : 'select CONTROL_TYPE from uss.dt_sample where SAMPLE_ID = ',
                              "control-organism-name": 'select CONTROL_ORGANISM_NAME from uss.dt_sample where SAMPLE_ID = ',
                              "control-organism-tax-id": 'select CONTROL_ORGANISM_TAX_ID from uss.dt_sample where SAMPLE_ID = ',
                              "pool-number": 'select POOL_NUMBER from uss.dt_sample where SAMPLE_ID = ',
                              "destination-container-name" : 'select DESTINATION_CONTAINER_NAME from uss.dt_sample where SAMPLE_ID = ',
                              "destination-container-location": 'select DESTINATION_CONTAINER_LOCATION from uss.dt_sample where SAMPLE_ID = ',
                              "internal-collaborator-sample-name": 'select INTERNAL_COLLABOR_SAMPLE_NAME from uss.dt_sample where SAMPLE_ID = ',
                              "source-container-barcode" : 'select CONTAINER_IDENTIFIER from uss.dt_sample where SAMPLE_ID = '
                             }

        # Dict that maps the entity type with the query necessary to get the status from the DB
        # key = entity type ('sp', 'sow', 'sample'
        # value =  query string to get the status from DB, can be None
        #


    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'claritydev1.jgi-psf.org'
    #
    def setURL(self,server):
        requestURL = 'http://' + server + '/sample-submission/'
        return requestURL

    # -------------------------------------------------------------------------------------------
    # sampleSubmissionPost
    #
    # inputs: requestURL
    def sampleSubmissionPost(self,requestURL):
        #need to check sample to make sure if it is a plate or tube, use query
        #if plate, need to add plate loc

        mydata = json.dumps(self.submissionAttributes)
        headers = {"Content-Type": "application/json", 'data':mydata}
        # Call REST API
        response = requests.post(requestURL, data=mydata, headers=headers)  #POST  request
        status = response.status_code
        if status != 200:
            print("*** Error, Unexpected Returned Status = ", status)
            self.errorCnt +=  1
            prettyJson = json.loads(response.text)
            print("WS request status = " + str(status))
            print("response:" + json.dumps(prettyJson, indent=4, sort_keys=True))
        else:
            # Print Response
            prettyJson = json.loads(response.text)
            if self.printOn:
                print (json.dumps(prettyJson, indent=4, sort_keys=True))
        return status
    # -------------------------------------------------------------------------------------------
    # getUDFs
    #
    # inputs: sampleId, server,
    # outputs: UDFs as a dict
    def getUDFs(self,sampleId,server):
        myUDFs = {}
        url = udfTools.setURLsample(sampleId,server)
        print("sample base URL = ", url)
        if url:
            sampleUrl = udfTools.connectToClarityAPIsamples(url)
            print("sample  URL = ", sampleUrl)
            if sampleUrl:
                   myUDFs = udfTools.getSampleUDFs(sampleUrl)
                   print ("udfs retrieved:")
                   print (myUDFs)


        return myUDFs


    # -------------------------------------------------------------------------------------------
    # verifySampleUDFsArePopulatedByDB
    # verify that the UDFS have the value stored in DB.
    #  input:  UDFs - the dict of UDFs for sample
    #
    # ouput:  updates errorCnt (global)

    def verifySampleUDFsArePopulatedByDB(self, myUDFs,sampleId):
        if myUDFs:
            myDB = dbTools()
            for udf, query in self.UDFdbQueryMap.items():
                print (udf)
                if (udf in myUDFs) and query:
                    newQuery = query + str(sampleId)
                    dbValue = myDB.doQuery(newQuery)
                    print ("DB value= ", dbValue)
                    if str(myUDFs[udf]) != str(dbValue):
                        print("*** UDF does not match DB !!!  UDF(" + udf + ")=" + str(myUDFs[udf]) + " !=  DB= " + str(dbValue))
                        self.errorCnt += 1
                    elif self.printOn:
                        print(udf + ':  values match DB')
        else:
            print("*** Error. UDFs not retrieved !!! ")
            self.errorCnt += 1

    # -------------------------------------------------------------------------------------------
    # verifyDBisPopulatedWithRequestedAttribues
    # verify that the new sample was created with the values of the Request attributes.
    # input:
    #         wsRequest - the request Dict from WS call
    #         sampleID - the sample ID to get data from DB
    # ouput:  updates errorCnt (global)

    def verifyDBisPopulatedWithRequestedAttribues(self, wsRequest, sampleID):
        if wsRequest:
            myDB = dbTools()
            for wsAttr, query in self.wsAttrDbQueryMap.items():
                print('----',wsAttr,'----')
                if query and wsAttr in wsRequest:
                    newQuery = query + str(sampleID)
                    dbValue = myDB.doQuery(newQuery)
                    print("     DB value= ", dbValue, " Request value= ",str(wsRequest[wsAttr]))
                    if str(wsRequest[wsAttr]) != str(dbValue):
                        roundedNumbersMatch = self.checkRoundedNumbers(wsAttr,wsRequest[wsAttr],dbValue)
                        if not roundedNumbersMatch:
                            print("***ERROR!  DB and Request do not match!! !!!  WS attribute(" + wsAttr + ")=" + str(wsRequest[wsAttr]) + " !=  DB= " + str(
                            dbValue))
                            self.errorCnt += 1
                    elif self.printOn:
                        print('    ' + wsAttr + ':  values match DB')
                else:
                    print("*** Warning. field not found in wsRequest  !!! ")
                    #self.errorCnt += 1
        else:
            print("*** Error. WSrequest is null !!! ")
            self.errorCnt += 1

    # -------------------------------------------------------------------------------------------
    # checkRoundedNumbers
    # compare two numbers after rounded up.  return true if they match
    # input:
    #         attribute - only some attributes are number fields, 1st check if this is one of them
    #         number1
    #         number2
    #
    # ouput:  boolean :  true if two numbers match after rounded

    def checkRoundedNumbers(self,attr, number1, number2):
        match = False
        print("checking rounded numbers")
        print(type(number2), type(number1))
        if attr in ['latitude-of-sample-collection','longitude-of-sample-collection','absorbance-260-280',
                    'absorbance-260-230','collaborator-concentration-ngul','collaborator-volume-ul','altitude-or-depth-of-sample-collection']:

                print("checking rounded numbers")
                print (attr,number1,number2)
                num1 = round(float(number1), 1)
                num2 = round(float(number2), 1)

                if num1 == num2:
                    print("match ", num1, num2)
                    match = True
                else:
                    print("no match", num1, num2)
                    match = False
        return match



    # -------------------------------------------------------------------------------------------
    # submitAsample
    #
    # input:  server,sampleID, samplename
    # ouput:  err - number of errors found

    def submitAsample(self, server,sampleID,sampleName):
        apiDevServer = 'clarity-dev01.jgi-psf.org'
        print("")
        print("--------start: create sample submission WST tests---------")
        url = self.setURL(server)
        if self.printOn:
            print("url = " + url)

        self.submissionAttributes['samples'][0]['sample-name'] = sampleName
        self.submissionAttributes['samples'][0]['tube-plate-label'] = 'label' + str(sampleID)
        self.submissionAttributes['samples'][0]['sample-id'] = str(sampleID)

        #find a sample that needs submitting


        #determine if it can be plate or tube, this query return 'plate' if only can be plate
        query = "SELECT CASE WHEN MAX(lcq.TUBE_TARGET_VOL_LIB_TRIAL_UL) IS NOT NULL AND  MAX(lcq.PLATE_TARGET_VOL_LIB_TRIAL_UL) IS  NOT NULL " \
                              "THEN 'tubeORplate' " \
                              "ELSE CASE WHEN MAX(lcq.TUBE_TARGET_VOL_LIB_TRIAL_UL) IS NOT NULL THEN 'tube' ELSE 'plate' " \
                               "    END " \
                        "END " \
                "FROM uss.dt_sample sam " \
                "left join uss.dt_m2m_samplesowitem m2sam on m2sam.sample_id = sam.sample_id " \
                "left join uss.dt_sow_item sow on sow.sow_item_id = m2sam.sow_item_id " \
                "left join uss.DT_M2M_LC_QUEUE_SPECS  m2mlq on  m2mlq.LIBRARY_CREATION_SPECS_ID = sow.LIBRARY_CREATION_SPECS_ID " \
                "left join uss.DT_LIBRARY_CREATION_QUEUE_CV lcq on m2mlq.LIBRARY_CREATION_QUEUE_ID = lcq.LIBRARY_CREATION_QUEUE_ID " \
                " WHERE sam.sample_id in " + str(sampleID)


        myDB = dbTools()
        tubeData = myDB.doQuery(query)

        if tubeData == 'plate':   # null returned in field TUBE_TARGET_VOL_LIB_TRIAL_UL means it is not a tube
            print ("this is plate")
            #add plate location
            self.submissionAttributes['samples'][0]['plate-location'] = "A2"
            print (self.submissionAttributes)
        else:
            print("this is a tube or either")

        if self.printOn:
            print("the response:")
        status = self.sampleSubmissionPost(url)  # call WS POST,response in dictionary form
        print (status)
        if status == 200:  #sample submitted, now verify data

            #verify database has been updated
            print("\n=====verify database has been updated======\n")
            self.verifyDBisPopulatedWithRequestedAttribues(self.submissionAttributes['samples'][0], sampleID)

            #verify udfs have been updated
            print("\n=====verify UDFs has been updated======\n")
            myUDFs = self.getUDFs(sampleID,apiDevServer) # get the udfs, return as a dict with UDF name as key, UDF value as value
            # verify that the  UDFs are populated by DB values
            self.verifySampleUDFsArePopulatedByDB(myUDFs, sampleID)

            #self.verifyDBisPopulatedWithRequestedAttribues(self.submissionAttributes['samples'][0], sampleID)

        return self.errorCnt




    # -------------------------------------------------------------------------------------------
    # doTests  - runs test created for submitting a sample, creates samplename
    #
    # input:  server,sampleID
    # ouput:  err - number of errors found

    def doTests(self, server,sampleID):
        #sampleName = 'sampleñóǹ äŝçíì 汉语/漢語  华语/華語 Huáyǔ; 中文 Zhōngwén 漢字仮名交じり文 Lech Wałęsa æøå' + str(sampleID)  # use to verify pps-4066
        sampleName = 'sample' + str(sampleID)
        self.submitAsample( server, sampleID,sampleName)
        return self.errorCnt


# -------------------------------------------------------------------------------------------
#run tests

devServer = 'claritydev1.jgi-psf.org'
#prdServer = 'clarityprd1.jgi-psf.org'
myTest = sampleSubmissionWST()
sampleID=308349

errs = myTest.doTests(devServer,sampleID)

print("")
print("---Number of Errors Found = " + str(errs) + " ---")

















