#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 7/25/17

import json

import requests

from Utils.util_dbTools import dbTools
from Utils.util_jsonInput import jsonInput


#name:  addSowWST  -  add sow Web Service Test,
# per requirements doc: https://docs.google.com/document/d/1DRIKtA7KZGBGONjaJKWe_jT8pp7LAaHI7uc8c215IHU/edit

class  addSowWST():

    def __init__(self):
        self.printOn = True
        self.myDB = dbTools()
        self.serverName = 'int'
        self.myDB.connect(self.serverName)
        self.errorCnt = 0
        #these are all the attributes that can be updated on put command. Use this or subset
    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'claritydev1.jgi-psf.org'
    #
    def setURL(self,server):
        requestURL = 'http://' + server + '/add-sow-item/'
        return requestURL

    # -------------------------------------------------------------------------------------------
    # addSowPost
    #
    # inputs: requestURL
    #         requestJson  - the json record for request
    def addSowPost(self,requestURL,requestJson,expectedStatus):
        mydata = json.dumps(requestJson)
        headers = {"Content-Type": "application/json", 'data':mydata}
        # Call REST API
        response = requests.post(requestURL, data=mydata, headers=headers)  #POST  request
        status = response.status_code
        if status != expectedStatus:
            print("*** Error, Unexpected Returned Status = ", status)
            self.errorCnt +=  1
            prettyJson = json.loads(response.text)
            print("WS request status = " + str(status))
            print("response:" + json.dumps(prettyJson, indent=4, sort_keys=True))
        else:
            # Print Response
            prettyJson = json.loads(response.text)
            if self.printOn:
                print (json.dumps(prettyJson, indent=4, sort_keys=True))
            return prettyJson

    def chooseScenarioExistSample(self):
        print('Choose (Existing Sample) Scenario to verify:', "\n"
              '110. inputs: sample id, (any purpose), linked sow item present', "\n"
              '115. inputs: sample id, (any purpose), linked sow item NOT present', "\n"
              )
        scenarioNum = input('Please enter scenario number: ')
        getInput = jsonInput()
        if scenarioNum=='110':
            submissionJson= getInput.addSowToExistSamJson  #addSowToExistSamScenario110
        elif scenarioNum=='115':
            submissionJson = getInput.addSowToExistSamJson #addSowToExistSamScenario115
        else:
            submissionJson = getInput.addSowToExistSamJson
        return submissionJson

    def doTestFromMenu(self,server):
        url = self.setURL(server)
        expectedStatus = 201
        print('add sow WS tests:', "\n"
              '1. add a sow to a new sample', "\n",
              '2. add a sow to an existing sample', "\n",
              '3. add a sow to existing library', "\n",
              '4. add a sow to an existing plate', "\n",
              '6. add a sow  to single cell - todo',"\n",
              '7. add an onboarding sow  - todo', "\n",
              '8. scenarios -add sow to new sample = to do', "\n",
              '9. scenarios -add sow to existing sample',"\n",
              '10. scenarios -add sow to existing library - todo', "\n",
              '11. regression PPS-4201 - add sow with inactive runmode', "\n",
              '12. regression PPS-3983 - add sow with exome', "\n"

              )
        testnum = input('Please enter test number: ')
        getInput = jsonInput()
        if testnum == '1':
            submissionJson = getInput.addSowToNewSam
            print("sow added to new sample")
        elif testnum == '2':
            submissionJson = getInput.addSowToExistSamJson
            print("sow added to existing sample")
        elif testnum == '3':

            self.myDB.doUpate("update uss.dt_sow_item set current_status_id =6, status_comments = 'for auto regression test, set to complete' ,status_date = sysdate where CLARITY_ARTIFACT_LIMSID = '2-133765'")
            submissionJson = getInput.addSowToLib
            print("sow added to existing library")
        elif testnum == '4':
            submissionJson = getInput.submissionAttrs4plate
            print("sow added to plate of samples")
        elif testnum == '6':
            submissionJson = getInput.submissionAttrs4singleCellSam
            print("sow added: to single cell sample")
        elif testnum == '9':
            submissionJson = self.chooseScenarioExistSample()
        elif testnum == '11':
            print("\n regression PPS-4201 expect sow add to fail - using inactive runmode")
            submissionJson = getInput.regression_PPS4021
            expectedStatus = 422
        elif testnum == '12':
            submissionJson = getInput.regression_PPS3983
            submissionJson["exome-capture-probe-set"] =  "140604_Maize_SK_EZ_HX3"
            print("sow added: regression PPS-3982  with exome set = 140604_Maize_SK_EZ_HX3")
            wsResp = self.addSowPost(url, submissionJson,201)  # call WS POST,response in dictionary form
            if self.printOn:
                print("the response:")
                print(wsResp)
            submissionJson["exome-capture-probe-set"] = "120911_Switchgrass_GLBRC_R_EZ_HX1"
            print("sow added: regression PPS-3982  with exome set = 120911_Switchgrass_GLBRC_R_EZ_HX1")
        else:
            submissionJson = getInput.addSowToNewSam
            print(" default - sow added to new sample")

        wsResp = self.addSowPost(url, submissionJson,expectedStatus)  # call WS POST,response in dictionary form
        if self.printOn:
            print("the response:")
            print(wsResp)
        expectedStatus = 201  # reset, most cases we expect the action to succeed


        return submissionJson

    # -------------------------------------------------------------------------------------------
    def doAllTests(self, server):
        myJson = jsonInput()
        url = self.setURL(server)
        print("------------------------------")
        print("Test- sow added to new sample")
        print("------------------------------")
        submissionJson = myJson.addSowToNewSam
        wsResp = self.addSowPost(url, submissionJson, 201)  # call WS POST,response in dictionary form
        if self.printOn:
            print("the response:")
            print(wsResp)
        print("------------------------------")




        print("Test - sow added to existing sample")
        print("------------------------------")
        submissionJson = myJson.addSowToExistSamJson
        wsResp = self.addSowPost(url, submissionJson, 201)  # call WS POST,response in dictionary form
        if self.printOn:
            print("the response:")
            print(wsResp)

        print("------------------------------")
        print("Test- sow added to existing library")
        print("------------------------------")
        #sow status must be complete (or ..) to add asow to library

        submissionJson = myJson.addSowToLib
        clarity_artifact_lims = submissionJson['library-stock-id'] #grab the lims id from the request
        statement = "update uss.dt_sow_item set current_status_id =6, " \
                    "status_comments = 'for auto regression test, " \
                    "set to complete, rjr',status_date = sysdate where CLARITY_ARTIFACT_LIMSID = " + "'" + clarity_artifact_lims + "'"
        self.myDB.doUpate(statement)
        wsResp = self.addSowPost(url, submissionJson, 201)  # call WS POST,response in dictionary form
        if self.printOn:
            print("the response:")
            print(wsResp)

        print("------------------------------")
        print("Test- sow added to plate of samples")
        print("------------------------------")
        submissionJson = myJson.submissionAttrs4plate
        wsResp = self.addSowPost(url, submissionJson, 201)  # call WS POST,response in dictionary form
        if self.printOn:
            print("the response:")
            print(wsResp)

        print("------------------------------")
        print("Test- sow added: to single cell sample")
        print("------------------------------")
        submissionJson = myJson.submissionAttrs4singleCellSam
        wsResp = self.addSowPost(url, submissionJson, 201)  # call WS POST,response in dictionary form
        if self.printOn:
            print("the response:")
            print(wsResp)

        print("regression PPS-4021 expect sow add to fail - using inactive runmode")
        submissionJson = myJson.regression_PPS4021

        wsResp = self.addSowPost(url, submissionJson, 422)  # call WS POST,response in dictionary form
        if self.printOn:
            print("the response:")
            print(wsResp)

        print("------------------------------")
        print("Test - sow added: regression PPS-3982  with exome set = 140604_Maize_SK_EZ_HX3")
        print("------------------------------")
        submissionJson = myJson.regression_PPS3983
        submissionJson["exome-capture-probe-set"] = "140604_Maize_SK_EZ_HX3"
        wsResp = self.addSowPost(url, submissionJson, 201)  # call WS POST,response in dictionary form
        if self.printOn:
            print("the response:")
            print(wsResp)

        print("------------------------------")
        print("Test - regression PPS-3982 now add another with exome set = 120911_Switchgrass_GLBRC_R_EZ_HX1")
        print("------------------------------")
        submissionJson["exome-capture-probe-set"] = "120911_Switchgrass_GLBRC_R_EZ_HX1"
        wsResp = self.addSowPost(url, submissionJson, 201)  # call WS POST,response in dictionary form
        if self.printOn:
            print("the response:")
            print(wsResp)

        return self.errorCnt


    # -------------------------------------------------------------------------------------------
    # runTest
    #
    def runTest(self):

        # -------------------------------------------------------------------------------------------
        devServer = 'claritydev1.jgi-psf.org'
        # prdServer = 'clarityprd1.jgi-psf.org'
        apiDevServer = 'clarity-dev01.jgi-psf.org'
        intServer = 'clarityint1.jgi-psf.org'
        apiIntServer = 'clarity-int01.jgi-psf.org'

        # sp=1019606       #1018639 this part of a plate
        # set up the test with desired  server
        urlx = self.setURL(intServer)
        print(urlx)
        # submissionJson = myTest.doTestFromMenu(devServer)
        errs = self.doAllTests(intServer)
        print(" ")
        print("---Number of Errors Found = " + str(errs) + " ---")


#--------------------------------

#uncomment to run from this file

myTest = addSowWST()
myTest.runTest()





