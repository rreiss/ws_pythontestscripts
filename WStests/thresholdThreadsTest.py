#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created  9/26/18

# run this with 1000-3000 loops to try to reach the 4096 thread threshold
# to check on the linux server use this command
#   >> ps -eFL  | grep '^60021' | sed 's/\s\+/ /g' | cut -d' ' -f 13- | sort | uniq -c
# add all numbers in 1st column to get total threads

#note to tester.  Please make the following modifications when running this test:
#set testServer to  one of the defined intServer, prdServer, etc
#for the server that you are testing, make sure that the SP lists, lib and pru are valid.  Choose one(index value) to use as test sp
# you can also modify calls to webservices, with specific sps

from WStests.getSowItemsWST import sowItemsWST
from WStests.getSequencingProjectsWST import getSequencingProjectsWST
from WStests.getSampleWST import getSampleWST
from WStests.rqcReworkWST import rqcReworkWST
from WStests.putSequencingProjectsWST import putSequencingProjectsWST
import copy


# -------------------------------------------------------------------------------------------
# please  update data in the following dictionaries to reflect current info (url, sp, lib, pru) for each server
# run tests
devServer = {
    "url": 'claritydev1.jgi-psf.org',
    "sp": [1063593,1019606,1018639],
    "pru": "xxx",     #used for RQCrework calls
    "lib": "xxx"       #used for RQCrework calls
}
intServer = {
    "url": 'clarityint1.jgi-psf.org',
    "sp": [1063593,
           1019606,
           1018639,
           1207398,     #1207398=93 samples and 1288 sow items
           1208084]  ,   #1208084 has 1 sow item
    "pru": "2-3325372",
    "lib": "CSYZY"

}
prdServer = {
    "url": 'clarityprd1.jgi-psf.org',
    "sp": [1063593,1019606,1018639],
    "pru": "xxx",
    "lib": "xxx"
}
zuulServer = {
    "url": 'clarity-int01.jgi-psf.org:8181',
    "sp": [1063593,1019606,1018639],
    "pru": "xxx",
    "lib": "xxx"
}
virtualServer = {
    "url": 'clarity-prd01.jgi.doe.gov:57867',   # sequencing projects
    "url2": 'clarity-prd01.jgi.doe.gov:60966',   # for  sow items WS , ie. add sow item
    "sp": [1233256,1233243,1233099],
    "pru": "xxx",
    "lib": "xxx"
}




# tester:  you must set up the testServer by copying the correct data.
# 1. set the testServer to copy the server you are testing
# 2. choose the sp to use from the list in the servers dictionary.

testServer = copy.deepcopy(virtualServer)  #choose the server to use
server = testServer["url"]
sp = testServer["sp"][0]   #choose the sp to use.
lib = testServer["lib"]
pru = testServer["pru"]


#all the following tests are running on clarity-int01
 #initiate the objects
mySPgetTest = getSequencingProjectsWST()
mySPputTest = putSequencingProjectsWST()
mySowgetTest = sowItemsWST()
mySampleTest = getSampleWST()
myRQCreworkTest = rqcReworkWST()

#initialize error counts
spGetErrs=0
sowGetErrs = 0
sampleGetErrs = 0
spPutErrs= 0
rqcReworkErrs= 0
totalErrs = 0
loopErrs = 0
# -------------------------------------------------------------------------
for element in testServer["sp"]:  # for each SP run tests in loop
    print (element)
    sp = element
    input("wait")
    for x in range(2):  # will loop this many times
        spGetErrs = mySPgetTest.runTest(server, sp)
        sowGetErrs = mySowgetTest.runTest(server,sp)
        sampleGetErrs = mySampleTest.runTest(server,sp)
        spPutErrs = mySPputTest.runTest(server,sp)
        #rqcReworkErrs = myRQCreworkTest.runRQC_Rework_Action_Test(server, lib, pru)
        print("loop: ", x)
        loopErrs = spGetErrs + sowGetErrs + sampleGetErrs + spPutErrs + rqcReworkErrs
        print ("sp Get errors =  ", spGetErrs)
        print("sow Get errors =  ", sowGetErrs)
        print("sample Get errors =  ", sampleGetErrs)
        print("sp put errors =  ", spPutErrs)
        print("RQC rework errors =  ", rqcReworkErrs)
totalErrs = totalErrs + loopErrs
if (totalErrs>0):
    print("stopped in loop :", x)
    exit()


print ("Done!")
print("Total errors = ", totalErrs)







