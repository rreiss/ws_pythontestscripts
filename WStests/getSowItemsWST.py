#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'

#import json
from Utils.wsTools import wsTools

#name:  sowItemsWST  - sow Items Web Service Test
#code verifies info on requirements doc:  https://docs.google.com/document/d/1Gc8cR8NLU70gHhfKFGvoWspnJwdM0lCnoXzELRtsrvw/edit
#this program, runs the ws and verifies that the attributes returned in response are the same as documented in
#requirements doc.  Also checks that return status is correct when invalid request is given
#input:  sequencing project ID

class sowItemsWST():
    def __init__(self):
        self.errorCnt = 0
        self.expectedAttributes = {'sow-item-id': 0,
                                   'sow-item-type': 0,
                                   'sow-item-number': 0,
                                   'current-status': 0,
                                   'status-date': 0,
                                   'target-logical-amount': 0,
                                   'completed-logical-amount': 0,
                                   'total-logical-amount-completed': 0,
                                   'logical-amount-units': 0,
                                   'degree-of-pooling': 0,
                                   'run-mode': 0,
                                   'sequencer-model': 0,
                                   'platform': 0,
                                   'sample-id': 0,
                                   'sample-name': 0,
                                   'gls-sample-id': 0,
                                   'sequencing-project-id': 0,
                                   'sequencing-product': 0,
                                   'material-type': 0,
                                   'target-insert-size-kb': 0,
                                   'target-template-size-bp': 0,
                                   'target-fragment-size-bp': 0,
                                   'tight-insert': 0,
                                   'overlapping-reads': 0,
                                   'rrna-depletion': 0,
                                   'poly-a-selection': 0,
                                   'itag-primer-set': 0,
                                   'library-protocol': 0,
                                   'exome-capture-probe-set': 0,
                                   'sm-instructions': 0,
                                   'lc-instructions': 0,
                                   'sq-instructions': 0,
                                   'library-creation-specs': 0, ''
                                   'library-codes': 0}
    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          sp  -  the sequencing project ID under test
    # i.e 'http://claritydev1.jgi-psf.org/sequencing-projects/' + str(spID) + '/sow-items'
    def setURL(self, sp, server):
        requestURL = 'http://' + server + '/sequencing-projects/' + str(sp) + '/sow-items'
        print("url = " + requestURL)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # verifyExpectedAttributes
    # verify that all the expected attributes are returned in response of WS call
    # input:  json string from WS call
    # ouput:  updates errorCnt (global)
    def verifyExpectedAttributes(self, jsonstr):

        for key, retrievedList in jsonstr.items():  # for each sow item, check that  all attributes that are required (per contract)
                                                    #  are in WS response
            if key == 'sow-items':
                print("number of sow items = " + str(len(retrievedList)))
                for sowItem in retrievedList:
                    print("sow-item-id: " + str(sowItem.get('sow-item-id')))
                    numOfAttributesNotFound = 0
                    for attributeName in self.expectedAttributes:
                        if attributeName not in sowItem:
                            print("*** Error. Not Found. Expecting attribute in sowItem= ", attributeName)
                            numOfAttributesNotFound = numOfAttributesNotFound + 1
                            self.errorCnt += 1
                    if numOfAttributesNotFound == 0:
                        print("       Found all expected attributes.")


    # -------------------------------------------------------------------------------------------
    # verifyReturnedAttributes
    # now go the other way and verify that all the returned  attributes are the same as the expected attributes (per contract)
    # input:  json string from WS call
    # ouput:  updates errorCnt (global)
    def verifyReturnedAttributes(self, jsonstr):
         for key, retrievedList in jsonstr.items():
            if key == 'sow-items':
                print("number of sow items = " + str(len(retrievedList)))
                for sowItem in retrievedList:
                    print("sow-item-id: " + str(sowItem.get('sow-item-id')))
                    numOfUnExpectedAttributesFound = 0
                    for attributeName in sowItem:
                        if attributeName not in self.expectedAttributes:
                            print("*** Error, Unexpected attribute returned in sowItem= ", attributeName)
                            numOfUnExpectedAttributesFound = numOfUnExpectedAttributesFound + 1
                            self.errorCnt += 1
                    if numOfUnExpectedAttributesFound == 0:
                        print("       No unexpected attributes.")


    # -------------------------------------------------------------------------------------------
    # doTests
    #
    # input:  server,sp
    # ouput:  updates errorCnt (global)
    def doTests(self, server, sp):
        url = self.setURL(sp, server)
        wsTool = wsTools()
        print("")
        print("--------start: get Sow Items WST tests---------")
        print("---- TEST 1: VALID REQUEST,   EXPECT STATUS = 200 and JSON response ----")
        jsonStr = wsTool.checkforValidRequest(url)
        '''if jsonStr == {}:  # if no connection  or can not get jsonstr, exit
            self.errorCnt = 1
        else:
            # TEST 2: verify that all the expected attributes are returned in response of WS call
            print(" ")
            print("---- TEST 2: VERIFY THAT ALL EXPECTED ATTRIBUTES are RETRIEVED in response of WS call ----")
            self.verifyExpectedAttributes(jsonStr)

            # TEST 3: VERIFY THAT all the RETURNED ATTRIBUTES are the same as the expected attributes
            print(" ")
            print(
                "---- TEST 3: VERIFY THAT all the RETURNED ATTRIBUTES are same as expected attributes ----")
            self.verifyReturnedAttributes(jsonStr)

            # TEST 4 invalid sp-id , expect 404
            print(" ")
            print("---- TEST 4: verify INVALID REQUEST - invalid SP ID,   EXPECT STATUS = 404 ----")
            self.errorCnt += wsTool.checkForInvalidRequest(self.setURL(0, server))

            # TEST 5 missing spid , expect 404
            print(" ")
            print("---- TEST 5: verify INVALID REQUEST - No SP ID,   EXPECT STATUS = 404 ----")
            self.errorCnt += wsTool.checkForMissingSPID(self.setURL('', server))

            # TEST 6 using (not allowed)  POST, PUT,DELETE  methods , expect 405
            print(" ")
            print("---- TEST 6: METHOD NOT ALLOWED - for POST, PUT,DELETE ,   EXPECT STATUS = 405 ----")
            self.errorCnt += wsTool.checkForInvalidCommands(self.setURL(sp, server))

            print ("*** sow Items error count =" + str(self.errorCnt))'''

        return self.errorCnt

    # -------------------------------------------------------------------------------------------
    # runTest
    #
    # input:  server,sp
    # ouput:  updates errorCnt (global)
    def runTest(self, server, sp):
        errs = self.doTests(server, sp)
        return errs

# -------------------------------------------------------------------------------------------
# get Sow Items WS tests
# run tests

'''myTest = sowItemsWST()
        devServer = 'claritydev1.jgi-psf.org'
        prdServer = 'clarityprd1.jgi-psf.org'
        intServer = 'clarityint1.jgi-psf.org'
        zuulServer = 'clarity-int01.jgi-psf.org:8181'
        sp =1208084 # 1207398  # has 1288 sow items  ;    1208084 has 1 sow item

myTest.runTest(intServe,sp)'''


'''devServer = 'claritydev1.jgi-psf.org'
prdServer = 'clarityprd1.jgi-psf.org'
intServer = 'clarityint1.jgi-psf.org'
zuulServer = 'clarity-int01.jgi-psf.org:8181'
sp= 1207398 #has 1288 sow items  ;    1208084 has 1 sow item

myTest = sowItemsWST()
errs = myTest.doTests(intServer, sp)

print("")
print("---Number of Errors Found = " + str(errs) + " ---")
'''


# history -
# 5-19-17 this test was successful in finding a bug with documentation (attribute was not listed that actually was retrieved)
# created   PPS-3832 (doc: retrieve sow-items for SP WS returns unexpected attribute)
# 6-30-17 requirements doc updated and code likewise.  all Tests passed
# 7-19-17 converted to run as unit tests modules. called from the testengine, this returns the number of errors found.
