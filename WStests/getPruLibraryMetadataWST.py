#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'

from Utils.wsTools import wsTools


#name:  getPruLibraryMetadataWST  - get PruLibraryMetadata Web Service Test
#code verifies info on requirements doc:  https://docs.google.com/document/d/17Nj1SFuQZiPMoTQ62bTg_nyX9Fxa2Gs037mt0M6jVaU/edit#heading=h.4ylivlgdammn



class getPruLibraryMetadata():
    def __init__(self):
        self.errorCnt = 0
        self.printOn = True

        self.expectedAttributes = {'scientific-program': 0,'user-program': 0,'account-purpose': 0,'account-year': 0,
                                   'final-deliv-project-id': 0,'final-deliv-project-name': 0,'proposal-id': 0,
                                   'sequencing-project-id': 0,'sequencing-project-name': 0,'material-type': 0,
                                   'sequencing-project-manager-id': 0,'sequencing-product-name': 0,'genus': 0,
                                   'species': 0,'strain': 0,'genome-size-estimated-mb': 0,'ncbi-organism-name': 0,
                                   'ncbi-tax-id': 0,'isolate': 0,'kingdom': 0,'sow-item-id': 0,'sow-item-type': 0,
                                   'target-logical-amount': 0,'total-logical-amount-completed': 0,'logical-amount-units': 0,
                                   'target-insert-size-kb': 0,'target-template-size-bp': 0,
                                   'tight-insert': 0,'overlapping-reads': 0,'rrna-depletion': 0,'poly-a-selection': 0,
                                   'itag-primer-set': 0,'itag-primer-set-id': 0,'sample-id': 0,'sample-name': 0,
                                   'plate-location': 0,'container-id': 0,'container-label': 0,'library-name': 0,
                                   'library-protocol': 0,'index-name': 0,'index-sequence': 0,'actual-insert-size-kb': 0,
                                   'actual-template-size-bp': 0,'final-deliv-product-name': 0,'actual-library-creation-queue': 0,
                                   'actual-library-creation-queue-id': 0,'library-creation-specs': 0,
                                   'library-creation-specs-id': 0,'library-creation-queue-id': 0,
                                   'library-creation-queue': 0,'number-of-pcr-cycles': 0,'control-type': 0,
                                   'control-organism-name': 0,'control-organism-tax-id': 0,'collaborator-library-name': 0,
                                   'external': 0}

    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          cmd  -  includes lib and pru,
    # i.e 'http://claritydev1.jgi-psf.org/pru-library-metadata?pru-lims-id=2-262195&library-name=AYUNC
    def setURL(self, cmd, server):
        requestURL = 'http://' + server + '/' + cmd
        print("url = " + requestURL)
        return requestURL





    # -------------------------------------------------------------------------------------------
    # getPruLib_Generic
    #
    # input:  server
    #
    #         lib = library name
    #         pru = pyhsical run unit

    def getPruLib_Generic(self, server, lib, pru):
        wsTool = wsTools()
        pruLibCmd = 'pru-library-metadata?pru-lims-id=' + pru + '&library-name=' + lib  # set up command
        url = self.setURL(pruLibCmd, server)
        print('PRU Lib metada WS test:  run ' + pru + ' ' + lib, "\n")

        jsonStr = wsTool.checkforValidRequest(url)
        if jsonStr == {}:  # if no connection  or cand not get jsonstr, exit
            self.errorCnt+= 1
            # exit()
        else:
            # TEST 2: verify that all the expected attributes are returned in response of WS call
            print(" ")
            print("---- TEST 2: VERIFY THAT ALL EXPECTED ATTRIBUTES are RETRIEVED in response of WS call ---(removed test temporarily)-")

            #self.verifyExpectedAttributes(jsonStr)

            # TEST 3: VERIFY THAT all the RETURNED ATTRIBUTES are the same as the expected attributes
            print(" ")
            print(
                "---- TEST 3: VERIFY THAT all RETURNED ATTRIBUTES are same as expected attributes ---(removed test temporarily)-")
            #self.verifyReturnedAttributes(jsonStr)
            print("")

    # -------------------------------------------------------------------------------------------
    # verifyExpectedAttributes
    # verify that all the expected attributes are returned in response of WS call
    # input:  json string from WS call
    # ouput:  updates errorCnt (global)

    def verifyExpectedAttributes(self, jsonstr):
          retrievedList = jsonstr['libraries-metadata'][0]
          #print (retrievedList)
          numOfAttributesNotFound = 0
          for attributeName in self.expectedAttributes:
                if attributeName not in retrievedList:
                    print("*** Error. Not Found. Expecting attribute in response  = ", attributeName)
                    numOfAttributesNotFound = numOfAttributesNotFound + 1
                    self.errorCnt = self.errorCnt + 1
          if numOfAttributesNotFound == 0:
                print("       Found all expected attributes.")

    # -------------------------------------------------------------------------------------------
    # verifyReturnedAttributes
    # now go the other way and verify that all the returned  attributes are the same as the expected attributes (per contract)
    # input:  json string from WS call
    # ouput:  updates errorCnt (global)

    def verifyReturnedAttributes(self, jsonstr):
        retrievedList = jsonstr['libraries-metadata'][0]
        numOfUnExpectedAttributesFound = 0
        for attributeName in retrievedList:
            #print (attributeName)
            if attributeName not in self.expectedAttributes:
                  print("*** Error, Unexpected attribute returned  = ", attributeName)
                  numOfUnExpectedAttributesFound = numOfUnExpectedAttributesFound + 1
                  self.errorCnt = self.errorCnt + 1
        if numOfUnExpectedAttributesFound == 0:
              print("       No unexpected attributes.")

    # -------------------------------------------------------------------------------------------
    # doTests
    #
    # input:  server
    # ouput:  err - number of errors found

    def doTests(self, server):
        print("")
        print("--------start: pru-library-metadata WST tests---------")
        #clarity -int01 pacbio sequel libs
        '''self.getPruLib_Generic(server, 'CSYHZ', "2-3315534")
        self.getPruLib_Generic(server, 'CSZXY', "2-3323972")
        self.getPruLib_Generic(server, 'CSYWX', "2-3320003")
        self.getPruLib_Generic(server, 'CSYWH', "2-3323995")
        self.getPruLib_Generic(server, 'CSYOG', "2-3315534")
        self.getPruLib_Generic(server, 'CSYHY', "2-3315532")

        self.getPruLib_Generic(server, 'CUXAB', "2-3420444")  #for pool CUXOG  
        print("")

        self.getPruLib_Generic(server, 'CSYOO', "2-3315530")  # for pool CSYOU
        print("")
        '''
        self.getPruLib_Generic(server, 'AUBZP', "2-143675")
        print("")
        self.getPruLib_Generic(server, 'CBBZT', "2-2294065")  #
        print("")
        self.getPruLib_Generic(server, 'CNOHO', "2-3111654")  # POOL  COOTS
        print("")
        print("")
        '''
        self.getPruLib_Generic(server, 'CHCCP', "2-2889893")  # POOL  COOTS
        self.getPruLib_Generic(server, 'CHHHA', "2-2456542")  # POOL  CHPUX
        '''
        '''self.getPruLib_Generic(server, 'BSXBZ', "2-3600314")
        self.getPruLib_Generic(server,  'AYUNC', "2-262195")
        self.getPruLib_Generic(server,  'AYUNC', "2-1349834")  # gls pru
        self.getPruLib_Generic(server, 'BTPCT', "2-2393624")
        self.getPruLib_Generic(server, 'BYYXA', "2-2596666")
        self.getPruLib_Generic(server, 'BBTNZ', "2-1530407")  # DOP = 1
        self.getPruLib_Generic(server, 'BBTNZ', "2-262195")    #bad combo, expect error
        self.getPruLib_Generic(server, 'BBTNZ', "2-26219500")''  # resource, expect error'''
        print (self.errorCnt)

        return self.errorCnt


# -------------------------------------------------------------------------------------------
# get Sow Items WS tests
# run tests
devServer = 'claritydev1.jgi-psf.org'
prdServer = 'clarityprd1.jgi-psf.org'
intServer = 'clarityint1.jgi-psf.org'
zuulServer = 'zuul-int.jgi.doe.gov:8181'
myTest = getPruLibraryMetadata()
myTest.doTests(zuulServer)
print ("errors found = ", myTest.errorCnt)






