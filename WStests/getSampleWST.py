#!/usr/bin/env python
__author__ = 'Oleg and Becky(later)'

import requests

from Utils.wsTools import wsTools


#name:  getSampleWST  - get samples of SP Web Service Test
#code verifies info on requirements doc:  https://docs.google.com/document/d/1Z7wtLp-F1fE797JmzuVgiuvQSyId0-dSJFMJBE7qdkg/edit
#this program, runs the ws and verifies that the attributes returned in response are the same as documented in
#requirements doc. Checks that return status is correct when invalid request is given. Checks that correct return status for HTTP calls
#returns the number of errors found


class getSampleWST():

    def __init__(self):
        self.errorCnt = 0
        self.zuulTesting = False
        self.zuulURL = ""
        self. expectedAttributes = {'sample-id': 0, 'sample-name': 0, 'sample-receipt-date': 0,
                          'current-sample-status': 0,
                          'sample-current-mass-ng': 0,
                          'sample-qc-status': 0, 'sample-qc-type': 0, 'max-insert-size-kb': 0,
                          'plated-sample': 0, 'tube-plate-label': 0, 'group-name': 0,
                          'qc-comments': 0, 'sample-comments': 0 ,'external':0 #not in requirements doc
                          }

    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          sp  -  the sequencing project ID under test
    def setURL(self,sp,server):
        requestURL = 'http://' + server + '/sequencing-projects/' + str(sp) + '/samples'

        #zuulURL

        if self.zuulTesting:
            requestURL = self.zuulURL
              #  'http://clarity-int01.jgi-psf.org:8181/sequencing-projects/' + str(sp) + '/samples'

        print("url = " + requestURL)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # verifyExpectedAttributes
    # verify that all the expected attributes are returned in response of WS call
    # input:  json string from WS call
    # ouput:  updates errorCnt (global)
    def verifyExpectedAttributes(self,jsonstr):
         for key, retrievedList in jsonstr.items():  #for each sample, check that  all attributes that are required (per contract) are in WS response
            if key == 'samples':
                print("number of samples in SP = " + str(len(retrievedList)))
                for sample in retrievedList:
                    print("Sample ID: " + str(sample.get('sample-id')))
                    numOfAttributesNotFound = 0
                    for attributeName in self.expectedAttributes:
                         if attributeName not in sample:
                            print("*** Error. Not Found. Expecting attribute in sample = ", attributeName)
                            numOfAttributesNotFound = numOfAttributesNotFound+1
                            self.errorCnt += 1
                    if numOfAttributesNotFound == 0:
                        print("       Found all expected attributes.")


    # -------------------------------------------------------------------------------------------
    # verifyReturnedAttributes
    # now go the other way and verify that all the returned  attributes are the same as the expected attributes (per contract)
    # input:  json string from WS call
    # ouput:  updates errorCnt (global)
    def verifyReturnedAttributes(self, jsonstr):

         for key, retrievedList in jsonstr.items():
            if key == 'samples':
                print("number of samples = " + str(len(retrievedList)))
                for sample in retrievedList:
                    print("Sample ID: " + str(sample.get('sample-id')))
                    numOfUnExpectedAttributesFound = 0
                    for attributeName in sample:
                        if attributeName not in self.expectedAttributes:
                            print("*** Error, Unexpected attribute returned in sample = ", attributeName)
                            numOfUnExpectedAttributesFound +=  1
                            self.errorCnt +=  1
                    if numOfUnExpectedAttributesFound == 0:
                        print("       No unexpected attributes.")


    # -------------------------------------------------------------------------------------------
    # doWSrequest
    # run WS, expect 200 status
    # input:  url
    # ouput:  updates errorCnt (global), jsonstring (type = dict)
    def doWSrequest(self,url):
        r = requests.get(url)
        status = r.status_code
        if status != 200:
            print("*** Error, Unexpected Returned Status = ", status)
            self.errorCnt += 1
            return {}
        else:
            return r.json()



    # -------------------------------------------------------------------------------------------
    # doTests
    #
    # input:  server,sp
    # ouput:  updates errorCnt (global)
    def doTests(self, server, sp):
        wsTool = wsTools()
        url = self.setURL(sp, server)
        print("")
        print("--------start: getSamples WST tests---------")
        #print("---- TEST 1: VALID REQUEST,   EXPECT STATUS = 200 and JSON response ----")
        jsonStr = wsTool.checkforValidRequest(url)
        if jsonStr == {}:  # if no connection  or can not get jsonstr, exit
            self.errorCnt = 1
        else:
            # TEST 2: verify that all the expected attributes are returned in response of WS call
            print(" ")
            '''print("---- TEST 2: VERIFY THAT ALL EXPECTED ATTRIBUTES are RETRIEVED in response of WS call ----")
            self.verifyExpectedAttributes(jsonStr)

            # TEST 3: VERIFY THAT all the RETURNED ATTRIBUTES are the same as the expected attributes
            print(" ")
            print(
                "---- TEST 3: VERIFY THAT all the RETURNED ATTRIBUTES are same as expected attributes ----")
            self.verifyReturnedAttributes(jsonStr)

             # TEST 4 invalid sp-id , expect 404
            print(" ")
            print("---- TEST 4: verify INVALID REQUEST - invalid SP ID,   EXPECT STATUS = 404 ----")
            self.errorCnt +=  wsTool.checkForInvalidRequest(self.setURL(0, server))

            # TEST 5 missing spid , expect 404
            print(" ")
            print("---- TEST 5: verify INVALID REQUEST - No SP ID,   EXPECT STATUS = 404 ----")
            self.errorCnt +=  wsTool.checkForMissingSPID(self.setURL('', server))

            # TEST 6 using (not allowed)  POST, PUT,DELETE  methods , expect 405
            print(" ")
            print("---- TEST 6: METHOD NOT ALLOWED - for POST, PUT,DELETE ,   EXPECT STATUS = 405 ----")
            self.errorCnt +=  wsTool.checkForInvalidCommands(self.setURL(sp, server))'''

        return self.errorCnt


    # -------------------------------------------------------------------------------------------
    # runTest
    #
    # input:  server,sp
    # ouput:  num of errs
    def runTest(self,server, sp):
       errs = self.doTests(server, sp)
       return errs





#--------------------------------

#uncomment to run from this file

'''myTest = getSampleWST(url,sp)
        devServer = 'claritydev1.jgi-psf.org'
        prdServer = 'clarityprd1.jgi-psf.org'
        intServer = 'clarityint1.jgi-psf.org'
        sp=1207398      #93 samples
myTest.runTest()
'''
#history -
# 6-30-17 this test was successful in finding 1 bug  with documentation:
# https://docs.google.com/document/d/1Z7wtLp-F1fE797JmzuVgiuvQSyId0-dSJFMJBE7qdkg/edit
#      * created  PPS-3912 doc: retrieve sow-item templates for SP WS expected attribute not returned
# 7-19-17 converted to run as unit tests modules. called from the testengine, this returns the number of errors found.
