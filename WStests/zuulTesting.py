#!/usr/bin/env python


import requests
import json
import datetime
from Utils.wsTools import wsTools
from Utils.util_jsonInput import jsonInput
from WStests.putSequencingProjectsWST import putSequencingProjectsWST


#name:  zuulTesting  - run WS using zuul URL addressing

class ZuulTestingWST():

    def __init__(self):
        self.errorCnt = 0
        self.zuulURL = ""
        self.env = ""
        self.zuulServer=""
        self.wsTool = wsTools()
        self.putSP = putSequencingProjectsWST()



    # -------------------------------------------------------------------------------------------
    # callWS  calls webservice (get)
    #
    # input:  server,sp
    # ouput:  updates errorCnt (global)
    def callWS(self,webService):
        url = self.zuulServer + 'env:' + self.env + webService  #set URL
        print("url = " + url)

        #wsTool = wsTools()

        jsonStr = self.wsTool.runWS(url)
        if jsonStr == {}:  # if no connection  or can not get jsonstr, exit
            self.errorCnt += 1
        else:
             print("!!!---Test Success: " + webService + " -------------\n ")

    # -------------------------------------------------------------------------------------------
    # addSowPost
    #
    # inputs: requestURL
    #         requestJson  - the json record for request
    def addSowPost(self,webservice,requestJson,expectedStatus):
        url = self.zuulServer + 'env:' + self.env + webService  # set URL
        print("url = " + url)
        mydata = json.dumps(requestJson)
        headers = {"Content-Type": "application/json", 'data':mydata}
        # Call REST API
        response = requests.post(url, data=mydata, headers=headers)  #POST  request
        status = response.status_code
        if status != expectedStatus:
            print("*** Error, Unexpected Returned Status = ", status)
            print("***---Test Failed: " + webService + " ****\n ")
            self.errorCnt +=  1
            prettyJson = json.loads(response.text)
            print("WS request status = " + str(status))
            print("response:" + json.dumps(prettyJson, indent=4, sort_keys=True))
        else:
            # Print Response
            prettyJson = json.loads(response.text)
            print (json.dumps(prettyJson, indent=4, sort_keys=True))
            print("!!!---Test Success: " + webService + " -------------\n ")
            #return prettyJson

    # -------------------------------------------------------------------------------------------
    # updateSPall
    #
    # inputs: requestURL
    def updateSPall(self,webService):
        url = self.zuulServer + 'env:' + self.env + webService  # set URL
        print("url = " + url)
        #  make sure that you will always have a unique SP name by adding time stamp to base name
        timeNow = ' {:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())

        self.putSP.updatedAttributes['sequencing-project-name']= self.putSP.updatedAttributes['sequencing-project-name']  + timeNow

        mydata = json.dumps(self.putSP.updatedAttributes)
        headers = {"Content-Type": "application/json", 'data':mydata}
        # Call REST API
        response = requests.put(url, data=mydata, headers=headers)  #PUT  request
        status = response.status_code
        if status != 200:
            print("*** Error, Unexpected Returned Status = ", status)
            print("***---Test Failed: Update:" + webService + " ****\n ")
            self.errorCnt +=  1
            prettyJson = json.loads(response.text)
            print("WS request status = " + str(status))
            print("response:" + json.dumps(prettyJson, indent=4, sort_keys=True))
        else:
            # Print Response
            prettyJson = json.loads(response.text)
            print (json.dumps(prettyJson, indent=4, sort_keys=True))
            print("!!!---Test Success: Update:" + webService + " -------------\n ")



# -------------------------------------------------------------------------------------------
#getSamples WS tests

# run tests

sp=1205820

print("--------start: ZUUL TESTING---------")
myTest = ZuulTestingWST()
myTest.zuulServer = "http://pps-util01.jgi-psf.org:8181/"
envNames = ["int","dev","prd"]
for envToTest in envNames:
    myTest.env = envToTest
    print ("\nTesting environment: " + myTest.env + "\n")

    #sequencing projects/sample
    webService = '/sequencing-projects/' + str(sp) + '/samples'
    myTest.callWS(webService)

    #pru-library-metadata
    webService = '/pru-library-metadata?pru-lims-id=2-143675&library-name=AUBZP'
    myTest.callWS(webService)

    #sequencing projects
    sp=1204109
    webService = '/sequencing-projects/' + str(sp)
    myTest.callWS(webService)

    #sequencing projects/sow-items
    webService = '/sequencing-projects/' + str(sp) + '/sow-items'
    myTest.callWS(webService)

    #sequencing projects/sow-items-templates

    webService = '/sequencing-projects/' + str(sp) + '/sow-item-templates'
    myTest.callWS(webService)

    #addsowitem
    webService = '/add-sow-item/'
    if myTest.env != "prd":
        myJson = jsonInput()
        submissionJson = myJson.addSowToNewSam
        myTest.addSowPost(webService, submissionJson, 201)  # call WS POST,response in dictionary form
    else:
        print ("**** !!skipping " + webService + " test on " + myTest.env + "**** !!")

    #updateSequencingProject
    webService = '/sequencing-projects/' + str(sp)
    if myTest.env != "prd":
        myTest.updateSPall(webService)  # response in dictionary form
    else:
        print ("**** !!skipping updating" + webService + " test on " + myTest.env + "**** !!")

    #library - creation - queues
    webService = '/library-creation-queues'
    myTest.callWS(webService)

    #clarity-api
    # (not working as expected)
    #webService = '/api/v2'
    #myTest.callWS(webService)


print ("End")
print("---Number of Errors Found = " + str(myTest.errorCnt) + " ---")

