#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 7/25/17

import datetime
import json

import requests

from Utils.util_udfTools import udfTools
from Utils.wsTools import wsTools


#under constuction  - for later   rjr 8/22


#name:  sequencingProjectsPutMoreWST  - (Generic) Sequencing Projects Web Service Test, test PUT command only
# runs more verifications test that are specidied in requirements
# under construction  - intend to expand on status checks and other tests not covered in sequencingProjectsPutWST
#code verifies  using info on requirements doc:
# https://docs.google.com/document/d/15ANSoeMku86UkllU5fizCseGIYHpc_Jg7wgexsuP48A/edit#heading=h.6rg9uf20u7t4
#Checks that correct return status for HTTP Put calls
#note: the json retrieved is only one level.

class  sequencingProjectsPutMoreWST():

    def __init__(self):

        self.errorCnt = 0
        #these are all the attributes that can be updated on put command. Use this or subset
        self.updatedAttributes = {"last-updated-by": 5981,
                             "sequencing-project-name": "RjrSP_WS_automatic_test",
                             "current-status": "In Progress",
                             "status-date": "2017-04-27",
                             "embargo-start-date": "2017-04-27",
                             "project-contact-cid": 5981,
                             "sample-contact-cid": 13,
                             "comments": "warning! this SP is being used for automated testings.  The fields are set through the test program - RJR",
                             "actual-sequencing-product-name": "Microbial Minimal Draft, Single Cell",
                             "ref-genome": "genometest",
                             "ref-genome-type": "Genome Portal",
                             "ref-transcriptome": "transcriptome",
                             "ref-transcriptome-type": "Local File",
                             "auto-schedule-sow-items": "N"}

        # mapping of attributes of WS call and UDFS (the key is the attribute name from the ws call, the value is the UDF name)
        self.spUDFattributesMap = {'sequencing-project-name': 'Sequencing Project Name',
                              'project-contact-cid': 'Sequencing Project PI Contact Id',
                              'scientific-program-name': 'Scientific Program',
                              'material-type': 'Material Category',
                              'project-manager-cid': 'Sequencing Project Manager Id',
                              'sample-contact-cid': 'Sample Contact Id',
                              'actual-sequencing-product-name': 'Sequencing Product Name',
                              'proposal-id': 'Proposal Id',
                              'auto-schedule-sow-items': 'AutoSchedule Sow Items'}


    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          sp  -  the sequencing project ID under test
    def setURL(self,sp,server):
        requestURL = 'http://' + server + '/sequencing-projects/' + str(sp)
        print (requestURL)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # updateSPall
    #
    # inputs: requestURL
    def updateSPall(self,requestURL):
        #  make sure that you will always have a unique SP name by adding time stamp to base name
        timeNow = ' {:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())

        self.updatedAttributes['sequencing-project-name']= self.updatedAttributes['sequencing-project-name']  + timeNow

        mydata = json.dumps(self.updatedAttributes)
        headers = {"Content-Type": "application/json", 'data':mydata}
        # Call REST API
        response = requests.put(requestURL, data=mydata, headers=headers)  #PUT  request
        status = response.status_code
        if status != 200:
            print("*** Error, Unexpected Returned Status = ", status)
            self.errorCnt +=  1
            prettyJson = json.loads(response.text)
            print("WS request status = " + str(status))
            print("response:" + json.dumps(prettyJson, indent=4, sort_keys=True))
        else:
            # Print Response
            prettyJson = json.loads(response.text)
            print (json.dumps(prettyJson, indent=4, sort_keys=True))
            print (type(prettyJson))
        return prettyJson
    # --------------------------------------------------------------------------
    # updateStatusAndDate
    #
    # inputs:
    def updateStatusAndDate(self, url, date,status,errorExpected):
        #set up data structure to use in request PUT to update status and status date
        statusDateAttributes = {"last-updated-by": 5981,
                                "current-status": 0,
                                "status-date": 0
                                }
        requestURL = url

        statusDateAttributes['status-date'] = date
        statusDateAttributes['current-status'] = status

        print ("change requested: " + str(statusDateAttributes))

        mydata = json.dumps(statusDateAttributes)
        headers = {"Content-Type": "application/json", 'data': mydata}
        # Call REST API
        response = requests.put(requestURL, data=mydata, headers=headers)  # PUT  request

        # print the json response in pretty format
        prettyJson = json.loads(response.text)
        status = response.status_code
        print("WS request status = " + str(status))

        if errorExpected:
             if status != 200:
                  print("response:" + json.dumps(prettyJson, indent=4, sort_keys=True))

             else:
                newDate = prettyJson['status-date']
                newStatus = prettyJson['current-status']
                print("Updated to New Status = " + newStatus + " ,New Date = " + newDate)
                self.errorCnt += 1
                print("*** Expected Error for request of  illegal date/status combo, but none given ***")

        else:  #   error not Expected:
            if status != 200:
                    self.errorCnt += 1
                    print("*** Unexpected Error for  date/status combo ***")
                    print("response:" + json.dumps(prettyJson, indent=4, sort_keys=True))

            else:
                newDate = prettyJson['status-date']
                newStatus = prettyJson['current-status']
                print("Updated to New Status = " + newStatus + " ,New Date = " + newDate)

        print("----done----")
        print("")

     # --------------------------------------------------------------------------
     # use this  test on sp that starts in progress
    def verifyStatusDateChanges(self,url):
        #test that the status date can only be updated to a future date.
        #  verify that status date cannot be set to a date prior to the last status date, expect error 422
        self.updateStatusAndDate(url,"2017-01-30","Awaiting Colaborator Metadata",True)
        self.updateStatusAndDate(url,"2017-02-30","Awaiting Shipping Approval",True)  #expect error illegal date
        self.updateStatusAndDate(url,"2017-13-30","Awaiting Eligible Sample",True)
        self.updateStatusAndDate(url, "2017-01-30", "Awaiting Colaborator Metadata", True)
        self.updateStatusAndDate(url,"2017-02-12","In Progress",False)  #ok to set prior date
        # self.updateStatusAndDate(url,"2017-05-30","Awaiting Eligible Sample",True)
        self.updateStatusAndDate(url,"2015-06-12","Awaiting Shipping Approval",True)
        self.updateStatusAndDate(url,"2017-02-18","",True)  #expect error, status date can not be set with out status change
        self.updateStatusAndDate(url, "2017-03-12", "Deleted",True)  # expect error,  bad status transisiton
        self.updateStatusAndDate(url,"2017-09-30","Awaiting Eligible Sample",False)
        self.updateStatusAndDate(url, "2017-05-19", "In Progress", False)  # ok to set future date


    # -------------------------------------------------------------------------------------------
    # doTests
    #
    # input:  server,sp
    # ouput:  updates errorCnt (global)
    def doTests(self, server, sp):
        url = self.setURL(sp, server)
        wsTool = wsTools()
        print("")
        print("--------start: PUT SequencingProjects WST tests---------")
        print("---- TEST 1: VALID REQUEST,   EXPECT STATUS = 200 and JSON response ----")
        jsonStr = wsTool.checkforValidRequest(url)
        if jsonStr == {}:  # if no connection  or can not get jsonstr, exit
            self.errorCnt = 1
        else:
            # TEST 2: verify that one can update the SP with a PUT request
            print(" ")
            print("---- TEST 2: VERIFY THAT one can update data by using a PUT command to WS  update sp name----")
            self.updateSPall(url)

            print(" ")
            print("---- TEST 3: VERIFY  status date changes ----")
            if jsonStr['current-status']== 'In Progress':
                self.verifyStatusDateChanges(url)
            else:
                print (jsonStr['current-status'])
                self.verifyStatusDateChanges(url)
        return self.errorCnt


    # getProjectUDFs
    #
    # inputs: sp, server, type (i.e. 'project', 'sample', etc)
    # outputs: SP UDFs as a dict
    def getUDFs(self,sp,server,type):
        myUDFs = {}
        url = udfTools.setURL(sp, server)
        if url:
            if type=='project':
                projectUrl = udfTools.connectToClarityAPIprojects(url)
                if projectUrl:
                    myUDFs = udfTools.getProjectUDFs(projectUrl)
        return myUDFs



# -------------------------------------------------------------------------------------------

# -------------------------------------------------------------------------------------------
    # runTest
    #
    # input:  server
    # ouput:  err - number of errors found

    def runTest(self):
        # -------------------------------------------------------------------------------------------
        # get Sow Items WS tests
        # run tests
        devServer = 'claritydev1.jgi-psf.org'
        prdServer = 'clarityprd1.jgi-psf.org'
        intServername = "clarity-int01"
        intServer = 'clarityint1.jgi-psf.org'
        sp= 1063593
        self.doTests(intServer,sp)
        return self.errorCnt


# run tests


myTest = sequencingProjectsPutMoreWST()
myTest.runTest()




'''
check these, some of them have other conditions to switch to other
#valid status transitions:
1-Created -> On Hold, awaiting colaborator metadata, deleted, Abandoned,
2-Awaiting Colaborator Metadata -> On Hold, Deleted, Abandoned,Awaiting Shipping Approval,Complete
3-Awaiting Shipping Approval ->  On Hold, Abandoned,Awaiting Colaborator Metadata,Awaiting Eligible Sample
4-Awaiting Eligible Sample -> On Hold, Abandoned, Awaiting Shipping Approval,Awaiting Colaborator Metadata, In Progress
5-In Progress -> On Hold, Abandoned, Awaiting Shipping Approval,Awaiting Colaborator Metadata, Awaiting Eligible Sample, All Sequencing Done, In Progress and Awaiting Material
10-All Sequencing Done -> Abandoned, Awaiting Shipping Approval,Awaiting Colaborator Metadata, Awaiting Eligible Sample,Complete,In Progress,In Progress and Awaiting Material
11-In Progress and Awaiting Material-> Abandoned, All Sequencing Done, In Progress
7-Abandoned -> On Hold
8-On Hold -> Abandoned
9-Deleted -> (none)
6-Complete -> (none)
'''

