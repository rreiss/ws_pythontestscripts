#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 7/25/17

import datetime
import json
import time

import requests

from Utils.util_dbTools import dbTools
from Utils.util_udfTools import udfTools
from Utils.wsTools import wsTools


#name:  sequencingProjectsPutWST  - (Generic) Sequencing Projects Web Service Test, test PUT command only
#1. veries that the data in PUT request is equal to the data in PUT response
#2.  veries that the UDF fields are updated as specified per requirements
#3. verifies that the database is updated per requirements

#code verifies  using info on requirements doc:
# https://docs.google.com/document/d/15ANSoeMku86UkllU5fizCseGIYHpc_Jg7wgexsuP48A/edit#heading=h.6rg9uf20u7t4
#Checks that correct return status for HTTP Put calls
#note: the json retrieved is only one level.

class  putSequencingProjectsWST():

    def __init__(self):
        self.printOn = True

        self.errorCnt = 0
        #these are all the attributes that can be updated on put command. Use this or subset
        self.updatedAttributes = {"last-updated-by": 5981,
                             "sequencing-project-name": "RjrSP_WS_automatic_test",
                             "current-status": "Awaiting Shipping Approval",
                             "status-date": "2017-05-11",
                             "embargo-start-date": "2017-03-27",
                             "project-contact-cid": 5981,
                             "sample-contact-cid": 5981,
                             "comments": "warning! this SP is being used for automated testings.  The fields are set through the test program - RJR",
                             "actual-sequencing-product-name": "Metagenome Standard Draft",
                             "ref-genome": "genometest",
                             "ref-genome-type": "Genome Portal",
                             "ref-transcriptome": "transcriptome",
                             "ref-transcriptome-type": "Local File",
                             "auto-schedule-sow-items": "N"}

        # mapping of attributes of WS call and UDFS (the key is the attribute name from the ws call, the value is the UDF name)
        self.spUDFattributesMap = {'sequencing-project-name': 'Sequencing Project Name',
                              'project-contact-cid': 'Sequencing Project PI Contact Id',
                              'scientific-program-name': 'Scientific Program',
                              'material-type': 'Material Category',
                              'project-manager-cid': 'Sequencing Project Manager Id',
                              'sample-contact-cid': 'Sample Contact Id',
                              'actual-sequencing-product-name': 'Sequencing Product Name',
                              'proposal-id': 'Proposal Id',
                              'auto-schedule-sow-items': 'AutoSchedule Sow Items'}

        # mapping of attributes of WS call and db field  (the key is the  ws  attribute , the value is the DB field name)
        self.spDBattributesMap = { "sequencing-project-id": 'sequencing_project_id',
                                   'gls-sequencing-project-id': 'GLS_SEQUENCING_PROJECT_ID',
                                   # "sequencing-product-name":   // not a uss.dt_sequencing_project field - needs a join
                                   "sequencing-product-id": "SEQUENCING_PRODUCT_ID",
                                   "sequencing-project-name": "SEQUENCING_PROJECT_NAME",
                                   #"material-type": "gDNA"  // not a uss.dt_sequencing_project field - needs a join
                                   "default-account-id": "DEFAULT_ACCOUNT_ID",
                                   #"scientific-program-name":  // not a uss.dt_sequencing_project field - needs a join
                                   #"user-program-name": "CSP",  // not a uss.dt_sequencing_project field - needs a join
                                   #"user-program-year": "2012",  // not a uss.dt_sequencing_project field - needs a join
                                   #"program-purpose": "Programmatic",  // not a uss.dt_sequencing_project field - needs a join
                                   #"current-status": "In Progress",  // not a uss.dt_sequencing_project field -  uses separate method to get status
                                   "status-date": "STATUS_DATE",
                                   "eligible-public-release": "ELIGIBLE_PUBLIC_RELEASE",
                                   "embargo-start-date": "EMBARGO_START_DATE",
                                   #embargo-days": 0,  // not a uss.dt_sequencing_project field - needs a join
                                   "taxonomy-info-id": "TAXONOMY_INFO_ID",
                                    #"genome-size-mb": 4,   //not sure where this comes from
                                   "genomic-resources": "GENOMIC_RESOURCES",
                                   #"is-analyzed-data-releasable": "N",  // not a uss.dt_sequencing_project field - needs a join
                                   #"is-raw-data-releasable": "Y", // not a uss.dt_sequencing_project field - needs a join
                                   "project-contact-cid": "SEQUENCING_PROJECT_CONTACT_ID",
                                   "project-manager-cid": "SEQUENCING_PROJECT_MANAGER_ID",
                                   "sample-contact-cid": "SAMPLE_CONTACT_ID",
                                   "comments":"SEQUENCING_PROJECT_COMMENTS",
                                   # "sample-mapping-strategy": "1:All" // not a uss.dt_sequencing_project field - needs a join
                                   "author-names": "AUTHOR_NAMES",
                                   "actual-sequencing-product-id": "ACTUAL_SEQUENCING_PRODUCT_ID",
                                   #"actual-sequencing-product-name":  // not a uss.dt_sequencing_project field - needs a join
                                   "ref-genome": "REF_GENOME",
                                   "ref-genome-type": "REF_GENOME_TYPE",
                                   "ref-transcriptome": "REF_TRANSCRIPTOME",
                                   "ref-transcriptome-type": "REF_TRANSCRIPTOME_TYPE",
                                   #"proposal-id": 612,  // not a uss.dt_sequencing_project field - needs a join
                                   "sp-portal-visibility": "SP_PORTAL_VISIBILITY",
                                   "sp-project-visibility": "SP_PROJECT_VISIBILITY",
                                   "sp-visibility": "SP_VISIBILITY",
                                   "auto-schedule-sow-items": "AUTO_SCHEDULE_SOW_ITEMS",
                                   "availability-date": "AVAILABILITY_DATE",
                                   "final-deliv-project-id": "FINAL_DELIV_PROJECT_ID"}

    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          sp  -  the sequencing project ID under test
    def setURL(self,sp,server):
        requestURL = 'http://' + server + '/sequencing-projects/' + str(sp)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # updateSPall
    #
    # inputs: requestURL
    def updateSPall(self,requestURL):
        #  make sure that you will always have a unique SP name by adding time stamp to base name
        timeNow = ' {:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
        newName = 'RjrSP_WS_automatic_test' + timeNow
        time.sleep(1)
        self.updatedAttributes['sequencing-project-name'] = newName
        #self.updatedAttributes['sequencing-project-name']= self.updatedAttributes['sequencing-project-name']  + timeNow
        #self.updatedAttributes['comments'] =  timeNow

        mydata = json.dumps(self.updatedAttributes)
        headers = {"Content-Type": "application/json", 'data':mydata}
        # Call REST API
        response = requests.put(requestURL, data=mydata, headers=headers)  #PUT  request
        status = response.status_code
        if status != 200:
            print("*** Error, Unexpected Returned Status = ", status)
            self.errorCnt +=  1
            prettyJson = json.loads(response.text)
            print("WS request status = " + str(status))
            print("response:" + json.dumps(prettyJson, indent=4, sort_keys=True))
        else:
            # Print Response
            prettyJson = json.loads(response.text)
            if self.printOn:
                print (json.dumps(prettyJson, indent=4, sort_keys=True))
            return prettyJson
    # -------------------------------------------------------------------------------------------
    # getUDFs
    #
    # inputs: sp, server, type (i.e. 'project', 'sample', etc)
    # outputs: UDFs as a dict
    def getUDFs(self,sp,server,type):
        myUDFs = {}
        url = udfTools.setURLproject(sp, server)
        if url:
            if type=='project':
                projectUrl = udfTools.connectToClarityAPIprojects(url)
                if projectUrl:
                    myUDFs = udfTools.getProjectUDFs(projectUrl)
        return myUDFs

    # -------------------------------------------------------------------------------------------
    # CompareResponseToRequestedAttributes
    # verify that the value in the Response attributes are the same as the value of the Requested attributes
    # input:  response (dict)
    # ouput:  updates errorCnt (global)

    def compareResponseToRequestedAttributes(self, respDict):
        for attr in self.updatedAttributes:
            if attr not in respDict:
                if attr != 'last-updated-by':  # ignore this one
                    print("*** Error. Not Found.  Requested attribute not in sequencing project response= ", attr)
                    print("")
                    self.errorCnt += 1
            else:
                if  respDict[attr]!= self.updatedAttributes[attr]:
                    print("*** Error. Requested attribute not updated sequencing project response= ", attr)
                    print(attr + ': request value=' + str(self.updatedAttributes[attr]) + ', response value= ' + str(respDict[attr]) )
                    print("")
                    self.errorCnt += 1
                elif self.printOn:
                    print(attr + ': request value=' + str(self.updatedAttributes[attr]) + ', response value= ' + str(
                        respDict[attr]))

    # -------------------------------------------------------------------------------------------
    # compareResponseToUDFAttributes
    # verify that the value in the Response attributes are the same as the value of the Requested attributes
    # input:  spUDF - the dict of UDFs for sp
    #         wsResp - the response Dict from WS call
    # ouput:  updates errorCnt (global)

    def compareResponseToUDFAttributes(self, spUDFs,wsResp):
        if spUDFs:
            if self.printOn:
                print("UDFS retrieved:")
                print(spUDFs)
            for wsAttr, udf in self.spUDFattributesMap.items():
                if str(spUDFs[udf]) != str(wsResp[wsAttr]):
                    print("*** UDF does not match WS update !!!  UDF(" + udf + ")=" + str(
                        wsResp[wsAttr]) + " !=  WS= " + str(spUDFs[udf]))
                    self.errorCnt += 1
                elif self.printOn:
                     print(wsAttr + ': UDF found')
        else:
            print("*** Error. UDFs not retrieved !!! ")
            self.errorCnt += 1



   # -------------------------------------------------------------------------------------------
    # compareResponseToDB
    # verify that the value in the Response attributes are the same as the value of the DB fields
    # input:
    #         wsResp - the response Dict from WS call
    #         sp - the SPID to get data from DB
    # ouput:  updates errorCnt (global)

    def compareResponseToDB(self, wsResp,spID):
        myDB = dbTools()
        if self.printOn:
            print("-----")
            print("DB fields:")
        for wsAttr, dbField in self.spDBattributesMap.items():
            if self.printOn:
                print (dbField)
            dbvalue = myDB.getDBattibute(dbField,"uss.dt_sequencing_project","SEQUENCING_PROJECT_ID",str(spID))
            if self.printOn:
                print (str (dbvalue))
            if str(wsResp[wsAttr]) == str(dbvalue):
                if self.printOn:
                    print ("    !Yay!  matches response!! ")
            else:
                print ("**** DB does not match Response: " + dbField + ": WS=" + wsResp[wsAttr] + ', DB=' + str(dbvalue))
                self.errorCnt += 1
        status = myDB.getStatus('sp',str(spID))
        if wsResp['current-status'] != status:
            print("**** DB does not match Response: Status: WS=" + wsResp['current-status'] + ', DB=' + status)
            self.errorCnt += 1
        else:
            print('current-status is correct:' + status)


    # -------------------------------------------------------------------------------------------
    # doTests
    #
    # input:  server,sp
    # ouput:  err - number of errors found
    def doTests(self, server, sp):
        url = self.setURL(sp, server)
        wsTool = wsTools()
        print("")
        print("--------start: put sequencing project WST tests - update SP name---------")
        #apiDevServer = 'clarity-dev01.jgi-psf.org'

        # set up the test with desired sp id and server
        if self.printOn:
            print("url = " + url)

        # check that the WS call will do a put, response should  be the updated fields
        if self.printOn:
            print("the response:")
        wsResp = self.updateSPall(url)  # response in dictionary form

        # compare the response with the request
        #self.compareResponseToRequestedAttributes(wsResp)
        #print (wsResp)

        # UDF verification
        # verify that the response for WS and the UDFs are the same
        # spUDFs = self.getUDFs(sp, apiDevServer,'project')  # get the udfs, return as a dict with UDF name as key, UDF value as value
        #  self.compareResponseToUDFAttributes(spUDFs, wsResp)

        # verify that the fields were updated in DB

        #self.compareResponseToDB(wsResp, sp)

        return self.errorCnt


    # -------------------------------------------------------------------------------------------
    # runtest
    #
    # input:  server,sp
    # ouput:  err - number of errors found
    def runTest(self,server,sp):
        errs = 0
        errs = self.doTests(server, sp)
        return errs


# -------------------------------------------------------------------------------------------
'''myTest = putSequencingProjectsWST()
errs = 0

        #devServer = 'claritydev1.jgi-psf.org'
        # prdServer = 'clarityprd1.jgi-psf.org'
        intServer = 'clarityint1.jgi-psf.org'
        #apiDevServer = 'clarity-dev01.jgi-psf.org'
        sp = 1019606  # 1018639 this part of a plate
for x in range(2):
     errs =  myTest.runTest()
     print(x)

print("")
print("---Number of Errors Found = " + str(errs) + " ---")'''
















# history -
# 7-26  created, ran with no errors test1-6