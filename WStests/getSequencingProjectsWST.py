#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 7/25/17

from Utils.wsTools import wsTools


#name:  sequencingProjectsWST  - (Generic) Sequencing Projects Web Service Test
#code verifies info on requirements doc:
# https://docs.google.com/document/d/15ANSoeMku86UkllU5fizCseGIYHpc_Jg7wgexsuP48A/edit#heading=h.6rg9uf20u7t4
#this program, runs the ws and verifies that the attributes returned in response are the same as documented in
#requirements doc. Checks that return status is correct when invalid request is given. Checks that correct return status for HTTP calls
#note: the json retrieved is only one level.




class getSequencingProjectsWST():

    def __init__(self):
        self.errorCnt = 0
        self. expectedAttributes ={'sequencing-project-id': 0,'gls-sequencing-project-id': 0,'sequencing-product-name': 0,
                          'sequencing-product-id': 0,'sequencing-project-name': 0,'material-type': 0,'default-account-id': 0,
                          'scientific-program-name': 0,'user-program-name': 0,'user-program-year': 0,'program-purpose': 0,
                          'current-status': 0,'status-date': 0,'eligible-public-release': 0,'embargo-start-date': 0,'embargo-days': 0,
                          'taxonomy-info-id': 0,'genome-size-mb': 0,'genomic-resources': 0,'is-analyzed-data-releasable': 0,
                          'is-raw-data-releasable': 0,'project-contact-cid': 0,'project-manager-cid': 0,'sample-contact-cid': 0,
                          'comments': 0,'sample-mapping-strategy': 0,'author-names': 0,'actual-sequencing-product-name': 0,
                          'actual-sequencing-product-id': 0,'ref-genome': 0,'ref-genome-type': 0,'ref-transcriptome': 0,
                          'ref-transcriptome-type': 0,'proposal-id': 0,'sp-portal-visibility': 0,'sp-project-visibility': 0,
                          'sp-visibility': 0,'availability-date': 0,'final-deliv-project-id': 0,'auto-schedule-sow-items': 0}
    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          sp  -  the sequencing project ID under test
    # outputs: request URL - properly formed URL for WS request
    def setURL(self,sp,server):
        requestURL = 'http://' + server + '/sequencing-projects/' + str(sp)
        print("url = " + requestURL)
        return requestURL


    # -------------------------------------------------------------------------------------------
    # verifyExpectedAttributes
    # verify that all the expected attributes are returned in response of WS call
    # input:  json string from WS call
    # ouput:  updates errorCnt (global)
    def verifyExpectedAttributes(self,jsonstr):
        # for sp, check that  all attributes that are required (per contract) are in WS response
        numOfAttributesNotFound = 0
        for attributeName in self.expectedAttributes:
            if attributeName not in jsonstr:
                print("*** Error. Not Found. Expecting attribute in sequencing project response= ", attributeName)
                numOfAttributesNotFound +=  1
                self.errorCnt +=  1
        if numOfAttributesNotFound == 0:
            print("       Found all expected attributes.")


    # -------------------------------------------------------------------------------------------
    # verifyReturnedAttributes
    # now go the other way and verify that all the returned  attributes are the same as the expected attributes (per contract)
    # input:  json string from WS call
    # ouput:  updates errorCnt (global)
    def verifyReturnedAttributes(self, jsonstr):
        numOfUnExpectedAttributesFound = 0
        for key in jsonstr.keys():
            keyfound = False
            for attributeName in self.expectedAttributes:
                if attributeName == key:
                    keyfound = True
                    break

            if not keyfound:
                print("*** Error, Unexpected attribute returned in SEQUENCING PROJECT WS response = ", str(key))
                numOfUnExpectedAttributesFound = numOfUnExpectedAttributesFound + 1
                self.errorCnt += 1


        if numOfUnExpectedAttributesFound == 0:
            print("       No unexpected attributes.")


    # -------------------------------------------------------------------------------------------
    # doTests
    #
    # input:  server,sp
    # ouput:  updates errorCnt (global)
    def doTests(self, server, sp,manyTests):
        wsTool = wsTools()
        url = self.setURL(sp, server)
        print("")
        print("--------start: Get SequencingProjects WST tests---------")
        print("---- TEST 1: VALID REQUEST,   EXPECT STATUS = 200 and JSON response ----")
        jsonStr = wsTool.checkforValidRequest(url)
        if jsonStr == {}:  # if no connection  or can not get jsonstr, exit
            self.errorCnt = 1
        elif (manyTests):  #if doing all the tests, continue
                # TEST 2: verify that all the expected attributes are returned in response of WS call
                print(" ")
                print("---- TEST 2: VERIFY THAT ALL EXPECTED ATTRIBUTES are RETRIEVED in response of WS call ----")
                self.verifyExpectedAttributes(jsonStr)

                # TEST 3: VERIFY THAT all the RETURNED ATTRIBUTES are the same as the expected attributes
                print(" ")
                print(
                    "---- TEST 3: VERIFY THAT all the RETURNED ATTRIBUTES are same as expected attributes ----")
                self.verifyReturnedAttributes(jsonStr)

                # TEST 4 invalid sp-id , expect 404
                print(" ")
                print("---- TEST 4: verify INVALID REQUEST - invalid SP ID,   EXPECT STATUS = 404 ----")
                self.errorCnt +=  wsTool.checkForInvalidRequest(self.setURL(0, server))

                # TEST 5 missing spid , expect 404
                print(" ")
                print("---- TEST 5: verify INVALID REQUEST - No SP ID,   EXPECT STATUS = 404 ----")
                self.errorCnt +=  wsTool.checkForMissingSPID(self.setURL('', server))

                # TEST 6 using (not allowed)  POST,DELETE  methods , expect 405  do not test for PUT (OK for this WS)
                print(" ")
                print("---- TEST 6: METHODs NOT ALLOWED - for POST,DELETE ,   EXPECT STATUS = 405 ----")
                self.errorCnt += wsTool.checkForInvalidPOST(self.setURL(sp, server))
                self.errorCnt += wsTool.checkForInvalidDELETE(self.setURL(sp, server))

        return self.errorCnt

    # -------------------------------------------------------------------------------------------
    # runTest
    #
    # input:  server,sp
    # ouput:  updates errorCnt (global)
    def runTest(self, server, sp):
        errsSP = self.doTests(server, sp, manyTests=False)
        return errsSP

# -------------------------------------------------------------------------------------------

# run tests

'''
devServer = 'claritydev1.jgi-psf.org'
prdServer = 'clarityprd1.jgi-psf.org'
intServer = 'clarityint1.jgi-psf.org'
zuulServer = 'clarity-int01.jgi-psf.org:8181'
sp = 1063593
mySPgetTest = getSequencingProjectsWST()
mySPgetTest.runTest(intServer,sp)
'''





#history -
# 7-26  created, ran with no errors test1-6
# 7-27  bug found with documentation. PPS-3981. Test 4 is returning 404, but requirement says s.b. 403











