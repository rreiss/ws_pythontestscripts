#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'

from Utils.wsTools import wsTools

#name:  sowItemTemplatesWST  - sow Items Templates Web Service Test
#code verifies info on requirements doc:  https://docs.google.com/document/d/1UXW7BEIbzy8XWuiIvLk7W6lKtLTFWwckkpm_fsALnok/edit
#this program, runs the ws and verifies that the attributes returned in response are the same as documented in
#requirements doc. Checks that return status is correct when invalid request is given. Checks that correct return status for HTTP calls

class sowItemTemplatesWST():
    def __init__(self):
        self.errorCnt = 0
        self.expectedAttributes = {'sow-number': 0, 'sow-item-type': 0, 'sequencing-product': 0,
                          'platform': 0, 'sequencer-model': 0, 'target-logical-amount': 0,
                          'logical-amount-units': 0, 'tight-insert': 0, 'target-fragment-size-bp': 0,
                          'target-insert-size-kb': 0, 'target-template-size-bp': 0,
                          'degree-of-pooling': 0, 'rna-protocol-option': 0, 'rrna-depletion': 0,
                          'read-total': 0, 'read-length-bp': 0, 'plate-target-mass-lib-trial-ng': 0,
                          'tube-target-mass-lib-trial-ng': 0, 'run-mode': 0, 'library-creation-specs': 0,
                          'library-creation-queue': 0, 'poly-a-selection': 0,
                          'itag-primer-set': 0}

    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          sp  -  the sequencing project ID under test
    def setURL(self, sp, server):
        requestURL = 'http://' + server + '/sequencing-projects/' + str(sp) + '/sow-item-templates'
        print("url = " + requestURL)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # verifyExpectedAttributes
    # verify that all the expected attributes are returned in response of WS call
    # input:  json string from WS call
    # ouput:  updates errorCnt (global)
    def verifyExpectedAttributes(self, jsonstr):
        for key, retrievedList in jsonstr.items():  # for each sow item template, check that  all attributes that are required (per contract) are in WS response
            if key == 'sow-item-templates':
                print("number of sow item templates = " + str(len(retrievedList)))
                print("")
                for sowItemTemplate in retrievedList:
                    print("sow-number: " + str(sowItemTemplate.get('sow-number')))
                    numOfAttributesNotFound = 0
                    for attributeName in self.expectedAttributes:
                        if attributeName not in sowItemTemplate:
                            print("*** Error. Not Found. Expecting attribute in sow Item Template = ", attributeName)
                            numOfAttributesNotFound = numOfAttributesNotFound + 1
                            self.errorCnt = self.errorCnt + 1
                    if numOfAttributesNotFound == 0:
                        print("       Found all expected attributes.")



    # -------------------------------------------------------------------------------------------
    # verifyReturnedAttributes
    # now go the other way and verify that all the returned  attributes are the same as the expected attributes (per contract)
    # input:  json string from WS call
    # ouput:  updates errorCnt (global)
    def verifyReturnedAttributes(self, jsonstr):
        for key, retrievedList in jsonstr.items():
            if key == 'sow-item-templates':
                print("number of sow item templates = " + str(len(retrievedList)))
                print("")
                for sowItemTemplate in retrievedList:
                    print("sow-number: " + str(sowItemTemplate.get('sow-number')))
                    numOfUnExpectedAttributesFound = 0
                    for attributeName in sowItemTemplate:
                        if attributeName not in self.expectedAttributes:
                            print("*** Error, Unexpected attribute returned in sowItemTemplate = ", attributeName)
                            numOfUnExpectedAttributesFound = numOfUnExpectedAttributesFound + 1
                            self.errorCnt = self.errorCnt + 1
                    if numOfUnExpectedAttributesFound == 0:
                        print("       No unexpected attributes.")



    # -------------------------------------------------------------------------------------------
    # doTests
    #
    # input:  server,sp
    # ouput:  updates errorCnt (global)
    def doTests(self, server, sp):
        wsTool = wsTools()
        url = self.setURL(sp, server)
        print("")
        print("--------start: sowItemTemplates WST tests---------")
        print("---- TEST 1: VALID REQUEST,   EXPECT STATUS = 200 and JSON response ----")
        jsonStr = wsTool.checkforValidRequest(url)
        if jsonStr == {}:    # if no connection  or cand not get jsonstr, exit
            self.errorCnt=1
            #exit()
        else:
            # TEST 2: verify that all the expected attributes are returned in response of WS call
            print(" ")
            print("---- TEST 2: VERIFY THAT ALL EXPECTED ATTRIBUTES are RETRIEVED in response of WS call ----")

            self.verifyExpectedAttributes(jsonStr)

            # TEST 3: VERIFY THAT all the RETURNED ATTRIBUTES are the same as the expected attributes
            print(" ")
            print(
                "---- TEST 3: VERIFY THAT all RETURNED ATTRIBUTES are same as expected attributes ----")
            self.verifyReturnedAttributes(jsonStr)

            # TEST 4 invalid sp-id , expect 404
            print(" ")
            print("---- TEST 4: verify INVALID REQUEST - invalid SP ID,   EXPECT STATUS = 404 ----")
            self.errorCnt += wsTool.checkForInvalidRequest(self.setURL(0, server))

            # TEST 5 missing spid , expect 404
            print(" ")
            print("---- TEST 5: verify INVALID REQUEST - No SP ID,   EXPECT STATUS = 404 ----")
            self.errorCnt +=  wsTool.checkForMissingSPID(self.setURL('', server))

            # TEST 6 using (not allowed)  POST, PUT,DELETE  methods , expect 405
            print(" ")
            print("---- TEST 6: METHOD NOT ALLOWED - for POST, PUT,DELETE ,   EXPECT STATUS = 405 ----")
            self.errorCnt += wsTool.checkForInvalidCommands(self.setURL(sp, server))

        return self.errorCnt


# -------------------------------------------------------------------------------------------
#get sowItemTemplates WS tests

# run tests
devServer = 'claritydev1.jgi-psf.org'
prdServer = 'clarityprd1.jgi-psf.org'
intServer = 'clarityint1.jgi-psf.org'
zuulServer = 'clarity-int01.jgi-psf.org:8181'
sp=1207398 #1208084

myTest = sowItemTemplatesWST()


errs = myTest.doTests(intServer,sp)

print(" ")
print("---Number of Errors Found = " + str(errs) + " ---")





#history -
# 6-30-17 this test was successful in finding 2 bugs with documentation
#      * created  PPS-3912 doc: retrieve sow-item templates for SP WS expected attribute not returned
#      * created PPS-3913 - doc: SowItemTemplates WS - UnExpected Returned status for invalid request(missing SP ID)
# 7-11-17 requirements doc have been updated to fix above problems, I have updated code to match document.  All tests pass
# 7-19-17 converted to run as unit tests modules. called from the testengine, this returns the number of errors found.