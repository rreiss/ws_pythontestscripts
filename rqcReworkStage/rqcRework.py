#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'

import json

import requests

from Utils.util_dbTools import dbTools
from Utils.util_udfTools import udfTools


#name:  rqcReworkWST  - RQC Rework Web Service Test
#code verifies info on requirements doc:  https://docs.google.com/document/d/11D5Tjxwxut93czXUWxyAdqn0dc30OCT7_7qpvkS95FY/edit#heading=h.42zovrvuu6af
#this program, runs the ws and verifies that the attributes returned in response are the same as documented in
#requirements doc.  Also checks that return status is correct when invalid request is given

#Regression PPS_4053 use runRQC_Rework_Generic(devServer,'validation','make-new-library','AUBZP',"2-143675")
#Regression PPS_4053 use runRQC_Rework_Generic(devServer,'validation','reschedule-pool','BSXBZ',"2-3600314")
#Regression PPS_4053 use runRQC_Rework_Generic(devServer,'validation','sequence-existing-library','AUBZP',"2-143675")
#Regression  PPS_4009 use runRegressionPPS_4009


class RqcRework():
    def __init__(self):
        self.errorCnt = 0
        self.printOn = True
        self.myDB = dbTools()
        self.myUDFs = udfTools()
        self.myDB.connect('int')
        self.apiServer = 'clarity-int01.jgi-psf.org'



        self. rqcRequestSubmission = {
            "submitted-by": 5981,
            "rqc-rework-info": [
                {
                    "abandon-sample": "N",
                    #"library-name": "BSXBZ",   #"AUBZP",
                    #"physical-run-unit-id": "2-3600314", #"2-143675",
                    #"revised-genome-size-mb": 0,
                    #"genome-size-estimation-method": "Library QC",
                    "abandon-library": "N",
                    "total-logical-amount-completed": 0,
                    "targeted-logical-amount": 0
                }]
            }
    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          sp  -  the sequencing project ID under test
    # i.e 'http://claritydev1.jgi-psf.org/sequencing-projects/' + str(spID) + '/sow-items'
    def setURL(self, action, server):
        requestURL = 'http://' + server + '/' + action
        print("url = " + requestURL)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # rqcPost
    #
    # inputs: requestURL
    #         requestJson  - the json record for request
    #         expectedStatus - if the test expects a success or failure on WS call

    def rqcPost(self, requestURL, requestJson, expectedStatus):
        mydata = json.dumps(requestJson)
        headers = {"Content-Type": "application/json", 'data': mydata}
        # Call REST API
        response = requests.post(requestURL, data=mydata, headers=headers)  # POST  request
        status = response.status_code
        wsResp = response.text
        if response.text:
            wsResp = json.loads(response.text)
            print("response:" + json.dumps(wsResp, indent=4, sort_keys=True))

        if status != expectedStatus:
            print("*** Error, Unexpected Returned Status = ", status)
            print("WS request status = " + str(status))
            self.errorCnt += 1

        return wsResp


    # -------------------------------------------------------------------------------------------
    # checkForWrongSowState
    #
    # input:  errorMsg - from response of WS  (string)
    # output:  true  - if error message indicated sow status in wrong state and  was changed
    #          false - otherwise

    def checkForWrongSowState(self, errorMsg):
         if 'sowItem:' in errorMsg:  # if the sow item status is wrong state, update it to the correct status and
            errorMsgList = errorMsg.split(' ')  # make a list of the words of errormsg
            # to find the sowitem ID, 1st need to locate it
            sowItemString = errorMsgList[
                errorMsgList.index('[sowItem:') + 1]  # got it, but it still has a ',' on end of it
            # get rid of comma by spliting the string again
            sowItemID = (sowItemString.split(','))[0]  # now we have it.
            statement = "update uss.dt_sow_item set current_status_id=12 where sow_item_id in (" + sowItemID + ")"

            self.myDB.doUpate(statement)
            print("sow: " + sowItemID + " status updated!")
            self.errorCnt = self.errorCnt - 1
            return True
         else:
            return False

    # -------------------------------------------------------------------------------------------
    # runRQC_Rework_Generic
    #
    # input:  server
    #         actionType = 'validation' or 'action'
    #         function = 'reschedule-pool', 'make-new-library',
    #         lib = library name
    #         pru = pyhsical run unit

    def runRQC_Rework_Generic(self, server, actionType, function,lib, pru):
        if actionType == 'action':
            expectedStatus = 204
        else:   #for 'action'
            expectedStatus = 201
        rqcReworkCmd = 'rqc-rework-' + actionType + '/' + function  # set up command
        url = self.setURL(rqcReworkCmd, server)
        print('RQC WS test:  run ' + function + ' ' + actionType, "\n")
        submissionJson = self.rqcRequestSubmission  # get submission json

        submissionJson["rqc-rework-info"][0]["library-name"] = lib
        submissionJson["rqc-rework-info"][0]["physical-run-unit-id"] = pru

        wsResp = self.rqcPost(url, submissionJson, expectedStatus)  # call WS POST,response in dictionary form

        if 'errors' in wsResp:  # if error, check if sow status is bad state, if so, change it
            if self.checkForWrongSowState(wsResp['errors'][0]['message']):
                # try posting again
                wsResp = self.rqcPost(url, submissionJson,
                                      expectedStatus)  # call WS POST,response in dictionary form
        return wsResp
    # -------------------------------------------------------------------------------------------
    # runRQC_Rework_Inquiry  - runs the inquiry command and does the actions that are returned
    #
    # input:  server
    #         lib = library name
    #         pru = pyhsical run unit

    def runRQC_Rework_InquiryAndDo(self, server, lib, pru):
        print ("\n-----Inquiry and DO using", lib, pru)
        errCntSoFar = self.errorCnt
        wsResp = self.runRQC_Rework_Generic(server, 'inquiry', ' ', lib, pru)
        if self.errorCnt == errCntSoFar:  # no new errors, proceed
            allowActions = wsResp['rqc-rework-actions'][0]['allowed-actions']
            for actionDict in allowActions:
                print (actionDict['lab-action'])
                self.runRQC_Rework_Generic(server, 'action', actionDict['lab-action'], lib, pru)

    # -------------------------------------------------------------------------------------------
    # runRQC_Rework_SowItemComplete  - sets the sowitem to status
    #
    # input:  server
    #         action - 'validation' or 'action'
    #         sowstatus - desired new status of sow
    #         lib = library name
    #         pru = pyhsical run unit

    def runRQC_Rework_SowItemComplete(self,server,action, sowstatus,  lib, pru):
        print("\n-----Change sow status", sowstatus, lib, pru)
        submissionJson = self.rqcRequestSubmission  # get submission json
        submissionJson["rqc-rework-info"][0]["sow-item-complete"] = sowstatus
        errCntSoFar = self.errorCnt
        wsResp = self.runRQC_Rework_Generic(server, action, '', lib, pru)
        if self.errorCnt == errCntSoFar:  # no new errors, proceed
            # verify sow status was changed in db
            sowItem = self.getSowItemFromLibUDFs(lib)   #pull out the sow item id associated with the library
            print ('sowID=',sowItem)

            actSowStatus = self.myDB.getStatus('sow', sowItem)  #actual sow status
            print ('sow status =',actSowStatus)
            if actSowStatus!=sowstatus:
                print("*** Error, Sow Status not updated to ", sowstatus)
                print("Actual sow status (in DB) = " + str(actSowStatus))
                self.errorCnt += 1


    # -------------------------------------------------------------------------------------------
    # getSowItemFromLibUDFs  - runs the inquiry command and does the actions that are returned
    #
    # input:
    #         lib = library name
    # output:  returns sow item id

    def getSowItemFromLibUDFs(self,lib):
        sowID = 0
        print("\n-----get sow ID from lib", lib )
        url = self.myUDFs.setURLartifact(lib, self.apiServer)
        print("artifact base URL = ", url)

        if url:
            libUrl = self.myUDFs.connectToClarityAPIartifacts(url)
            print("lib  URL = ", libUrl)
            if libUrl:
                myUDFs = self.myUDFs.getArtifactUDFs(libUrl)
                #print("udfs retrieved:")
                #print(myUDFs)
                for key in myUDFs.keys():
                    if key:
                        keySplit = key.split(' ')
                        if keySplit[0] == 'SOW':
                            sowID = keySplit[4]
                            break

        return sowID


    # -------------------------------------------------------------------------------------------
    # doTests
    #
    # input:  server
    # ouput:  err - number of errors found

    def doTests(self, server):
        print("")
        print("--------start: RQC Rework WST tests---------")

        #self.runRQC_Rework_Generic(server, 'validation', 'make-new-library', 'BSXBZ',
        #                           "2-3600314")  # error, lib on plate not allowed
        #self.runRQC_Rework_Generic(server, 'validation', 'make-new-library', 'AUBZP', "2-143675")


        '''self.runRQC_Rework_Generic(server, 'validation', 'reschedule-pool', 'BSXBZ', "2-3600314")
        self.runRQC_Rework_Generic(server, 'validation', 'sequence-existing-library', 'AUBZP', "2-143675")
        self.runRQC_Rework_Generic(server, 'inquiry', '', 'AUBZP', "2-143675")
        self.runRQC_Rework_Generic(server, 'inquiry', '', 'BSXBZ', "2-3600314")
        self.runRQC_Rework_Generic(server, 'inquiry', '', 'AYUNC', "2-262195")
        self.runRQC_Rework_Generic(server, 'inquiry', '', 'AYUNC', "2-1349834")  # gls pru

        self.runRQC_Rework_InquiryAndDo(server, 'AYUNC', "2-1349834")  # run the inquiry and do all actions (gls pru)
        self.runRQC_Rework_InquiryAndDo(server, 'AYUNC', "2-262195")  # run the inquiry and do all actions
        self.runRQC_Rework_InquiryAndDo(server, 'BSXBZ', "2-3600314")
        self.runRQC_Rework_InquiryAndDo(server, 'AUBZP', "2-143675")
        self.runRQC_Rework_InquiryAndDo(server, 'BTPCT', "2-2393624")
        self.runRQC_Rework_InquiryAndDo(server, 'BYYXA', "2-2596666")
        self.runRQC_Rework_InquiryAndDo(server, 'BBTNZ', "2-1530407")  # DOP = 1'''
        self.runRQC_Rework_SowItemComplete(server, 'action', 'Complete', 'AUBZP', "2-143675")


        return self.errorCnt


# -------------------------------------------------------------------------------------------
# get Sow Items WS tests
# run tests
devServer = 'claritydev1.jgi-psf.org'
prdServer = 'clarityprd1.jgi-psf.org'
intServer = 'clarityint1.jgi-psf.org'

myRqcRework = RqcRework()
#myRqcRework.getInput()
#myRqcRework.setServer()  right now will be set to 'int'  implement choice later

for x in range(40):
    print("loop: ", x)
    myRqcRework.doTests(intServer)
    print ("")
print ("errors found = ", myRqcRework.errorCnt)





