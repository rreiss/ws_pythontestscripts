#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 11/13/17


import datetime
import json
import time

import requests

from Utils.util_dbTools import dbTools
from Utils.util_udfTools import udfTools
from Utils.wsTools import wsTools


# this will read info from log from the run of selenium process that goes thru sequencing.
# It will grab the pru number and use that to  run RQC tests...
# this is intended to run from jenkins.



class AutoTester_RQCrework():
    def __init__(self):
        self.printOn = False
        self.errorCnt = 0
        self.sowItemID = ""
        self.pool = ""
        self.glsPRUunitID = ""
        self.library = ""
        self.myDB = dbTools()
        self.myUDF = udfTools()
        self.url = ''
        self.serverName = 'int'
        self.server = 'clarityint.jgi-psf.org'
        self.apiDevServer = 'clarity-int01.jgi-psf.org'
        self.webDriverLogFile = 'C:\clarity_jar\log1.txt'

        self.rqcGenericSubmission = {
            "submitted-by": 5981,
            "rqc-rework-info": [
                {
                    "abandon-sample": "N",
                    # "library-name": "BSXBZ",   #"AUBZP",
                    # "physical-run-unit-id": "2-3600314", #"2-143675",
                    "revised-genome-size-mb": 66,
                    "genome-size-estimation-method": "Finished Genome",
                    #"sow-item-complete": "complete",
                    "abandon-library": "N",
                    "total-logical-amount-completed": 77,
                    "targeted-logical-amount": 99
                }]
        }
        self.wsAttrDbQueryMap = {
                    "revised-genome-size-mb":
                        "select tax.GENOME_SIZE_ESTIMATED_MB "
                        "from  uss.dt_sequencing_project sp "
                        "left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id "
                        "left join uss.DT_TAXONOMY_INFO tax on sp.TAXONOMY_INFO_ID = tax.TAXONOMY_INFO_ID "
                        "where sow.SOW_ITEM_ID = " ,
                    "genome-size-estimation-method":
                         "select  "
                         "cv.GENOME_SIZE_METHOD "
                         "from  uss.dt_sequencing_project sp "
                         "left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id "
                         "left join uss.DT_TAXONOMY_INFO tax on sp.TAXONOMY_INFO_ID = tax.TAXONOMY_INFO_ID "
                         "LEFT JOIN uss.DT_GENOME_SIZE_METHOD_CV cv ON cv.GENOME_SIZE_METHOD_ID = tax.GENOME_SIZE_METHOD_ID "
                         "where sow.SOW_ITEM_ID = " ,
                    "total-logical-amount-completed":
                        "select TOTAL_LOGICAL_AMOUNT_COMPLETED "
                        "from uss.dt_sow_item "
                        "where sow_item_id = ",
                    "targeted-logical-amount":
                        "select TARGET_LOGICAL_AMOUNT "
                        "from uss.dt_sow_item "
                        "where sow_item_id = ",
                    "sow-item-complete":
                        "select cv.status "
                         "from uss.dt_sow_item sow "
                         "LEFT JOIN uss.dt_sow_item_status_cv cv ON cv.STATUS_ID = sow.CURRENT_STATUS_ID "
                         "where sow.sow_item_id = "
        }

        # mapping of attributes of WS call and schedule Sample UDFS (the key is the attribute name from the ws call, the value is the UDF name)
        self.wsAttrSsamUDFMap = {'total-logical-amount-completed': 'Total Logical Amount Completed',
                                  'targeted-logical-amount': 'Target Logical Amount'
                                }

        self.expectResults = {
        "labAction": [
            {
                "action": "make-new-library",
                "workflowAssignment":
                    [
                        {
                            "entity": "schSam",
                            "workflow": "AC Sample Aliquot Creation"
                        }
                    ]
            },

            {
                "action": "qpcr-existing-library",
                "workflowAssignment":
                    [
                        {
                            "entity": "lib",
                            "workflow": "LQ Library qPCR"
                        }
                    ]
            },
            {
                "action": "use-existing-library-in-new-pool",
                "workflowAssignment":
                    [
                        {
                            "entity": "lib",
                            "workflow": "LP Pool Creation"
                        }
                    ]
            },
            {
               "action": "sequence-existing-library",
               "workflowAssignment":
                    [
                        {
                            "entity": "",
                            "workflow": ""
                        }
                    ]
            },
            {
                "action": "reschedule-pool",
                "workflowAssignment":
                    [
                        {
                            "entity": "pool",
                            "workflow": "SQ Cluster Generation Illumina HiSeq-1TB"
                        }
                    ]
            }
        ]
        }
    # -----------------------------------------------------
    # setServer
    # request from user which server to use
    # inputs:  server -  "prod"  from production , 'dev' for development
    #------------------------------------------------------------------------------------------

    def setServer(self):
     self.myDB.connect(self.serverName)
    # -------------------------------------------------------------------------------------------
    # setURLaction  for rqc rework WS ACTION function
    # build the URL
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          action   -  the rework lab action to be performed
    # i.e 'http://claritydev1.jgi-psf.org/rqc-rework-action/sequence-existing-library'

    def setURLaction (self, action):
        requestURL = 'http://' + self.server + '/' + 'rqc-rework-action/' + action
        print("url = " + requestURL)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # setURLvalidation  for rqc rework WS VALIDATION function
    # build the URL
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          action   -  the rework lab action to be performed
    # i.e 'http://claritydev1.jgi-psf.org/rqc-rework-validation/sequence-existing-library'

    def setURLvalidation(self, action):
        requestURL = 'http://' + self.server + '/' + 'rqc-rework-validation/' + action
        print("url = " + requestURL)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # setURLinquiry for rqc rework WS INQUIRY function
    # build the URL
    #
    # i.e 'http://claritydev1.jgi-psf.org/rqc-rework-inquiry'

    def setURLinquiry(self):
        self.rqcReworkRequestURL = 'http://' + self.server + '/' + 'rqc-rework-inquiry'
        print("url = " + self.rqcReworkRequestURL)


    # -------------------------------------------------------------------------------------------
    # rqcPost
    #
    # inputs: requestURL
    #         requestJson  - the json record for request
    #         expectedStatus - if the test expects a success or failure on WS call

    def rqcPost(self, requestURL, requestJson, expectedStatus):
        mydata = json.dumps(requestJson)
        print (mydata)
        headers = {"Content-Type": "application/json", 'data': mydata}
        # Call REST API
        response = requests.post(requestURL, data=mydata, headers=headers)  # POST  request
        status = response.status_code
        print (status,expectedStatus)
        wsResp = response.text
        if response.text:
            wsResp = json.loads(response.text)
            print("response:" + json.dumps(wsResp, indent=4, sort_keys=True))

        if status != expectedStatus:
            print("*** Error, Unexpected Returned Status = ", status)
            print("WS request status = " + str(status))
            self.errorCnt += 1

        return wsResp

    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'claritydev1.jgi-psf.org'
    # ------------------------------------------------------------------------------------------

    def setURLforPRUws(self, flowCellName):
        requestURL = 'http://' + self.server + '/physical-runs/physical-run-units/?container-barcode=' + flowCellName
        # i.e. 'http://claritydev1.jgi-psf.org/physical-runs/physical-run-units/?container-barcode=FC171107162803'
        print("url = " + requestURL)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # checkForWrongSowState
    #
    # input:  errorMsg - from response of WS  (string)
    # output:  true  - if error message indicated sow status in wrong state and  was changed
    #          false - otherwise

    def checkForWrongSowState(self, errorMsg):
        if 'sowItem:' in errorMsg:  # if the sow item status is wrong state, update it to the correct status and
            errorMsgList = errorMsg.split(' ')  # make a list of the words of errormsg
            # to find the sowitem ID, 1st need to locate it
            sowItemString = errorMsgList[
                errorMsgList.index('[sowItem:') + 1]  # got it, but it still has a ',' on end of it
            # get rid of comma by spliting the string again
            self.sowItemID = (sowItemString.split(','))[0]  # now we have it.
            statement = "update uss.dt_sow_item set current_status_id=12 where sow_item_id in (" + self.sowItemID + ")"

            self.myDB.doUpate(statement)
            print("sow: " + self.sowItemID + " status updated!")
            self.errorCnt = self.errorCnt - 1
            return True
        else:
            return False

    # -------------------------------------------------------------------------------------------
    # verifyDBchanges  - verify that the expected changes to the db took place
    #  "revised-genome-size-mb": 0,
    #   "genome-size-estimation-method": "Library QC",
    #   "total-logical-amount-completed": 0,
    #   "targeted-logical-amount": 0
    #
    # input:  use  info  from the WS request json  as  input
    # output:  update error count

    def verifyDBchanges(self):
        sowID = ""
        #get sowID
        if self.sowItemID:
            sowID = self.sowItemID
        else:    #get it from library artifcat UDF
            lib = self.library
            sowID = self.getSowItemFromLibUDFs()   #pull out the sow item id associated with the library
            print ('sowID=',sowID)
            self.sowItemID = sowID
        wsRequest = self.rqcGenericSubmission["rqc-rework-info"][0]
        self.verifyDBupdatedWithRequestedAttribues(wsRequest, sowID)
        #check sample status
        if self.rqcGenericSubmission["rqc-rework-info"][0]["abandon-library"] == "Y":
            self.verifyStatusChanges('sample','Abandoned')   #sample

    # -------------------------------------------------------------------------------------------
    # verifyStatusChanges  - verify that the expected status changes to the db took place
    #   if "abandon-sample" = "Y", then check that sample has been abandoned,
    #   "genome-size-estimation-method": "Library QC",
    #   "total-logical-amount-completed": 0,
    #   "targeted-logical-amount": 0
    #
    # input:  use  info  from the WS request json  as  input
    # output:  update error count

    def verifyStatusChanges(self,entity,status):
        if entity == "sample":
            # verify sow status was changed in db
            sampleID = ""  #get sample id
            actStatus = self.myDB.getStatus('sample', sampleID)  #actual sow status
            print ('sample status =',actStatus)
            if actStatus!=status:
                print("*** Error, sample Status was not updated to ", status)
                print("Actual sow status (in DB) = " + str(actStatus))
                self.errorCnt += 1

    # -------------------------------------------------------------------------------------------
    # checkTotalLogAmtCmpltd  - check that the value return from the db has been increased by the new value
    # the expected current value has been updated before DB read
    # input:
    # output: true  if error found, false otherwise

    def checkTotalLogAmtCmpltd(self,value):
        errorFound = True
        #valueFromDB = int(value)
        currentExpectedValue = int(self.currentTotLogAmtCmpltd)
        if int(value) == currentExpectedValue:
           return not errorFound

        else:
            return errorFound

    # -------------------------------------------------------------------------------------------
    # verifyDBupdatedWithRequestedAttribues
    # verify that the sow was updated with the values of the Request attributes.
    # input:
    #         wsRequest - the request Dict from WS call
    #         sowID - the sow  ID to get data from DB
    # ouput:  updates errorCnt (global)

    def verifyDBupdatedWithRequestedAttribues(self, wsRequest, sowID):
        if wsRequest:
            for wsAttr, query in self.wsAttrDbQueryMap.items():
                print('----', wsAttr, '----')
                if query and (wsAttr in wsRequest):
                    newQuery = query + str(sowID)
                    dbValue = self.myDB.doQuery(newQuery)
                    print("     DB value= ", dbValue, " Request value= ", str(wsRequest[wsAttr]))
                    if str(wsRequest[wsAttr]) != str(dbValue):
                        if wsAttr == "total-logical-amount-completed":
                            err = self.checkTotalLogAmtCmpltd(dbValue)
                        else:
                            err = True
                        if err:
                            print( "***ERROR!  DB and Request do not match!! !!!  WS attribute(" + wsAttr + ")=" +
                                  str(wsRequest[wsAttr]) + " !=  DB= " + str(dbValue))
                            self.errorCnt += 1
                        else:
                            print('    ' + wsAttr + ':  updated in DB')
                    else :
                        print('    ' + wsAttr + ':  values match DB')
                else:
                    print("*** Warning. field not found in wsRequest  !!! ")

        else:
            print("*** Error. WSrequest is null !!! ")
            self.errorCnt += 1


    # -------------------------------------------------------------------------------------------
    # getSchSampleUDF  - gets schedule sample UDFs and verifies that the values are updated
    # input: the udf field of schedule sample
    #
    # output: value of udf

    def getSchSampleUDF(self,udfField):
        udfValue = ''
        url = self.myUDF.setURLartifact(self.library, self.apiDevServer)
        # print("artifact base URL = ", url)
        if url:
            libUrl = self.myUDF.connectToClarityAPIartifacts(url)
            print("lib  URL = ", libUrl)
            if libUrl:
                myUDFs = self.myUDF.getSchSampleOfLibraryUDFs(libUrl)
                for key in myUDFs.keys():
                    #print (key)
                    if key == udfField:
                        udfValue = myUDFs.get(key)
                        print (key)
        return udfValue


    # -------------------------------------------------------------------------------------------
    # verifySchSampleUDFs
    # verify that the schedule sample udfs  was updated with the values of the Request attributes.
    # input:
    #         wsRequest - the request Dict from WS call

    # ouput:  updates errorCnt (global)

    def verifySchSampleUDFs(self):
        print("\n-----Verify Schedule sample UDFS of ", self.library)
        wsRequest = self.rqcGenericSubmission["rqc-rework-info"][0]
        if wsRequest:
            for wsAttr, udfField in self.wsAttrSsamUDFMap.items():
                print('----', wsAttr, '----')
                if udfField and (wsAttr in wsRequest):
                    udfValue = self.getSchSampleUDF(udfField)
                    print("     UDF value= ", udfValue, " Request value= ", str(wsRequest[wsAttr]))
                    if str(wsRequest[wsAttr]) != str(udfValue):
                        if wsAttr == "total-logical-amount-completed":
                            err = self.checkTotalLogAmtCmpltd(udfValue)
                        else:
                            err = True
                        if err:
                            print(
                                "***ERROR!  UDF and Request do not match!! !!!  WS attribute(" + wsAttr + ")=" +
                                str(wsRequest[wsAttr]) + " !=  UDF value= " + str(udfValue))
                            self.errorCnt += 1
                        else:
                            print('    ' + wsAttr + ':  updated in UDF')
                    else:
                        print('    ' + wsAttr + ':  values match UDF')
                else:
                    print("*** Warning. field not found in wsRequest  !!! ")

        else:
            print("*** Error. WSrequest is null !!! ")
            self.errorCnt += 1

    # -------------------------------------------------------------------------------------------
    # getSowItemFromLibUDFs  - gets the sow item id from library artifact udf (clarity api)
    # input:
    #         lib = library name
    # output:  set  self.sowItemID

    def getSowItemFromLibUDFs(self):
        lib = self.library
        sowID = ""
        print("\n-----get sow ID from lib", lib)
        url = self.myUDF.setURLartifact(lib, self.apiDevServer)
        # print("artifact base URL = ", url)
        if url:
            libUrl = self.myUDF.connectToClarityAPIartifacts(url)
            print("lib  URL = ", libUrl)
            if libUrl:
                myUDFs = self.myUDF.getArtifactUDFs(libUrl)
                for key in myUDFs.keys():
                    if key:
                        keySplit = key.split(' ')
                        if keySplit[0] == 'SOW':
                            sowID = keySplit[4]
                            break

        self.sowItemID = sowID

    # -------------------------------------------------------------------------------------------
    # verifyQueueMovement   (for library)
    # verify that the entity (i.e. lib or scheduled sample) was moved to correct queue.
    # input:  labaction of WS
    #

    # ouput:    updates errorCnt (global)

    def verifyQueueMovement(self, myAction):
        queuedList = []
        print ("Verifying queue movement")
        expectResultJson = self.expectResults
        labAction = expectResultJson['labAction']

        for i in range(len(labAction)):   #check the expected results
            action = labAction[i]['action']
            # workflowAssignment = expectResultJson['labAction'][x]['workflowAssignment'][0]['workflow']
            workflowAssignment = labAction[i]['workflowAssignment'][0]['workflow']
            entity = labAction[i]['workflowAssignment'][0]['entity']

            if myAction == action:
                print("expected results for ", action)
                print("the ", entity, " must be in queue: ", workflowAssignment)
                if entity == 'lib':
                    lib = self.library
                    url = self.myUDF.setURLartifact(lib, self.apiDevServer)
                    if url:
                        libUrl = self.myUDF.connectToClarityAPIartifacts(url)
                        print("lib  URL = ", libUrl)
                        if libUrl:
                           queuedList = self.myUDF.getWorkflowsFromArtifacts(libUrl, "QUEUED")
                           if workflowAssignment in queuedList:
                                print("Success! library is properly queued")
                           else:
                                print("*** ERROR! library is not in EXPECTED queue")
                                self.errorCnt += 1
                elif entity == 'schSam':
                    lib = self.library
                    url = self.myUDF.setURLartifact(lib, self.apiDevServer)
                    if url:
                        libUrl = self.myUDF.connectToClarityAPIartifacts(url)
                        print("lib  URL = ", libUrl)
                        if libUrl:
                            schSampleArtifactsURL = self.myUDF.getSchSampleArtifactUrl(libUrl)
                            print ("the schedule sample artifact URL is: ",schSampleArtifactsURL )
                            queuedList = self.myUDF.getWorkflowsFromArtifacts(schSampleArtifactsURL, "QUEUED")
                            if workflowAssignment in queuedList:
                                print("Success! scheduled sample is properly queued")
                            else:
                                print("*** ERROR! scheduled sample  is not in EXPECTED queue")
                                self.errorCnt += 1
                elif entity == 'pool':
                    pool = self.pool
                    url = self.myUDF.setURLartifact(pool, self.apiDevServer)
                    if url:
                        poolURL = self.myUDF.connectToClarityAPIartifacts(url)
                        print("pool URL = ", poolURL)
                        if poolURL:
                            queuedList = self.myUDF.getWorkflowsFromArtifacts(poolURL, "QUEUED")
                            if workflowAssignment in queuedList:
                                print("Success! Pool is properly queued")
                            else:
                                print("*** ERROR! Pool is not in EXPECTED queue")
                                self.errorCnt += 1
                else:
                    print("not defined")


    # -------------------------------------------------------------------------------------------
    # getTotalLogAmtCmpltd  - get the value of the field TOTAL_LOGICAL_AMOUNT_COMPLETED from
    # uss.dt_sow_item.   Store in class attribute
    # input:  none
    #
    # output:
    def setCurrTotalLogAmtCmpltd(self):
        # get the current value of TOTAL_LOGICAL_AMOUNT_COMPLETED from uss.dt_sow_item
        query = "select  TOTAL_LOGICAL_AMOUNT_COMPLETED from uss.dt_sow_item where SOW_ITEM_ID = " + self.sowItemID
        self.currentTotLogAmtCmpltd = self.myDB.doQuery(query)
        print ("setting new current TOTAL_LOGICAL_AMOUNT_COMPLETED value=",self.currentTotLogAmtCmpltd)



    # -------------------------------------------------------------------------------------------
    # populateData  - populate the RQC fields for use in WS  and DB calls
    # sets the object fields:
    # input:
    #         jsonStr
    # output:

    def populateData(self, jsonStr):
        # populate RQC data fields

        #robot.populateData(jsonStr)
        pruJson = jsonStr['physical-run-units'][0]
        self.pool = pruJson['pooled-library-name']
        self.glsPRUunitID = pruJson['gls-physical-run-unit-id']
        self.library = pruJson['pooled-libraries'][0]['library-name']
        self.getSowItemFromLibUDFs()  # pull out the sow item id associated with the library
        if (self.sowItemID) != "" :
            #set the current value of TOTAL_LOGICAL_AMOUNT_COMPLETED from uss.dt_sow_item
            self.setCurrTotalLogAmtCmpltd()
        else:
            print("**** Error!  No sow item ID found in Library UDFs")
            self.errorCnt += 1

        print('sowID=', self.sowItemID)
        print('current Tot Log Amt Cmptd =', self.currentTotLogAmtCmpltd)
        print ('pool=',self.pool)
        print('gls PRU unit ID=',self.glsPRUunitID)
        print('Library =',self.library)

        # build json for RQC rework WS request using library name  and gls physical run unit id
        submissionJson = self.rqcGenericSubmission  # get submission json
        submissionJson["rqc-rework-info"][0]["library-name"] = self.library
        submissionJson["rqc-rework-info"][0]["physical-run-unit-id"] = self.glsPRUunitID
        #print (submissionJson)
        self.setURLinquiry()




    # -------------------------------------------------------------------------------------------
    # Setup  - get flowcell from input file (stored in self.webDriverLogFile)
    # sets the object fields:
    # input:
    #         jsonStr
    # output:

    def setUP(self):
        self.setServer()
        # get output file of webdriver jarfile as read only
        # hard code name C:\clarity_jar\log1.txt
        # open file and get flowcell id.
        f = open(self.webDriverLogFile, 'r')
        flowCellName = ""
        for line in f:
            if 'flowcell: ' in line:  # search for "flowcell: "
                words = line.split()  # this line should be 'flowcell: FC171113132009'
                flowCellName = words[1]  # get the second word, save as flowCellName
                print(flowCellName)
                break
        #use the physical runs Ws to get the library, gls physical run unit and the pool
        url = self.setURLforPRUws(flowCellName)  #for physical runs Ws
        wsTool = wsTools()
        jsonStr = wsTool.runWS(url)  # call physical runs Ws  with parameter container-barcode=<flowCellName>
        if jsonStr == {}:  # if no connection  or can not get jsonstr, exit
            self.errorCnt = 1
        else:
            # populate RQC data fields from response of   Phys run units WS
            self.populateData(jsonStr)
# -------------------------------------------------------------------------------------------

# -------------------------------------------------------------------------------------------

# -------------------------------------------------------------------------------------------
# run tests
ts = time.gmtime()
print("Start ",time.strftime("%Y-%m-%d %H:%M:%S %ZPST", ts))
s = datetime.datetime.now().isoformat()
date = datetime.datetime.now().date()
time = datetime.datetime.now().time()
print(date, " ",time)
input("next")
robot = AutoTester_RQCrework()
robot.setUP()
if robot.errorCnt == 0 :
     # call RQC WS inquiry
    submissionJson = robot.rqcGenericSubmission
    wsResp = robot.rqcPost(robot.rqcReworkRequestURL, submissionJson,201)  # call WS POST,response in dictionary form
    print (wsResp)
    #get the allow actions
    allowActionsArray = wsResp["rqc-rework-actions"][0]["allowed-actions"]
    # print (allowActionsArray)
    for actionDict in allowActionsArray:
        allowedAction = actionDict['lab-action']
        print (allowedAction)
        url = robot.setURLaction(allowedAction)
        wsResp = robot.rqcPost(url, submissionJson, 204) #for each action, do action for RQC WS (use 204 for action, 201 for  validation)
        if 'errors' in wsResp:  # if error, check if sow status is in bad state, if so, change it and try again
            if robot.checkForWrongSowState(wsResp['errors'][0]['message']):
                 wsResp = robot.rqcPost(url, submissionJson, 204)  #  # try posting again
        if 'errors' not in wsResp:
            robot.setCurrTotalLogAmtCmpltd() #update the class attribute since this is used this value is used in caluculation with each submission
            print(submissionJson)
            #verify DB changes
            robot.verifyDBchanges()
            # verify udf changes  - check for targeted-logical-amount and total-logical-amount-completed in scheduled sample udf
            robot.verifySchSampleUDFs()
            # verify queue movement
            robot.verifyQueueMovement(allowedAction)
        input("contloop?")




print("---Number of Errors Found = " + str(robot.errorCnt) + " ---")
