#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 8/16/17




# name:  jsonInput  -  storage for the json requests for WS,

class  jsonInput():

    def __init__(self):
        self.printOn = True

        self.errorCnt = 0


        #-------use these for add sow WS -------------------------------------------
        #these are all the attributes that can be updated on put command. Use this or subset
        self.addSowToExistSamJson = {  # ok
                                       "created-by-cid": 5981,
                                       "sequencing-project-id": 1204109,
                                       "sow-item-purpose": "RnD",
                                       "sample-id": 75410,
                                       "sow-number": 135,
                                       "target-logical-amount": 4,
                                       "logical-amount-units": "reads per barcode",
                                       "degree-of-pooling": 10,
                                       "run-mode": "PacBio RS 1 X 120" ,
                                       "sm-comments": "random sm comments",
                                       "lc-comments": "random lc comment",
                                       "sq-comments": "random sq comment"
                                       }
        self.addSowToExistSamScenario115 = {  # ok
                                       "created-by-cid": 5981,
                                       "sequencing-project-id": 1204109,
                                       "sow-item-purpose": "Anticipated Planned Work",
                                       "sample-id": 75410,
                                       "sow-number": 135,
                                       "target-logical-amount": 4,
                                       "logical-amount-units": "reads per barcode",
                                       "degree-of-pooling": 10,
                                       "run-mode": "PacBio RS 1 X 120" ,
                                       "sm-comments": "random sm comments",
                                       "lc-comments": "random lc comment",
                                       "sq-comments": "random sq comment"
                                       }
        self.addSowToExistSamScenario110 = {  # ok
                                        "created-by-cid": 5981,
                                        "sequencing-project-id": 1204109,
                                        "sow-item-purpose": "Rework",
                                        "linked-sow-item-id": 344534,
                                        "sample-id": 166016,
                                        "sow-number": 135,
                                        "target-logical-amount": 88,
                                        "logical-amount-units": "X",
                                        "degree-of-pooling": 16,
                                        "run-mode": "PacBio RS 1 X 360"
                                         }

        self.addSowToNewSam =  {  #ok
                                               "created-by-cid": 5981,
                                               "sequencing-project-id": 1204109,
                                               "sow-item-purpose": "Unanticipated Additional Work",
                                               "sample-id": None,
                                               "sow-number": 180,
                                               "target-logical-amount": 3,
                                               "logical-amount-units": "M mappable reads",
                                               "degree-of-pooling": 2,
                                               "run-mode": "Illumina HiSeq-Rapid 2 X 150"
                                        }

        self.addSowToLib = {  #works but need to make sure all sows are in terminal state, this will create new sow in progress
                                           "created-by-cid": 5981,
                                           "sequencing-project-id": 1060423,
                                           "sow-item-purpose": "Unanticipated Additional Work",
                                           "library-stock-id": "2-133765",
                                           "sow-number": 255,
                                           "target-logical-amount": 1,
                                           "logical-amount-units": "M reads",
                                           "degree-of-pooling": 10,
                                           "run-mode": "Illumina HiSeq-Rapid 2 X 150",
                                           "qc-type":"Purity"
                                        }
        self.submissionAttrs4plate = {  #ok
                                           "created-by-cid": 5981,
                                           "sequencing-project-id": 1312970,
                                           "sow-item-purpose": "Unanticipated Additional Work",
                                           "sample-id": 243117,
                                           "sow-number": 280,
                                           "target-logical-amount": 55,
                                           "logical-amount-units": "M mappable reads",
                                           "degree-of-pooling": 48,
                                           "run-mode": "Illumina NextSeq-HO 2 X 150",
                                           "qc-type": "Quantity+Quality+Purity",
                                           "run-mode": "PacBio RS 1 X 120",
                                           "sm-comments": "77777777y",
                                           "lc-comments": "yyyyyyyyyyyyyyh",
                                           "sq-comments": "hhhhhhhhhh"
                                       }


        self.submissionAttrs4singleCellSam = {  #
            "created-by-cid": 5981,
            "sequencing-project-id": 1343549,
            "sow-item-purpose": "Anticipated Planned Work",
            "sample-id": 252318,
            "sow-number": 319,
            "target-logical-amount": 4,
            "logical-amount-units": "reads per barcode",
            "degree-of-pooling": 10,
            "run-mode": "Illumina NextSeq-HO 2 X 150",
            "sm-comments": "random sm comments",
            "lc-comments": "random lc comment",
            "sq-comments": "random sq comment"
        }

        self.submissionAttrs_regression_bad_message = {  #bad message refers to scheduling sow that are not on site (single cell internal
                                            "created-by-cid": 5981,
                                            "sequencing-project-id": 1294300,
                                            "sow-item-purpose": "Rework",
                                            "linked-sow-item-id": 291278,
                                            "sample-id": 234422,
                                            "sow-number": 334,
                                            "target-logical-amount": 88,
                                            "logical-amount-units": "X",
                                            "degree-of-pooling": 16,
                                            "run-mode": "Illumina HiSeq-1Tb 1 X 100"
                                            }


        self.regression_PPS4021 = { #regression testing, uses inactive run mode
                                            "created-by-cid": 5981,
                                            "sequencing-project-id": 1193149,
                                            "sow-item-purpose": "Unanticipated Additional Work",
                                            "sample-id": None,
                                            "sow-number": 180,
                                            "target-logical-amount": 3,
                                            "logical-amount-units": "M mappable reads",
                                            "degree-of-pooling": 2,
                                            "run-mode": "Illumina HiSeq-HO 2 X 150"
                                            }

        self.regression_PPS3983 = {  # for regression testing, uses inactive run mode
                                            "created-by-cid": 5981,
                                            "sequencing-project-id": 1339439,
                                            "sow-item-purpose": "Unanticipated Additional Work",
                                            "sample-id": 251091,
                                            "sow-number": 181,
                                            "target-logical-amount": 3,
                                            "logical-amount-units": "M mappable reads",
                                            "degree-of-pooling": 2,
                                            "run-mode": "Illumina HiSeq-HO 2 X 150",
                                            "exome-capture-probe-set": "140604_Maize_SK_EZ_HX3"
                                            }

        #----------------------------------------------------------------------------------------------------
        #---------- use these for sequencing project submission to get different types of sp projects created


        self.spSubmissionAttributesGeneric = {
            "submitted-by-cid": 5981,
            "sequencing-projects": [
                {
                    "final-deliverable-project-id": 1091346,   # clarity int FD = 1226772,
                    "sequencing-project-name": "MicrobialMinimalDraftIsolate_Auto Testing rjr ",
                    "sequencing-product-id": 1,
                    "sequencing-strategy-id": 1,
                    "expected-number-of-samples": 1,
                    "eligible-for-public-release": "Y",
                    "sequencing-project-manager-cid": 5981,
                    "sequencing-project-contact-cid": 1717,
                    "sample-contact-cid": 5981,
                    "embargo-days": 0,
                    "auto-schedule-sow-items": "Y",
                    "sequencing-project-comments-and-notes": "testing sp submission with autotest program",
                    "existing-jgi-taxonomy-id": 1492
                }
            ]
        }

        self.spSubmissionAttributesMicrobialMinimalDraftIsolate = {
            "submitted-by-cid": 5981,
            "sequencing-projects": [
                {
                    "final-deliverable-project-id": 1091346,
                    "sequencing-project-name": "MicrobialMinimalDraftIsolate_Auto Testing rjr ",
                    "sequencing-product-id": 1,
                    "sequencing-strategy-id": 1,
                    "expected-number-of-samples": 1,
                    "eligible-for-public-release": "Y",
                    "sequencing-project-manager-cid": 5981,
                    "sequencing-project-contact-cid": 1717,
                    "sample-contact-cid": 5981,
                    "embargo-days": 0,
                    "auto-schedule-sow-items": "Y",
                    # "qc-type": "Quality",
                    # "target-logical-amount": 10,
                    # "logical-amount-units": "M reads",     #valid if target-logical-amount is valid
                    # "degree-of-pooling": 1,
                    # "run-mode": "Illumina HiSeq-1Tb 1 X 150",
                    "sequencing-project-comments-and-notes": "testing sp submission with autotest program",
                    "existing-jgi-taxonomy-id": 1492
                }
            ]
        }


        self.spSubmissionAttributesLCMSLipidomics = {
            "submitted-by-cid": 5981,
            "sequencing-projects": [
                {
                    "final-deliverable-project-id": 1091346,
                    "sequencing-project-name": "LCMSLipidomics_Auto Testing rjr ",
                    "sequencing-product-id": 81,
                    "sequencing-strategy-id": 99,
                    "expected-number-of-samples": 1,
                    "eligible-for-public-release": "Y",
                    "sequencing-project-manager-cid": 5981,
                    "sequencing-project-contact-cid": 1717,
                    "sample-contact-cid": 5981,
                    "embargo-days": 0,
                    "auto-schedule-sow-items": "Y",
                    # "qc-type": "Quality",
                    # "target-logical-amount": 10,
                    # "logical-amount-units": "M reads",     #valid if target-logical-amount is valid
                    # "degree-of-pooling": 1,
                    # "run-mode": "Illumina HiSeq-1Tb 1 X 150",
                    "sequencing-project-comments-and-notes": "testing sp submission with autotest program",
                    "existing-jgi-taxonomy-id": 1492
                }
            ]
        }


        self.spSubmissionAttributesRNARnD = {
            "submitted-by-cid": 5981,
            "sequencing-projects": [
                {
                    "final-deliverable-project-id": 1091346,
                    "sequencing-project-name": "RNARnD_Auto Testing rjr ",
                    "sequencing-product-id": 57,
                    "sequencing-strategy-id": 65,
                    "expected-number-of-samples": 1,
                    "eligible-for-public-release": "Y",
                    "sequencing-project-manager-cid": 5981,
                    "sequencing-project-contact-cid": 1717,
                    "sample-contact-cid": 5981,
                    "embargo-days": 0,
                    "auto-schedule-sow-items": "Y",
                     "qc-type": "Quantity+Quality+smRNA",
                     "target-logical-amount": 10,
                     "logical-amount-units": "M reads",     #valid if target-logical-amount is valid
                     "degree-of-pooling": 1,
                     "run-mode": "Illumina HiSeq-1Tb 1 X 150",
                    "sequencing-project-comments-and-notes": "testing sp submission with autotest program",
                    "existing-jgi-taxonomy-id": 1492
                }
            ]
        }


        self.spSubmissionAttributesExomeCapture = {
            "submitted-by-cid": 5981,
            "sequencing-projects": [
                {
                    "final-deliverable-project-id": 1091346,
                    "sequencing-project-name": "ExomeCapture_Auto Testing rjr ",
                    "sequencing-product-id": 56,
                    "sequencing-strategy-id": 64,
                    "expected-number-of-samples": 1,
                    "eligible-for-public-release": "Y",
                    "sequencing-project-manager-cid": 5981,
                    "sequencing-project-contact-cid": 1717,
                    "sample-contact-cid": 5981,
                    "embargo-days": 0,
                    "auto-schedule-sow-items": "Y",
                    # "qc-type": "Quality",
                    # "target-logical-amount": 10,
                    # "logical-amount-units": "M reads",     #valid if target-logical-amount is valid
                    # "degree-of-pooling": 1,
                    # "run-mode": "Illumina HiSeq-1Tb 1 X 150",
                    "sequencing-project-comments-and-notes": "testing sp submission with autotest program",
                    "existing-jgi-taxonomy-id": 1492
                }
            ]
        }


        self.spSubmissionAttributesMetagenomeImprovedDraft = {
            "submitted-by-cid": 5981,
            "sequencing-projects": [
                {
                    "final-deliverable-project-id": 1091346,
                    "sequencing-project-name": "MetagenomeImprovedDraft_Auto Testing rjr ",
                    "sequencing-product-id": 16,
                    "sequencing-strategy-id": 16,
                    "expected-number-of-samples": 1,
                    "eligible-for-public-release": "Y",
                    "sequencing-project-manager-cid": 5981,
                    "sequencing-project-contact-cid": 1717,
                    "sample-contact-cid": 5981,
                    "embargo-days": 0,
                    "auto-schedule-sow-items": "Y",
                    # "qc-type": "Quality",
                    # "target-logical-amount": 10,
                    # "logical-amount-units": "M reads",     #valid if target-logical-amount is valid
                    # "degree-of-pooling": 1,
                    # "run-mode": "Illumina HiSeq-1Tb 1 X 150",
                    "sequencing-project-comments-and-notes": "testing sp submission with autotest program",
                    "existing-jgi-taxonomy-id": 1492
                }
            ]
        }
        self.spSubmissionAttributesFAIRESeq = {
            "submitted-by-cid": 5981,
            "sequencing-projects": [
                {
                    "final-deliverable-project-id": 1091346,
                    "sequencing-project-name": "FAIRESeq_Auto Testing rjr ",
                    "sequencing-product-id": 69,
                    "sequencing-strategy-id": 77,
                    "expected-number-of-samples": 1,
                    "eligible-for-public-release": "Y",
                    "sequencing-project-manager-cid": 5981,
                    "sequencing-project-contact-cid": 1717,
                    "sample-contact-cid": 5981,
                    "embargo-days": 0,
                    "auto-schedule-sow-items": "Y",
                    # "qc-type": "Quality",
                    # "target-logical-amount": 10,
                    # "logical-amount-units": "M reads",     #valid if target-logical-amount is valid
                    # "degree-of-pooling": 1,
                    # "run-mode": "Illumina HiSeq-1Tb 1 X 150",
                    "sequencing-project-comments-and-notes": "testing sp submission with autotest program",
                    "existing-jgi-taxonomy-id": 1492
                }
            ]
        }
        self.spSubmissionAttributesPlantTranscriptomeAnnotation = {
            "submitted-by-cid": 5981,
            "sequencing-projects": [
                {
                    "final-deliverable-project-id": 1091346,
                    "sequencing-project-name": "PlantTranscriptomeAnnotation_Auto Testing rjr ",
                    "sequencing-product-id": 33,
                    "sequencing-strategy-id": 33,
                    "expected-number-of-samples": 1,
                    "eligible-for-public-release": "Y",
                    "sequencing-project-manager-cid": 5981,
                    "sequencing-project-contact-cid": 1717,
                    "sample-contact-cid": 5981,
                    "embargo-days": 0,
                    "auto-schedule-sow-items": "Y",
                    # "qc-type": "Quality",
                    # "target-logical-amount": 10,
                    # "logical-amount-units": "M reads",     #valid if target-logical-amount is valid
                    # "degree-of-pooling": 1,
                    # "run-mode": "Illumina HiSeq-1Tb 1 X 150",
                    "sequencing-project-comments-and-notes": "testing sp submission with autotest program",
                    "existing-jgi-taxonomy-id": 1492
                }
            ]
        }


# ----------------------------------------------------------------------------------------------------
# ---------- use these for Sample submission  -----------------------------
        self.samSubmissionAttributesGeneric = {
            "samples": [
                {
                    "sample-format": "RNAstable",
                    "dnase-treated": "N",
                    "collaborator-concentration-ngul": 100.29,
                    "collaborator-volume-ul": 54.78,
                    "storage-solution": "MDA Reaction Buffer",
                    "starting-culture-axenic-strain-pure": "N",
                    "biosafety-material-category": "Bacteria",
                    "sample-isolation-method": "test method #111",
                    "sample-collection-date": "2012-10-01",
                    "sample-isolated-from": "my house"
                }
            ],
            "submitted-by-cid": 5981,
            "validate-only": False
            }