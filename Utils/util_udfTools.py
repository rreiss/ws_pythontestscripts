#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 7/25/17

from xml.etree import ElementTree as ET

import requests


#name:  udfValidation  -

class udfTools():

    # ---------project udfs--tools--------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request for clarity API
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          sp  -  the sequencing project ID under test
    def setURLproject(self,sp,server):
        requestURL = 'https://' + server + '/api/v2/projects?name=' + str(sp)
        print("Getting UDFs from: " + requestURL)  #https://clarity-dev01.jgi-psf.org/api/v2/projects?name=1012803
        #                             https: // claritydev01.jgi - psf.org / api / v2 / projects?name = 1012803
        return requestURL


    # -------------------------------------------------------------------------------------------
    # connectToClarityAPIprojects
    #
    # inputs: requestURL
    # outputs: url of SP UDFs

    def connectToClarityAPIprojects(self,requestURL):
        projURL = ''
        try:
            # r= requests.get(requestURL, timeout=2)
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=('rjr', 'rjr'), stream=True, timeout=2)
        except:
            # status = r.status_code
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            # print(r.text) #this is the xml of the project base record
            root = ET.fromstring(r.text)
            # print (root.tag)
            for project in root.iter('project'):
                # print("from connecttoclarity")
                # print(project.attrib)
                projURL = project.get('uri')
                # print("-")
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return projURL

    # -------------------------------------------------------------------------------------------
    # getProjectUDFs
    #
    # inputs: requestURL
    # outputs: url of SP UDFs

    def getProjectUDFs(self,requestURL):
        udfDict = {}
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=('rjr', 'rjr'), stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            # print(r.text) # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for node in root.iter():
                udfName = node.get('name')
                # print(udfName)
                udfValue = node.text
                # print(udfValue)
                udfDict[udfName] = udfValue
                # print('name= ' + str(root[i].get('name')) + ', value= '  +  str(root[i].text) )
                # print (root[i].text)


                # print (udfDict)

        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return udfDict


    # ---------sample udfs---tools-------------------------------------------------------------------------------

    def setURLsample(self,sampleId, server):
        requestURL = 'https://' + server + '/api/v2/samples?name=' + str(sampleId)
        print("Getting UDFs from: " + requestURL)  # https://clarity-dev01.jgi-psf.org/api/v2/samples?name=1012803
        #
        return requestURL

    # -------------------------------------------------------------------------------------------
    # connectToClarityAPIsamples
    #
    # inputs: requestURL
    # outputs: url of sampleUDFs
    def connectToClarityAPIsamples(self,requestURL):
        sampleURL = ''
        try:
            # r= requests.get(requestURL, timeout=2)
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=('rjr', 'rjr'), stream=True, timeout=2)

        except:
            # status = r.status_code
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            print("Valid Returned Status = ", status)
            #print(r.text) #this is the xml of the project base record
            root = ET.fromstring(r.text)
            #print (root.tag)
            for sample in root.iter('sample'):
                #print("from connecttoclarity")
                #print(sample.attrib)
                sampleURL = sample.get('uri')
                # print("-")
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return sampleURL


    # -------------------------------------------------------------------------------------------
    # getSampleUDFs
    #
    # inputs: url of the clarity sample API
    # outputs: a dictionary of UDFS  (name of udf: value of udf)
    def getSampleUDFs(self,requestURL):
        udfDict = {}
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=('rjr', 'rjr'), stream=True,timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            #print("Valid Returned Status = ", status)
            #print(r.text) # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for node in root.iter():
                udfName = node.get('name')
                #print(udfName)
                udfValue = node.text
                #print(udfValue)
                udfDict[udfName] = udfValue
                #print('name= ' + str(root[i].get('name')) + ', value= '  +  str(root[i].text) )
                #print (root[i].text)


                #print (udfDict)

        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return udfDict




    # ---------artifacts udfs---tools-------------------------------------------------------------------------------

    def setURLartifact(self,artifactName, server):
        requestURL = 'https://' + server + '/api/v2/artifacts?name=' + str(artifactName)
        print("Getting UDFs from: " + requestURL)  # https://clarity-dev01.jgi-psf.org/api/v2/artifacts?name=BXCCT
        #
        return requestURL

    # -------------------------------------------------------------------------------------------
    # connectToClarityAPIartifacts
    #
    # inputs: requestURL
    # outputs: url of artifacts
    def connectToClarityAPIartifacts(self,requestURL):
        artifactURL = ''
        try:
            # r= requests.get(requestURL, timeout=2)
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=('rjr', 'rjr'), stream=True, timeout=2)

        except:
            # status = r.status_code
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            print("Valid Returned Status = ", status)
            #print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            #print(root.tag)
            for art in root.iter('artifact'):
                # print("from connecttoclarity")
                #print(art.attrib)
                artifactURL = art.get('uri')
                # print(artifactURL)
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return artifactURL

    # -------------------------------------------------------------------------------------------
    # getArtifactUDFs
    #
    # inputs: requestURL
    # outputs: url of artifact UDFs
    def getArtifactUDFs(self,requestURL):
        udfDict = {}
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=('rjr', 'rjr'), stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            #print(r.text) # this is the xml of the project base record
            root = ET.fromstring(r.text)
            #self.getSchSampleOfLibraryUDFs(requestURL)  # put here just for debuggin
            #queuedList = self.getWorkflowsOfLibrary(requestURL,"QUEUED") # put here just for debuggin
            # print (queuedList)
            #input("waithere")
            for node in root.iter():
                udfName = node.get('name')
                # print(udfName)
                udfValue = node.text
                # print(udfValue)
                udfDict[udfName] = udfValue
                # print('name= ' + str(root[i].get('name')) + ', value= '  +  str(root[i].text) )
                # print (root[i].text)
                # print (udfDict)

        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return udfDict

    # -------------------------------------------------------------------------------------------
    # getSchSampleOfLibraryUDFs  get the UDFS of the scheduled sample related to the library
    #
    # inputs: Clarity API URL  of  entity (library ) artifact record
    # outputs: dict of UDFs from the scheudule sample related to the artifact
    def getSchSampleOfLibraryUDFs(self,libraryApiURL):
        schSampleUDFs = {}
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(libraryApiURL, headers=headers, auth=('rjr', 'rjr'), stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            #print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for sample in root.iter('sample'):
                sampleAttrib = sample.attrib
                schSampleURL = sampleAttrib.get("uri")
                print ("Getting Schedule Sample UDFs from ", schSampleURL)
                schSampleUDFs = self.getSampleUDFs(schSampleURL)

        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return schSampleUDFs


    # -------------------------------------------------------------------------------------------
    # getSchSampleArtifactUrl  get the URL  of the scheduled sample (related to the library) artifacts
    #
    # inputs: Clarity API URL  of  entity (library ) artifact record
    # outputs: dict of UDFs from the scheudule sample related to the artifact

    def getSchSampleArtifactUrl(self, libraryApiURL):
        schSampleArtifactUrl = ''
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(libraryApiURL, headers=headers, auth=('rjr', 'rjr'), stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            #print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for sample in root.iter('sample'):
                sampleAttrib = sample.attrib
                schSampleURL = sampleAttrib.get("uri")
                print("Getting Schedule Sample artifacts URL from ", schSampleURL)
            try:
                headers = {'Content-Type': 'application/xml'}
                r = requests.get(schSampleURL, headers=headers, auth=('rjr', 'rjr'), stream=True, timeout=2)
            except:
                print("Opps! Can't get the request, Time out")
                return {}
            status = r.status_code
            if status == 200:
                # print("Valid Returned Status = ", status)
                # print(r.text) # this is the xml of the project base record
                root = ET.fromstring(r.text)
                for artifact in root.iter('artifact'):
                    artifactAttrib = artifact.attrib
                    schSampleArtifactUrl = artifactAttrib.get("uri")
                    #print (schSampleArtifactUrl)
            else:
                print("*** Error, Unexpected Returned Status = ", status)
        else:
            print("*** Error, Unexpected Returned Status = ", status)
        return schSampleArtifactUrl

    # -------------------------------------------------------------------------------------------
    # getWorkflowsFromArtifacts get  a list of the workflow stages of the  entity artifacts page that are in certain status
    #
    # inputs: artifactApiURL = Clarity API URL  of  entity (library ) artifact record,
    #           statusSought = status  of workflow that you are looking for (i.e. "QUEUED")
    # outputs: list of the queues that the entity is in as the inputted status
    def getWorkflowsFromArtifacts(self, artifactApiURL,statusSought):
        queueList = []
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(artifactApiURL, headers=headers, auth=('rjr', 'rjr'), stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            # print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for workflow in root.iter('workflow-stage'):
                wfAttrib = workflow.attrib
                #print (wfAttrib)
                wfStatus = wfAttrib.get("status")
                wfqueue = wfAttrib.get("name")
                if wfStatus == statusSought:
                    queueList.append(wfqueue)
                    #print(wfStatus,wfqueue)
            #input("next")


        else:
            print("*** Error, Unexpected Returned Status = ", status)
        return queueList




    # history -
        # 8-2  created