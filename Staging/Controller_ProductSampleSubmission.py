#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 10/26/17


#from Staging.Stage_seqProjSubmissionWST import Stage_seqProjSubmissionWST
from Staging.HelperClasses import ProductInfo
from Staging.Sample import Sample
from Staging.SequencingProject import SequencingProject
from Utils.util_dbTools import dbTools


# this will request user input for product ID (from menu)
# uses that to create SP and submit SP data
# then submits data to sample created for SP
# this will create SP and sample and submit data for both
# will verify that the database and udfs are properly populated.



class Controller_ProductSampleSubmission():
    def __init__(self):
        self.printOn = False
        self.errorCnt = 0
        self.myDB = dbTools()
        self.url = ''
        self.serverName = 'dev'
        self.server = 'claritydev1.jgi-psf.org'
        self.maxProductNum = 20
        self.nextID = 1   #  used for auto looping to submit sps/samples of all product  types

        # these are all the attributes that can be updated on put command. Use this or subset

    # -------------------------------------------------------------------------------------------
    # getSPproduct
    # sets server from user choice
    # request product choice from user
    # outputs:  object of type ProductInfo
    #------------------------------------------------------------------------------------------

    def getSPproduct(self):
        self.setServer()
        #selectedProductinfo = productInfo()
        selectedProductinfo = self.getProductName('pickOne')
        strategyID = selectedProductinfo.strategyID
        return selectedProductinfo




    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'claritydev1.jgi-psf.org'
    #------------------------------------------------------------------------------------------

    def setURL(self, server):
        requestURL = 'http://' + server + '/sample-submission/'
        # requestURL = 'http://' + server + '/pps-1/v2/sample-submission/'
        return requestURL

    # -----------------------------------------------------
    # setServer
    # request from user which server to use,  also connects to DB
    # inputs:  server -  "prod"  from production , 'dev' for development
    #------------------------------------------------------------------------------------------

    def setServer(self):
        serverName = input("Is this for prod or dev: (type 'prod' or 'dev'): ")
        if serverName == 'dev':
            self.server = 'claritydev1.jgi-psf.org'
        elif serverName == 'prod':
            self.server = 'clarityprd1.jgi-psf.org'
        else:
            self.server = 'claritydev1.jgi-psf.org'

        self.myDB.connect(serverName)

        print("-------------------------------------------\n")


    # ------------------------------------------------------------------------------------------
    # getProductName
    # input:  runtype  = 'pickOne'  or  'pickRange'
    #     if  'pickOne'  - user picks one from menu
    #     if  'pickRange'   -  user picks a range of product ids to use to create SPs/samples
    # output:   array of [productid,productname]
    #------------------------------------------------------------------------------------------

    def getProductName(self,runType):
        #get active sequencing product names from db

        selectedProductinfo = ProductInfo()
        query = "select SEQUENCING_PRODUCT_ID,SEQUENCING_PRODUCT_NAME,DEFAULT_SEQUENCING_STRATEGY_ID  from uss.DT_SEQUENCING_PRODUCT where active= 'Y'"
        productList = self.myDB.doQueryGetAllRowsCols(query)

        idList = []  #get the list of product ids so you can sort them
        for id in productList.keys():
            x = int(id)
            idList.append(x)

        idListSorted = sorted(idList)  #put in sorted list for easier viewing when displayed
        self.maxProductNum = idListSorted[-1]  #set the max product number

        if runType == 'pickOne':
            print("product names:")
            for x in idListSorted:   # print all product ids (and info)
                key = str(x)
                if key in productList:
                    print(key + ": " ,productList[key])

            selectedProductinfo = ProductInfo()  #create instance of product info
            answer = input("Which Product would you like to create? \n-------> Select #: ")
            if answer in productList:
                #print (productList[answer])
                selectedProductinfo.productName= productList[answer][0]
                selectedProductinfo.strategyID = productList[answer][1]
                selectedProductinfo.productID = answer
                #print (selectedProductinfo)
                #input("continue?")
                #selectedProduct[answer] =   selectedProductinfo #productList[answer]  #store product id, product name in dict to return
                #print("\nSelection: " ,answer , ": " ,selectedProduct[answer])
            else:
                print("\n*** ERROR *** Invalid Selection: " + answer + " - No active product with that ID")
                self.errorCnt += 1

        '''else: #runType == 'pickRange':
            next = str(self.nextID)
            if next in productList:
                selectedProduct[next] = productList[next]  # store product id, product name in dict to return
                #print("\nSelection: " + next + ": " ,selectedProduct[next])'''


        return selectedProductinfo




# -------------------------------------------------------------------------------------------
# run tests
doVerify = True
myController = Controller_ProductSampleSubmission()

#get SP product info to create and submit SP, requests user input of product id, sets up which server to work on
spProduct = myController.getSPproduct()

#create and submit SP  and verify
mySP = SequencingProject(myController.myDB,myController.serverName)
mySP.submitSP(spProduct,doVerify)   #this could create more than one,  fix to handle


#submit data for sample created by sp submission
mySample = Sample(myController.myDB,myController.serverName)
mySample.submitSample(mySP.spID,doVerify)

print("---Number of Errors Found = " + str(myController.errorCnt) + " ---")
