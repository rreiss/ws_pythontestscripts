#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 10/31/17


#from Staging.Stage_seqProjSubmissionWST import Stage_seqProjSubmissionWST


class ProductInfo:
    productID = 0
    productName = ""
    strategyID = 0

    def __repr__(self):
        return "productInfo()"

    def __str__(self):
        printString = "productID = " + str(self.productID) +  "; productName= " +  self.productName + "; strategyID= "+ str(self.strategyID)
        return printString
