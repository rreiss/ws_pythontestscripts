#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 7/25/17

import datetime
import json

import requests

from Staging.HelperClasses import ProductInfo
from Utils.util_dbTools import dbTools
from Utils.util_udfTools import udfTools

#name:  sequencingProject -  definition
''' class  SequencingProject
    methods: submitSP   -   submit data to create SP (and related entities)
             setURL     -   sets URL to be used for WS calls
             setServerAndURL  -   sets the server i.e."'claritydev1.jgi-psf.org'" for WS calls and connects to DB
                            and sets URL based on server
             spSubmissionPost - calls the webservice  for actual submission'''


class  SequencingProject:

    spID = 0
    printOn = True
    errorCnt = 0
    doVerification = False
    url = ''
    serverName = 'dev'
    server = 'claritydev1.jgi-psf.org'
    apiServer = 'clarity-dev01.jgi-psf.org'
    myDB = dbTools()
    productInfoSeed = ProductInfo()
    wsResp = {}
    spSubmissionAttributes = {
        "submitted-by-cid":
            5981,
        "sequencing-projects":
            [{
                "final-deliverable-project-id": 1091346,
                "sequencing-project-name": "MicrobialMinimalDraftIsolate_Auto Testing rjr ",
                "sequencing-product-id": 1,
                "sequencing-strategy-id": 1,
                "expected-number-of-samples": 1,
                "eligible-for-public-release": "Y",
                "sequencing-project-manager-cid": 5981,
                "sequencing-project-contact-cid": 1717,
                "sample-contact-cid": 5981,
                "embargo-days": 0,
                "auto-schedule-sow-items": "Y",
                # "qc-type": "Quality",
                # "target-logical-amount": 10,
                # "logical-amount-units": "M reads",     #valid if target-logical-amount is valid
                # "degree-of-pooling": 1,
                # "run-mode": "Illumina HiSeq-1Tb 1 X 150",
                "sequencing-project-comments-and-notes": "testing sp submission with autotest program",
                "existing-jgi-taxonomy-id": 1492
            }]
    }
    # Dict that maps UDF to the DB query that gets the value that populates the field
    # key = UDF name
    # value =  query string to get the value from DB, can be None
    #
    UDFdbQueryMap = {'AutoSchedule Sow Items': "SELECT CASE WHEN AUTO_SCHEDULE_SOW_ITEMS IN ('Y')  "
                                                    "THEN 'true' else 'false'   END "
                                                    "from uss.dt_sequencing_project  where SEQUENCING_PROJECT_ID = ",
                          'Sample Contact': None,
                          'Sequencing Project Name': 'select sequencing_project_name from  uss.dt_sequencing_project sp '
                                                     'where sp.SEQUENCING_PROJECT_ID =',
                          'Sequencing Project Manager Id': 'select SEQUENCING_PROJECT_MANAGER_ID from  uss.dt_sequencing_project sp '
                                                           'where sp.SEQUENCING_PROJECT_ID = ',
                          'Sample Contact Id': 'select sample_contact_id from  uss.dt_sequencing_project sp where sp.SEQUENCING_PROJECT_ID = ',
                          'Sequencing Project PI': None,
                          'Sequencing Product Name': 'select  sequencing_product_name  from  uss.dt_sequencing_project sp '
                                                     'left join uss.dt_sequencing_product prod on sp.actual_sequencing_product_id=prod.SEQUENCING_PRODUCT_ID  '
                                                     'where SEQUENCING_PROJECT_ID = ',

                          'Material Category': 'select  MATERIAL_category from  uss.dt_sequencing_project sp '
                                               'left join uss.DT_MATERIAL_TYPE_CV mat  on sp.material_type_id=mat.material_type_id '
                                               'where sp.SEQUENCING_PROJECT_ID = ',
                          'Sequencing Project PI Contact Id': 'select SEQUENCING_PROJECT_CONTACT_ID from  uss.dt_sequencing_project sp '
                                                              'where sp.SEQUENCING_PROJECT_ID = ',
                          'Sequencing Project Manager': None,
                          'Scientific Program': 'select SCIENTIFIC_PROGRAM from  uss.dt_sequencing_project sp '
                                                'left join uss.dt_account acct  on acct.ACCOUNT_ID = sp.DEFAULT_ACCOUNT_ID '
                                                'left join uss.DT_SCIENTIFIC_PROGRAM_CV sci on acct.SCIENTIFIC_PROGRAM_ID = sci.SCIENTIFIC_PROGRAM_ID '
                                                'where sp.SEQUENCING_PROJECT_ID = ',
                          'Proposal Id': 'select Proposal_ID from uss.dt_sequencing_project sp '
                                         'left join uss.DT_FINAL_DELIV_PROJECT fd  on fd.FINAL_DELIV_PROJECT_ID = sp.FINAL_DELIV_PROJECT_ID '
                                         'where sp.SEQUENCING_PROJECT_ID = '}

    # Dict that maps the ws Request attributes to the DB query of SP that gets the value that populates the field
    # key = UDF name
    # value =  query string to get the value from DB, can be None
    #

    wsAttrDbQueryMapSP = {
        'final-deliverable-project-id': 'select final_deliv_project_id from  uss.dt_sequencing_project  '
                                        'where SEQUENCING_PROJECT_ID =',
        'sequencing-product-id': 'select SEQUENCING_PRODUCT_ID from  uss.dt_sequencing_project  '
                                 'where SEQUENCING_PROJECT_ID =',
        'sequencing-project-manager-cid': 'select SEQUENCING_PROJECT_MANAGER_ID from  uss.dt_sequencing_project  '
                                          'where SEQUENCING_PROJECT_ID =',
        'sequencing-project-name': 'select SEQUENCING_PROJECT_NAME  from  uss.dt_sequencing_project  '
                                   'where SEQUENCING_PROJECT_ID =',
        'sequencing-project-comments-and-notes': 'select SEQUENCING_PROJECT_COMMENTS from  uss.dt_sequencing_project  '
                                                 'where SEQUENCING_PROJECT_ID =',
        'existing-jgi-taxonomy-id': 'select TAXONOMY_INFO_ID from  uss.dt_sequencing_project  '
                                    'where SEQUENCING_PROJECT_ID =',
        'expected-number-of-samples': 'select EXPECTED_NUM_SAMPLES from  uss.dt_sequencing_project  '
                                      'where SEQUENCING_PROJECT_ID =',
        'eligible-for-public-release': 'select ELIGIBLE_PUBLIC_RELEASE from  uss.dt_sequencing_project  '
                                       'where SEQUENCING_PROJECT_ID =',
        'embargo-days': 'select EMBARGO_DAYS from  uss.dt_sequencing_project sp  '
                        'left join uss.DT_EMBARGO_DAYS_CV em  on em.EMBARGO_DAYS_ID = sp.EMBARGO_DAYS_ID '
                        'where sp.SEQUENCING_PROJECT_ID =  ',
        'sequencing-project-contact-cid': 'select SEQUENCING_PROJECT_CONTACT_ID from  uss.dt_sequencing_project  '
                                          'where SEQUENCING_PROJECT_ID =',
        'sample-contact-cid': 'select SAMPLE_CONTACT_ID from  uss.dt_sequencing_project  '
                              'where SEQUENCING_PROJECT_ID =',
        'auto-schedule-sow-items': 'select AUTO_SCHEDULE_SOW_ITEMS from  uss.dt_sequencing_project  '
                                   'where SEQUENCING_PROJECT_ID =',
        'sequencing-strategy-id': 'select SEQUENCING_STRATEGY_ID from uss.dt_sequencing_project  '
                                  'where SEQUENCING_PROJECT_ID ='
    }
    # Dict that maps the ws Request attributes to the DB query of SoW that gets the value that populates the field
    # key = UDF name
    # value =  query string to get the value from DB, can be None
    #
    wsAttrDbQueryMapSow = {
        'qc-type': 'select qc_type from  uss.dt_sow_item sow '
                   'left join uss.dt_qc_type_CV qc on qc.qc_type_id = sow.qc_type_id '
                   'where sow.sow_item_ID =  ',
        'target-logical-amount': 'select target_logical_amount from  uss.dt_sow_item  '
                                 'where sow_item_ID = ',
        'logical-amount-units': 'select log.LOGICAL_AMOUNT_UNITS from  uss.dt_sow_item sow '
                                'left join uss.DT_LOGICAL_AMOUNT_UNITS_CV log '
                                'on log.LOGICAL_AMOUNT_UNITS_id = sow.LOGICAL_AMOUNT_UNITS_id '
                                'where sow.sow_item_ID =   ',
        'degree-of-pooling': 'select deg.DEGREE_OF_POOLING from  uss.dt_sow_item sow '
                             'left join uss.DT_DEGREE_OF_POOLING_CV deg '
                             'on deg.DEGREE_OF_POOLING_ID = sow.DEGREE_OF_POOLING_ID '
                             'where sow.sow_item_ID =  ',
        'run-mode': 'select RUN_MODE from  uss.dt_sow_item sow '
                    'left join uss.DT_RUN_MODE_CV run on run.RUN_MODE_ID = sow.RUN_MODE_ID '
                    'where sow.sow_item_ID =  ',

    }
    # Dict that maps the entity type with the query necessary to get the status from the DB
    # key = entity type ('sp', 'sow', 'sample'
    # value =  query string to get the status from DB, can be None
    #
    statusQueryMap = {'sp': "select status from uss.dt_sequencing_project sp "
                                 "left join uss.DT_SEQ_PROJECT_STATUS_CV cv on cv.status_id = sp.current_status_id "
                                 "where sp.SEQUENCING_PROJECT_ID = ",
                           'sow': "select status from  uss.dt_sow_item sow "
                                  "left join uss.DT_SOW_ITEM_STATUS_CV cv on cv.status_id = sow.current_status_id "
                                  "where sow.sow_item_ID = ",
                           'sample': None
                           }

    def __init__(self,database,serverName):
        self.myDB=database
        self.setServerAndURL(serverName)
    # -------------------------------------------------------------------------------------------
    # getNewSP  - creates sp, verifies udfs and database, returns spid
    # uses the spSubmissionAttributes Json to build request for WS
    # input:   selectedProduct dict(keys: productID,productname,strategyID))
    # ouput:  spID  of new SP created

    def submitSP(self,  selectedProduct, doVerification):
        print("\n--------start: submit sequencing project ---------")
        self.doVerification = doVerification
        self.productInfoSeed = selectedProduct
        mySubmissionAttribute = self.spSubmissionAttributes   #make a copy of submission to use for submission

        #update attributes
        #  make sure that you will always have a unique SP name by adding time stamp to base name
        timeNow = ' {:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
        productName = selectedProduct.productName

        mySubmissionAttribute["sequencing-projects"][0]["sequencing-project-name"] = productName + "_Auto Testing rjr " + timeNow
        mySubmissionAttribute["sequencing-projects"][0]["sequencing-product-id"] = selectedProduct.productID
        mySubmissionAttribute["sequencing-projects"][0]["sequencing-strategy-id"] = selectedProduct.strategyID

        self.wsResp = self.spSubmissionPost(self.url, mySubmissionAttribute)  # call WS POST,response in dictionary form

        spList = self.wsResp["sequencing-projects"]
        if len(spList) > 0:
            self.spID = spList[0]
            if  doVerification:
                self.verifyUDFsDBandStatus()
        else:
            print("*** Error!! No SP was created!!! ***")
            self.errorCnt += 1

        print ("\nSP ID = ", self.spID, ", Product ID = ", selectedProduct.productID,
               ", Product Name=",selectedProduct.productName, ", Strategy ID=",selectedProduct.strategyID,"\n")
        return self.spID  # will return the 1st sp of list if more than one (fix this)

    # -------------------------------------------------------------------------------------------
    # setURL
    # sets the URL for WS calls,  assumes self.server  is already set
    # inputs:  servername  i.e. 'dev'  or 'prod'
    #
    def setURL(self):
       self.url = 'http://' + self.server + '/sequencing-project-submission/'

    # -------------------------------------------------------------------------------------------
    # connectToDB
    # connects to DB based on servername
    # inputs:  servername  i.e. 'dev'  or 'prod'
    #
    def connectToDB(self, serverName):
        self.myDB.connect(serverName)


    # -------------------------------------------------------------------------------------------
    # setServerAndURL
    # set the server to be used  for ws call, connects to affliated database for server
    # sets the URL for WS calls
    # inputs:  servername  i.e. 'dev'  or 'prod'
    #
    def setServerAndURL(self, serverName):
        self.serverName= serverName
        if serverName == 'dev':
            self.server = 'claritydev1.jgi-psf.org'
            self.apiServer = 'clarity-dev01.jgi-psf.org'
        elif serverName == 'prod':
            self.server = 'clarityprd1.jgi-psf.org'
            self.apiServer = 'clarity-prd01.jgi-psf.org'
        else:
            self.server = 'claritydev1.jgi-psf.org'
            self.apiServer = 'clarity-dev01.jgi-psf.org'

        self.url = 'http://' + self.server + '/sequencing-project-submission/'




    # -------------------------------------------------------------------------------------------
    # spSubmissionPost
    #
    # inputs: requestURL
    def spSubmissionPost(self,requestURL,mySubmissionAttribute):
        mydata = json.dumps(mySubmissionAttribute)
        headers = {"Content-Type": "application/json", 'data':mydata}
        # Call REST API
        response = requests.post(requestURL, data=mydata, headers=headers)  #POST  request
        status = response.status_code
        if status != 201:
            print("*** Error, Unexpected Returned Status = ", status)
            self.errorCnt +=  1
            prettyJson = json.loads(response.text)
            print("WS request status = " + str(status))
            print("response:" + json.dumps(prettyJson, indent=4, sort_keys=True))
        else:
            # Print Response
            prettyJson = json.loads(response.text)
            #print (json.dumps(prettyJson, indent=4, sort_keys=True))
            return prettyJson


    # -------------------------------------------------------------------------------------------
    # verifyUDFs  - verifies udfs
    # uses the spSubmissionAttributes Json to build request for WS
    # input:   selectedProduct dict(keys: productID,productname,strategyID))
    # ouput:  spID  of new SP created

    def verifyUDFsDBandStatus(self):
        #spList = self.wsResp["sequencing-projects"]
        #if len(spList) > 0:
            #self.spID = spList[0]
        spAttrDict = self.spSubmissionAttributes['sequencing-projects'][0]
        #print("--- the requested attributes:", str(spAttrDict))
        print("-------- verify UDFS for SP = ", self.spID, " --------")
        spUDFs = self.getUDFs(self.spID, self.apiServer,
                              'project')  # get the udfs, return as a dict with UDF name as key, UDF value as value
        #print(spUDFs)
        # verify that the  UDFs are populated by DB values
        self.verifyUDFsArePopulatedByDB(spUDFs)
        # verify that the DB records were correctly created based on the WS request
        # print("\n--------verify database updated--------\n")
        self.verifyDBisPopulatedWithRequestedAttribues(spAttrDict)



    # -------------------------------------------------------------------------------------------
    # getUDFs
    #
    # inputs: sp, server, type (i.e. 'project', 'sample', etc)
    # outputs: UDFs as a dict
    def getUDFs(self, spID, apiServer, type):
        myUDFs = {}
        url = udfTools.setURLproject(spID, apiServer)
        if url:
            if type == 'project':
                projectUrl = udfTools.connectToClarityAPIprojects(url)
                if projectUrl:
                    myUDFs = udfTools.getProjectUDFs(projectUrl)
        return myUDFs

    # -------------------------------------------------------------------------------------------
    # verifyUDFsArePopulatedByDB
    # verify that the UDFS have the value stored in DB.
    #  input:  spUDF - the dict of UDFs for sp
    #
    # ouput:  updates errorCnt (global)

    def verifyUDFsArePopulatedByDB(self, spUDFs):
        if spUDFs:
            for udf, query in self.UDFdbQueryMap.items():
                print("UDF = ", udf)
                if query:
                    newQuery = query + str(self.spID)
                    dbValue = self.myDB.doQuery(newQuery)
                    print("      UDF value = " + str(spUDFs[udf]))
                    print("      DB value= ", dbValue)
                    if str(spUDFs[udf]) != str(dbValue):
                        print(
                            "***** UDF does not match DB !!!  UDF(" + udf + ")="
                            + str(spUDFs[udf]) + " !=  DB= " + str(dbValue))
                        self.errorCnt += 1
                    else:
                        print('      UDF and DB match!!!')
                else:
                    print('      skipped')
        else:
            print("*** Error. UDFs not retrieved !!! ")
            self.errorCnt += 1



    # -------------------------------------------------------------------------------------------
    # verifyDBisPopulatedWithRequestedAttribues
    # verify that the new sequencing project was created with the values of the Request attributes. Check both SP and sow DB tables
    # input:
    #         wsRequest - the request Dict from WS call
    #         sp - the SPID to get data from DB
    # ouput:  updates errorCnt (global)

    def verifyDBisPopulatedWithRequestedAttribues(self, wsRequest):
        if wsRequest:
            #print(wsRequest)

            # do SP fields
            print("-------- verify Requested Attributes were updated in DB (DT_SEQUENCING_PROJECT) --------")
            for wsAttr, query in self.wsAttrDbQueryMapSP.items():  # check sp fields
                print("Requested Attribute: ", wsAttr)
                if query:
                    if wsAttr in wsRequest:
                        newQuery = query + str(self.spID)
                        dbValue = self.myDB.doQuery(newQuery)
                        if str(wsRequest[wsAttr]) != str(dbValue):
                            print("     *** DB not updated!! !!!  WS attribute(" + wsAttr + ")=" +
                                  str(wsRequest[wsAttr]) + " !=  DB= " + str(dbValue))
                            self.errorCnt += 1
                        else:
                            print("     WS attribute= " + str(wsRequest[wsAttr]) + " ==  DB= " + str(dbValue))
                            #print('     ' + wsAttr + ':  values match DB')
                    else:
                        print('    skipped')

            print("\n--------verify SP statuses updated--------")
            expectedStatus = 'Awaiting Collaborator Metadata'
            # check sp
            self.verifyStatus(self.spID, 'sp', expectedStatus)

            print("\n-------- verify Requested Attributes were updated in DB (DT_SOW_ITEM) --------")
            # do sow fields
            sowQuery = 'select sow_item_id from  uss.dt_sow_item  where SEQUENCING_PROJECT_ID = ' + str(
                self.spID)  # 1062428
            sowList = self.myDB.doQueryGetAllRows(sowQuery)
            for sow in sowList:  # for each sow created for SP
                print("SOW ID = ", sow)
                for wsAttr, query in self.wsAttrDbQueryMapSow.items():  # check sp fields
                    print("Requested Attribute: ", wsAttr)
                    if query:
                        if wsAttr in wsRequest:
                            newQuery = query + str(sow)

                            dbValue = self.myDB.doQuery(newQuery)
                            print("     DB value= ", dbValue)
                            if str(wsRequest[wsAttr]) != str(dbValue):
                                print("     ***  Error.DB not updated!! !!!  WS attribute(" + wsAttr + ")=" +
                                      str(wsRequest[wsAttr]) + " !=  DB= " + str(dbValue))
                                self.errorCnt += 1
                            else:
                                print("     WS attribute= " + str(wsRequest[wsAttr]) + " ==  DB= " + str(dbValue))
                        else:
                            print('    skipped')
                print("--------verify Sow status  for ", str(sow))
                self.verifyStatus(sow, 'sow', expectedStatus)
        else:
            print("*** Error. Unknown error - no request !!! ")
            self.errorCnt += 1

    # -------------------------------------------------------------------------------------------
    # verifyStatus
    # verify that the statuses are set correctly in the DB
    # input:
    #         itemID - the itemID to get data from DB, i.e. spID, sowitem id, sample id
    #         entity - the entity that you want the status of (i.e. "sp","sow","sample")
    #         expectedStatus - the expected status.  i.e. 'Awaiting Collaborator Metadata'
    # ouput:  updates errorCnt (global)

    def verifyStatus(self, itemID, entity, expectedStatus):

        # check status
        query = self.statusQueryMap[entity]  # get the query to use based on entity passed
        if query:
            newQuery = query + str(itemID)
            status = self.myDB.doQuery(newQuery)
            if status != expectedStatus:
                print("**** Error! " + entity + " is in wrong State: " + status)
                self.errorCnt += 1
            else:
                print('current-status(' + entity + ') is correct:' + status)


# -------------------------------------------------------------------------------------------
#run tests
'''serverName = 'dev'

myProductSeed = ProductInfo()
myProductSeed.productID = 16
myProductSeed.productName = "Metagenome Improved Draft"
myProductSeed.strategyID = 16
print (myProductSeed)

mySP = SequencingProject()
mySP.setServerAndURL(serverName)
mySP.connectToDB(serverName)
spId = mySP.submitSP(myProductSeed)
print (spId)
mySP.verifyUDFsDBandStatus()'''



















