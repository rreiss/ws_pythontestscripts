#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
import unittest

'''from getSowItemTemplatesWST import sowItemTemplatesWST'''
from WStests.getSampleWST import getSampleWST
'''from getSowItemsWST import sowItemsWST
from rqcReworkWST import rqcReworkWST
from getSequencingProjectsWST import getSequencingProjectsWST
from putSequencingProjectsWST import putSequencingProjectsWST
from seqProjSubmissionWST import seqProjSubmissionWST'''
'''from addSowWST import addSowWST'''
'''from sampleSubmissionWST import sampleSubmissionWST
from util_dbTools import dbTools'''

"""runs regression tests from the following pps Web Services:
    addSow
    getSample
    getSequencingProject
    getSowItems
    getSowItemTemplates
    putSequencingProjects (update sequencing project)
    rqcRework
    sequencing project submission
    makes a new sample by calling sp submission, then sample submission
    """


class ppsWSTestSuite(unittest.TestCase):
    server = 'claritydev1.jgi-psf.org'

    # addSowWSTestCases
    '''def test_addSow(self):
        myTest = addSowWST()
        errs = myTest.doAllTests(self.server)
        print(" ")
        print("---Number of Errors Found = " + str(errs) + " ---")
        self.assertLessEqual(errs, 1, "Errors Found in add sow WS")'''

    # getSamplesWSTestCases
    def test_getSamples(self):
        # run tests
        sp = 1192122 #1104935   #1192122
        myTest = getSampleWST()
        errs = myTest.doTests(self.server, sp)
        self.assertLessEqual(errs, 0, "Errors Found in get samples")
        print("")
        print("---Number of Errors Found = " + str(errs) + " ---")

    # getSequencingProjectsWS TestCases
    '''def test_getSequencingProjectsWST(self):
        sp = 1366629
        myTest = getSequencingProjectsWST()
        errs = myTest.doTests(self.server, sp)
        print("")
        print("---Number of Errors Found = " + str(errs) + " ---")
        self.assertLessEqual(errs, 0, "Errors Found in (get)SequencingProjects")


    # getSowItemsWSTestCases
    def test_sowItems(self):
        print("test get sowItems WS with tubes")
        myTest =  sowItemsWST()
        errs = myTest.doTests(self.server,1289089) #1289093 ) #1192122

        print("test get sowItems WS with plate and tubes")
        errs = errs + myTest.doTests(self.server, 1359555)

        print("test get sowItems WS with plate")
        errs = errs + myTest.doTests(self.server, 1032449)

        print("")
        print("---Number of Errors Found = " + str(errs) + " ---")
        self.assertLessEqual(errs, 0, "Errors Found in sowItems")



    #getSowItemTemplatesWSTestCases(
    def test_sowItemTemplates(self):
        sp = 1359555;
        myTest = sowItemTemplatesWST()
        print("test get sowItem templates WS ")
        errs = myTest.doTests(self.server, 1192122)
        print(" ")
        print("---Number of Errors Found = " + str(errs) + " ---")
        self.assertLessEqual(errs, 0, "Errors Found in sowItem templates")


    # updates sequencing project and verifies udf and db
    # currently some mismatches that need to be corrected  8/22
    def test_putSequencingProjectsWST(self):
        apiDevServer = 'clarity-dev01.jgi-psf.org'
        sp = 1019606  # 1018639 this part of a plate
        myTest = putSequencingProjectsWST()
        errs = myTest.doTests(self.server, sp)
        print("")
        print("---Number of Errors Found = " + str(errs) + " ---")
        self.assertLessEqual(errs, 7, "Errors Found in (put) SequencingProjects")

    #rqcReworkWSTestCases
    def test_RqcRework(self):
        myTest = rqcReworkWST()
        errs = myTest.doTests(self.server)
        print(" ")
        print("---Number of Errors Found = " + str(errs) + " ---")
        self.assertLessEqual(errs, 3, "Errors Found in RQC Rework")

    # SeqProjSubmissionWSTestCases
    def test_SeqProjSubmission(self):
        myTest = seqProjSubmissionWST()
        errs = myTest.doTests(self.server)
        print(" ")
        print("---Number of Errors Found = " + str(errs) + " ---")
        self.assertLessEqual(errs, 0, "Errors Found in sequencing project submission")'''




    '''def test_SampleSubmission(self):    #do this for special cases, need to supply a sampleID that has not been submitted
        myTest = sampleSubmissionWST()
        sampleID = 303981   # change this every time

        errs = myTest.doTests(self.server, sampleID)
        print(" ")
        print("---Number of Errors Found = " + str(errs) + " ---")
        self.assertLessEqual(errs, 0, "Errors Found in sample submission")'''


    '''def test_makeSample(self):
        # creates a sample for clarity  by first creating sp (checks udf and db),
        # then submitting data for sample (checks udf and db)
        errs = 0
        #( possible productTypes: 'MinimalDraft','LCMSLipidomics','RNARnD','ExomeCapture','MetagenomeImprovedDraft','FAIRESeq','PlantTranscriptomeAnnotation')

        spSub= seqProjSubmissionWST()
        spID = spSub.getNewSP(self.server,'PlantTranscriptomeAnnotation')
        #get the sample created using query
        samSub = sampleSubmissionWST()
        myDB = dbTools()
        query = 'select sam.SAMPLE_ID from uss.dt_sequencing_project sp ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id ' \
                'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id ' \
                'where sp.SEQUENCING_PROJECT_ID = '

        query = query + str(spID)
        sampleID = myDB.doQuery(query)
        print ('\nthe sample obtained is: ', sampleID)
        if (sampleID == 'None'):
            print('*** Warning - no Sample created for this product')
        else:
            errs =  samSub.doTests(self.server, sampleID)
            print('\nthe sample  is: ', sampleID)

        print(" ")
        print("---Number of Errors Found = " + str(errs) + " ---")
        self.assertLessEqual(errs, 1, "Errors Found in sample submission")'''





'''def suite():

        test_suite = unittest.TestSuite()
        test_suite.addTests(unittest.makeSuite(sowItemsWSTestCases))
        test_suite.addTests(unittest.makeSuite(sowItemTemplatesWSTestCases))
        test_suite.addTests(unittest.makeSuite(sampleWSTestCases))
        return test_suite

mySuite = suite()
runner=unittest.TextTestRunner()
runner.run(mySuite)


suite = unittest.TestSuite()
suite.addTests(unittest.makeSuite(sowItemsWSTestCases))
suite.addTests(unittest.makeSuite(sowItemTemplatesWSTestCases))
suite.addTests(unittest.makeSuite(sampleWSTestCases))

result = unittest.TextTestRunner(verbosity=2).run(suite)'''


if __name__ == '__main__':
     unittest.main()


