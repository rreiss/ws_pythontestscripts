# My project's README
**WStests**-  this folder has individual 
a python program for individual PPS webservices.  
It also has a couple of special scripts:

--thresholdThreadsTest.py  - loops through the different 
WS python script to put stress on the system.

--zuulTesting - uses the zuul URL to run Web services.

**Utils** - folder to hold python scripts that are used by 
most other programs

**Staging** - not fully tested scripts (though TestJira works)

**SPandSampleSubmissionTool** - deprecated - does not use PMOS
 WS that will create APs
 
 rqcReworkSatge - code that was (though not up to date)
  used for auto testing RQC (set up in Jenkins)
