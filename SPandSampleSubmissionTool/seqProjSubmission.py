#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 7/25/17

import datetime
import json
from pprint import pprint
import random
import requests

from Utils.util_dbTools import dbTools
from Utils.util_jsonInput import jsonInput
from Utils.util_udfTools import udfTools


#making a few changes to make sure it work with Stage_spSampleSubmission
#status 1/11/18   working  but only input one product at time.


#name:  seqProjSubmissionWST  -  Sequencing Project submission  Web Service Test, test Post command  to create new SP
# per requirements doc: https://docs.google.com/document/d/1r-BHgfYo1jPJ-0IL7ynfWgZm0eBMOQmrDoPvfOVkCP4/edit#heading=h.87332r6flro9
# verify:
#1.  that a valid status is returned by WS request and new SP is created and the new record is populated with passed attributes
#2.  that the  sequencing strategy id (if  input parameter) is active and that the sequencing product and the strategy are valid tuple
#3.  if the sequencing strategy id is not an  input parameter verify that the set uss.dt_sequencing_project.sequencing_strategy_id to
#    the default sequencing strategy id associated with the sequencing product in the mapping table
#4.  that sequencing project in Clarity LIMS system  is created and the UDF fields are set as specified per requirements
#5. verifies that the database ( SP and Sow) is updated per requirements






class  Stage_seqProjSubmissionWST():

    def __init__(self):
        self.printOn = False
        self.errorCnt = 0
        self.myDB = dbTools()
        self.myUDF = udfTools()
        self.url = ''
        self.serverName = 'dev'
        self.server = 'claritydev1.jgi-psf.org'
        self.apiDevServer = 'clarity-dev01.jgi-psf.org'
        #self.myDB.connect(self.serverName)
        #self.webDriverLogFile = 'C:\clarity_jar\log1.txt'

        #the submission attributes json can be found in util_jsonInput.  There are several that can be used.

        # Dict that maps UDF to the DB query that gets the value that populates the field
        # key = UDF name
        # value =  query string to get the value from DB, can be None
        #
        self.UDFdbQueryMap = {'AutoSchedule Sow Items':   "SELECT CASE WHEN AUTO_SCHEDULE_SOW_ITEMS IN ('Y')  "
                                                                  "THEN 'true' else 'false'   END "
                                                                  "from uss.dt_sequencing_project  where SEQUENCING_PROJECT_ID = ",
                              'Sample Contact': None,
                              'Sequencing Project Name':  'select sequencing_project_name from  uss.dt_sequencing_project sp '
                                                          'where sp.SEQUENCING_PROJECT_ID =',
                              'Sequencing Project Manager Id':  'select SEQUENCING_PROJECT_MANAGER_ID from  uss.dt_sequencing_project sp '
                                                                'where sp.SEQUENCING_PROJECT_ID = ' ,
                              'Sample Contact Id':  'select sample_contact_id from  uss.dt_sequencing_project sp where sp.SEQUENCING_PROJECT_ID = ',
                              'Sequencing Project PI':  None,
                              'Sequencing Product Name':  'select  sequencing_product_name  from  uss.dt_sequencing_project sp '
                                                          'left join uss.dt_sequencing_product prod on sp.actual_sequencing_product_id=prod.SEQUENCING_PRODUCT_ID  '
                                                          'where SEQUENCING_PROJECT_ID = ',

                              'Material Category':  'select  MATERIAL_category from  uss.dt_sequencing_project sp '
                                                    'left join uss.DT_MATERIAL_TYPE_CV mat  on sp.material_type_id=mat.material_type_id '
                                                    'where sp.SEQUENCING_PROJECT_ID = ',
                              'Sequencing Project PI Contact Id':  'select SEQUENCING_PROJECT_CONTACT_ID from  uss.dt_sequencing_project sp '
                                                                   'where sp.SEQUENCING_PROJECT_ID = ',
                              'Sequencing Project Manager':  None,
                              'Scientific Program':  'select SCIENTIFIC_PROGRAM from  uss.dt_sequencing_project sp '
                                                     'left join uss.dt_account acct  on acct.ACCOUNT_ID = sp.DEFAULT_ACCOUNT_ID '
                                                     'left join uss.DT_SCIENTIFIC_PROGRAM_CV sci on acct.SCIENTIFIC_PROGRAM_ID = sci.SCIENTIFIC_PROGRAM_ID '
                                                     'where sp.SEQUENCING_PROJECT_ID = ',
                              'Proposal Id':  'select Proposal_ID from uss.dt_sequencing_project sp '
                                              'left join uss.DT_FINAL_DELIV_PROJECT fd  on fd.FINAL_DELIV_PROJECT_ID = sp.FINAL_DELIV_PROJECT_ID '
                                              'where sp.SEQUENCING_PROJECT_ID = '}
        # Dict that maps the ws Request attributes to the DB query that gets the value that populates the field
        # key = UDF name
        # value =  query string to get the value from DB, can be None
        #
        self.wsAttrDbQueryMapSP = {'final-deliverable-project-id':  'select final_deliv_project_id from  uss.dt_sequencing_project  '
                                                                  'where SEQUENCING_PROJECT_ID =',
                                'sequencing-product-id':          'select SEQUENCING_PRODUCT_ID from  uss.dt_sequencing_project  '
                                                                  'where SEQUENCING_PROJECT_ID =',
                                'sequencing-project-manager-cid': 'select SEQUENCING_PROJECT_MANAGER_ID from  uss.dt_sequencing_project  '
                                                                  'where SEQUENCING_PROJECT_ID =',
                                'sequencing-project-name':        'select SEQUENCING_PROJECT_NAME  from  uss.dt_sequencing_project  '
                                                                  'where SEQUENCING_PROJECT_ID =',
                                'sequencing-project-comments-and-notes': 'select SEQUENCING_PROJECT_COMMENTS from  uss.dt_sequencing_project  '
                                                                         'where SEQUENCING_PROJECT_ID =',
                                'existing-jgi-taxonomy-id':       'select TAXONOMY_INFO_ID from  uss.dt_sequencing_project  '
                                                                  'where SEQUENCING_PROJECT_ID =',
                                'expected-number-of-samples':     'select EXPECTED_NUM_SAMPLES from  uss.dt_sequencing_project  '
                                                                  'where SEQUENCING_PROJECT_ID =',
                                'eligible-for-public-release':    'select ELIGIBLE_PUBLIC_RELEASE from  uss.dt_sequencing_project  '
                                                                  'where SEQUENCING_PROJECT_ID =',
                                'embargo-days':                   'select EMBARGO_DAYS from  uss.dt_sequencing_project sp  '
                                                                  'left join uss.DT_EMBARGO_DAYS_CV em  on em.EMBARGO_DAYS_ID = sp.EMBARGO_DAYS_ID '
                                                                  'where sp.SEQUENCING_PROJECT_ID =  ',
                                'sequencing-project-contact-cid': 'select SEQUENCING_PROJECT_CONTACT_ID from  uss.dt_sequencing_project  '
                                                                  'where SEQUENCING_PROJECT_ID =',
                                'sample-contact-cid':             'select SAMPLE_CONTACT_ID from  uss.dt_sequencing_project  '
                                                                  'where SEQUENCING_PROJECT_ID =',
                                'auto-schedule-sow-items':        'select AUTO_SCHEDULE_SOW_ITEMS from  uss.dt_sequencing_project  '
                                                                  'where SEQUENCING_PROJECT_ID =',
                                'sequencing-strategy-id':         'select SEQUENCING_STRATEGY_ID from uss.dt_sequencing_project  '
                                                                  'where SEQUENCING_PROJECT_ID ='
                              }
        self.wsAttrDbQueryMapSow = {
                            'qc-type': 'select qc_type from  uss.dt_sow_item sow '
                                       'left join uss.dt_qc_type_CV qc on qc.qc_type_id = sow.qc_type_id '
                                       'where sow.sow_item_ID =  ',
                            'target-logical-amount': 'select target_logical_amount from  uss.dt_sow_item  '
                                                     'where sow_item_ID = ',
                            'logical-amount-units': 'select log.LOGICAL_AMOUNT_UNITS from  uss.dt_sow_item sow '
                                                    'left join uss.DT_LOGICAL_AMOUNT_UNITS_CV log '
                                                    'on log.LOGICAL_AMOUNT_UNITS_id = sow.LOGICAL_AMOUNT_UNITS_id '
                                                    'where sow.sow_item_ID =   ',
                            'degree-of-pooling': 'select deg.DEGREE_OF_POOLING from  uss.dt_sow_item sow '
                                                 'left join uss.DT_DEGREE_OF_POOLING_CV deg '
                                                 'on deg.DEGREE_OF_POOLING_ID = sow.DEGREE_OF_POOLING_ID '
                                                 'where sow.sow_item_ID =  ',
                            'run-mode': 'select RUN_MODE from  uss.dt_sow_item sow '
                                        'left join uss.DT_RUN_MODE_CV run on run.RUN_MODE_ID = sow.RUN_MODE_ID '
                                        'where sow.sow_item_ID =  ',

            }
        # Dict that maps the entity type with the query necessary to get the status from the DB
        # key = entity type ('sp', 'sow', 'sample'
        # value =  query string to get the status from DB, can be None
        #
        self.statusQueryMap = {'sp': "select status from uss.dt_sequencing_project sp "
                                       "left join uss.DT_SEQ_PROJECT_STATUS_CV cv on cv.status_id = sp.current_status_id "
                                       "where sp.SEQUENCING_PROJECT_ID = ",
                               'sow': "select status from  uss.dt_sow_item sow "
                                        "left join uss.DT_SOW_ITEM_STATUS_CV cv on cv.status_id = sow.current_status_id "
                                        "where sow.sow_item_ID = " ,
                               'sample': None
                               }

        # note:  need to consider multiple sows that are created for sp, sow status should be based on sow id.   Also some sps do not have sows

    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'claritydev1.jgi-psf.org'
    #
    def setURL(self,server):
        requestURL = 'http://' + server + '/sequencing-project-submission'
        return requestURL

    # -------------------------------------------------------------------------------------------
    # spSubmissionPost
    #
    # inputs: requestURL
    def spSubmissionPost(self,requestURL,mySubmissionAttribute):
        #  make sure that you will always have a unique SP name by adding time stamp to base name
        timeNow = ' {:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
        # get the base SP name
        # subAttributes = jsonInput.submissionAttributesMicrobialMinimalDraftIsolate

        spName = mySubmissionAttribute['sequencing-projects'][0] ['sequencing-project-name']
        # update the name with timestamp so it will be unique
        mySubmissionAttribute['sequencing-projects'][0]['sequencing-project-name'] = spName + timeNow
        print(mySubmissionAttribute['sequencing-projects'][0]['sequencing-project-name'])

        mydata = json.dumps(mySubmissionAttribute)
        headers = {"Content-Type": "application/json", 'data':mydata}
        # Call REST API
        response = requests.post(requestURL, data=mydata, headers=headers)  #POST  request
        status = response.status_code
        if status != 201:
            print("*** Error, Unexpected Returned Status = ", status)
            self.errorCnt +=  1
            prettyJson = json.loads(response.text)
            print("WS request status = " + str(status))
            print("response:" + json.dumps(prettyJson, indent=4, sort_keys=True))
        else:
            # Print Response
            prettyJson = json.loads(response.text)
            if self.printOn:
                print (json.dumps(prettyJson, indent=4, sort_keys=True))
            return prettyJson
    # -------------------------------------------------------------------------------------------
    # getUDFs
    #
    # inputs: spID,  type (i.e. 'project', 'sample', etc)
    # outputs: UDFs as a dict

    def getUDFs (self, spID, type):
        myUDFs = {}
        print ("server=", self.apiDevServer)
        url = self.myUDF.setURLproject(spID, self.apiDevServer)
        if url:
            if type=='project':
                projectUrl = self.myUDF.connectToClarityAPIprojects(url)
                if projectUrl:
                   myUDFs = self.myUDF.getProjectUDFs(projectUrl)
        return myUDFs


    # -------------------------------------------------------------------------------------------
    # verifyUDFsArePopulatedByDB
    # verify that the UDFS have the value stored in DB.
    #  input:  spUDF - the dict of UDFs for sp
    #
    # ouput:  updates errorCnt (global)

    def verifyUDFsArePopulatedByDB(self, spUDFs,spId):
        if spUDFs:
            for udf, query in self.UDFdbQueryMap.items():  #get each expected UDF and its query string
                print ("UDF= ",udf, "=", str(spUDFs[udf]))
                if query:
                    newQuery = query + str(spId)
                    dbValue = self.myDB.doQuery(newQuery)
                    print ("      DB value= ", dbValue)
                    if str(spUDFs[udf]) != str(dbValue):
                       print("***** UDF does not match DB !!!  UDF(" + udf + ")=" + str(spUDFs[udf]) + " !=  DB= " + str(dbValue))
                       self.errorCnt += 1
                    elif self.printOn:
                        print('      ' +udf + ':  values match DB')
                else:
                    print('      skipped')
        else:
            print("*** Error. UDFs not retrieved !!! ")
            self.errorCnt += 1

    # -------------------------------------------------------------------------------------------
    # verifyDBisPopulatedWithRequestedAttribues
    # verify that the new sequencing project was created with the values of the Request attributes. Check both SP and sow DB tables
    # input:
    #         wsRequest - the request Dict from WS call
    #         sp - the SPID to get data from DB
    # ouput:  updates errorCnt (global)

    def verifyDBisPopulatedWithRequestedAttribues(self, wsRequest, spID):
        if wsRequest:
            #print (wsRequest)
            #do SP fields
            print ("--------verify SP fields updated --------")
            for wsAttr, query in self.wsAttrDbQueryMapSP.items():  #check sp fields
                print("WS ATTR: ", wsAttr, "=", str(wsRequest[wsAttr]))
                if query:
                    if wsAttr in wsRequest:
                        newQuery = query + str(spID)
                        #print (newQuery)
                        dbValue = self.myDB.doQuery(newQuery)
                        print("     DB value= ", dbValue)
                        if str(wsRequest[wsAttr]) != str(dbValue):
                            print("     *** DB not updated!! !!!  WS attribute(" + wsAttr + ")=" +
                                  str(wsRequest[wsAttr]) + " !=  DB= " + str(dbValue))
                            self.errorCnt += 1
                        elif self.printOn:
                            print('     ' + wsAttr + ':  values match DB')
                    else:
                        print('    skipped')


            print("\n--------verify SP statuses updated--------\n")
            expectedStatus = 'Awaiting Collaborator Metadata'
            # check sp
            self.verifyStatus(spID, 'sp', expectedStatus)

            print("\n--------verify Sow fields updated--------")
            # do sow fields
            sowQuery = 'select sow_item_id from  uss.dt_sow_item  where SEQUENCING_PROJECT_ID = ' + str(spID)   #1062428
            sowList = self.myDB.doQueryGetAllRows(sowQuery)
            for sow in sowList:  # for each sow created for SP
                print ("SOW=" , sow)
                for wsAttr, query in self.wsAttrDbQueryMapSow.items():  # check sp fields
                     if query:
                         if wsAttr in wsRequest:
                            print("WS ATTR: ", wsAttr, "=", str(wsRequest[wsAttr]))
                            newQuery = query + str(sow)
                            print(newQuery)
                            dbValue = self.myDB.doQuery(newQuery)
                            print("     DB value= ", dbValue)
                            if str(wsRequest[wsAttr]) != str(dbValue):
                                print("     ***  Error.DB not updated!! !!!  WS attribute(" + wsAttr + ")=" +
                                      str(wsRequest[wsAttr]) + " !=  DB= " + str(dbValue))
                                self.errorCnt += 1
                            elif self.printOn:
                                print('     ' + wsAttr + ':  values match DB')
                         else:
                            print("No attribute in Request: ", wsAttr, " skipped")
                            # check sow(s)

                print("--------verify Sow status  for ", str(sow))
                self.verifyStatus(sow, 'sow', expectedStatus)
        else:
            print("*** Error. Unknown error - no request !!! ")
            self.errorCnt += 1

    # -------------------------------------------------------------------------------------------
    # verifyStatus
    # verify that the statuses are set correctly in the DB
    # input:
    #         itemID - the itemID to get data from DB, i.e. spID, sowitem id, sample id
    #         entity - the entity that you want the status of (i.e. "sp","sow","sample")
    #         expectedStatus - the expected status.  i.e. 'Awaiting Collaborator Metadata'
    # ouput:  updates errorCnt (global)
    def verifyStatus(self, itemID, entity,expectedStatus):

        #check status
        query = self.statusQueryMap[entity]  # get the query to use based on entity passed
        if query:
            newQuery = query + str(itemID)
            status = self.myDB.doQuery(newQuery)
            if status != expectedStatus:
                print("**** Error! " + entity + " is in wrong State: " + status)
                self.errorCnt += 1
            else:
                print('current-status(' + entity + ') is correct:' + status)



    # -------------------------------------------------------------------------------------------
    # getNewSPgeneric  - creates sp, verifies udfs and database, returns spid
    #
    # input:  server, selectedProduct (key=productID, value=product name)
    # ouput:  spID  of new SP created

    def getNewSPgeneric(self, server, selectedProduct, fd):
        #apiDevServer = 'clarity-dev01.jgi-psf.org'
        print("")
        print("--------start: get a sequencing product ---------")
        url = self.setURL(server)
        if self.printOn:
            print("url = " + url)

        if self.printOn:
            print("the response:")

        spJson = jsonInput()
        productName = ''
        productId = ''
        for k, v in selectedProduct.items():
            productName= v[0]
            strategyId = v[1]
            productId = k
            print ("product ID: ", productId, ", productName: ", productName, ", strategy ID: ", strategyId)

        subAttributes = spJson.spSubmissionAttributesGeneric
        spRequestAttributes =  subAttributes['sequencing-projects'][0]
        spRequestAttributes["sequencing-project-name"] = productName + "_viaPython_rjr_" + str(random.randint(1,1001))
        spRequestAttributes ["sequencing-product-id"] = int(productId)
        spRequestAttributes["sequencing-strategy-id"] = int(strategyId)
        spRequestAttributes["final-deliverable-project-id"] = fd    # clarity int = 1230298

        # for specify product types, add additional attributes
        if int(productId) in [37,44,54,57,58,65,66,67,68]:
            spRequestAttributes["qc-type"] = "smRNA"
        if int(productId) in [57,58,67,68]:
            spRequestAttributes["target-logical-amount"] = 10
            spRequestAttributes["degree-of-pooling"] = 2
            spRequestAttributes["run-mode"] = "Illumina HiSeq-1Tb 1 X 150"
            spRequestAttributes["target-logical-amount"] = 10
            spRequestAttributes["logical-amount-units"] = "M reads"

        subAttributes['sequencing-projects'][0] = spRequestAttributes
        #print("SP Submission URL: ", url)
        #pprint (subAttributes)
        wsResp = self.spSubmissionPost(url, subAttributes)  # call WS POST,response in dictionary form
        #pprint (wsResp)
        spList = wsResp["sequencing-projects"]
        spID = 0
        if len(spList) > 0:
            for spID in spList:  # get each  sp
                print("SP_ID = ", spID)    #, spList.index(spID))
                spAttrDict = subAttributes['sequencing-projects'][spList.index(spID)]
                if self.printOn:
                    print("--- the requested attributes:", str(spAttrDict))
                #print("\n--------verify UDFS--------\n")
                #spUDFs = self.getUDFs(spID, 'project')  # get the udfs, return as a dict with UDF name as key, UDF value as value
                # verify that the  UDFs are populated by correct DB values
                #self.verifyUDFsArePopulatedByDB(spUDFs, spID)

                # verify that the DB records were correctly created based on the WS request
                #print("\n--------verify database updated--------\n")
                #self.verifyDBisPopulatedWithRequestedAttribues(spAttrDict, spID)

        else:
            print("*** Error!! No SP was created!!! ***")
            self.errorCnt += 1

        return spID  # will return the last sp of list if more than one (fix this)



# -------------------------------------------------------------------------------------------

#use this with spSampleSubmissionEngine














