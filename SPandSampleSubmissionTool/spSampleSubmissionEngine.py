#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 10/26/17
# use this engine to create SPs and samples from existing FD.  This does not create APs/ATs.

import json
from pprint import pprint
import datetime
import time
import requests
import copy
#from SPandSampleSubmissionTool.seqProjSubmission import Stage_seqProjSubmissionWST
from SPandSampleSubmissionTool.seqProjSubmission import Stage_seqProjSubmissionWST
from Utils.util_dbTools import dbTools
from Utils.util_udfTools import udfTools
from Utils.util_jsonInput import jsonInput


# this will create SP and sample and submit data for both
# will verify that the database and udfs are properly populated.


class Stage_spSampleSubmission():

    maxProductNum = 20
    nextID = 1   # used for auto looping to submit sps/samples of all product  types
    spSampleCount = 0
    def __init__(self):
        self.printOn = True
        self.errorCnt = 0
        self.productNumber = 0
        self.printOn = False
        self.errorCnt = 0
        self.myDB = dbTools()
        self.myUDF = udfTools()
        self.sampleSubURL =  ''
        self.url = ''

        self.server = 'clarityint1.jgi-psf.org'
        self.apiDevServer = 'clarity-int01.jgi-psf.org'
        # these are all the attributes that can be updated on put command. Use this or subset
        self.submissionAttributes = {"submitted-by-cid": 5981,
                                     "samples":
                                         [
                                             {
                                                 # "sample-id": 293514,
                                                 # "sample-name": "sample293514",
                                                 "bio-sample-id": 1,
                                                 "risk-group-level": 1,
                                                 "pathogenicity": "something",
                                                 #"collaborator-concentration-ngul": 100.29,
                                                 #"collaborator-volume-ul": 54.78,
                                                 "absorbance-260-280": .63,
                                                 "absorbance-260-230": 0.5887899211,
                                                 # "tube-plate-label": "plate293514",
                                                 # "plate-location": "A2",
                                                 "sample-format": "Aqueous",
                                                 "storage-solution": "Water",
                                                 "shipment-storage-comments": "no comments",
                                                 "dnase-treated": "Y",
                                                 "starting-culture-axenic-strain-pure": "Y",
                                                 "purity-evidence": "qPCR",
                                                 "known-or-contaminating-organisms": "no known contamination",
                                                 "small-subunit-rna-sequence": "ACGTACGTCCTAATCC",
                                                 "spacer-sequence": "GGTACTTGCAACGTGATACGATCGAGA",
                                                 # ?"vector-name": "pbr32",
                                                 "biosafety-material-category": "Bacteria",
                                                 "sample-isolation-method": "ethanol extraction",
                                                 "sample-collection-date": "1998-02-27",
                                                 "latitude-of-sample-collection": 23.89,
                                                 "longitude-of-sample-collection": 32.67,
                                                 "altitude-or-depth-of-sample-collection": -23.99,
                                                 "collection-isolation-site-or-growth-conditions": "optimal temperature 20C",
                                                 "sample-isolated-from": "new England",
                                                 "sample-location": "Atlantic Ocean"
                                                 # "group-name": 1,
                                                 # "control-type(positive/negative)"  for single cell only
                                                 # "control-organism-name"  for single cell only
                                                 # "control-organism-tax-id" for single cell only
                                                 # "pool-number" for single cell only
                                                 # "destination-container-name" for single cell only
                                                 # "destination-container-location" for internal single cell
                                                 # "internal-collaborator-sample-name"for internal single cell
                                                 # "source-container-barcode" for internal single cell

                                             },

                                         ]
                                     }
        self.ribosomalSeqOne = "TNAGCTNAGCTNAGCTNAGTNAGCTNAGCTNAGCTNAGCTNAGCTCAGCCTCAGCTCAGCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTCAGCTC"

        # Dict that maps UDF to the DB query that gets the value that populates the field
        # key = UDF name
        # value =  query string to get the value from DB, can be None
        #
        self.UDFdbQueryMap = {
            'Collaborator Concentration (ng/ul)': 'select collabor_concentration_ngul from uss.dt_sample where SAMPLE_ID =',
            'Format': 'select cvTable.SAMPLE_FORMAT from uss.dt_sample sam '
                      'left join uss.DT_SAMPLE_FORMAT_CV cvTable on cvTable.SAMPLE_FORMAT_ID = sam.SAMPLE_FORMAT_ID '
                      'where SAMPLE_ID =  ',
            'Biosafety Material Category': 'select cvTable.BIOSAFETY_MATERIAL_CATEGORY from uss.dt_sample sam '
                                           'left join uss.DT_BIOSAFETY_MATERIAL_CATEG_CV cvTable '
                                           'on cvTable.BIOSAFETY_MATERIAL_CATEGORY_ID = sam.BIOSAFETY_MATERIAL_CATEGORY_ID '
                                           'where SAMPLE_ID = ',
            'Estimated Genome Size (mb)': 'select tax.GENOME_SIZE_ESTIMATED_MB from uss.dt_sequencing_project sp '
                                          'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id '
                                          'left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id '
                                          'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id '
                                          'left join uss.DT_TAXONOMY_INFO tax on tax.TAXONOMY_INFO_ID = sp.TAXONOMY_INFO_ID '
                                          'where sam.SAMPLE_ID = ',
            'Collaborator Volume (ul)': 'select collabor_volume_ul from uss.dt_sample where SAMPLE_ID = ',
            'Stop at Receipt': "SELECT CASE WHEN MAX(s.SAMPLE_ID) IS NULL THEN 'false' ELSE 'true' END "  # returns true if related sow type id is 5,6
                               "FROM  uss.dt_sample s,uss.dt_sow_item sow, USS.DT_M2M_SAMPLESOWITEM ssow  "
                               "WHERE s.SAMPLE_ID=ssow.SAMPLE_ID and ssow.SOW_ITEM_ID=sow.sow_item_id and "
                               "sow.SOW_ITEM_TYPE_ID in (5,6) and s.SAMPLE_ID=",
            'Collaborator Sample Name': 'select SAMPLE_NAME from uss.dt_sample where SAMPLE_ID = ',
        }
        # Dict that maps the ws Request attributes to the DB query that gets the value that populates the field
        # key = UDF name
        # value =  query string to get the value from DB, can be None
        #
        self.wsAttrDbQueryMap = {"sample-id": 'select SAMPLE_ID from uss.dt_sample where SAMPLE_ID = ',
                                 "sample-name": 'select SAMPLE_NAME from uss.dt_sample where SAMPLE_ID = ',
                                 "bio-sample-id": 'select BIO_SAMPLE_ID from uss.dt_sample where SAMPLE_ID = ',
                                 "risk-group-level": 'select RISK_GROUP_LEVEL from uss.dt_sample where SAMPLE_ID = ',
                                 "pathogenicity": 'select PATHOGENICITY from uss.dt_sample where SAMPLE_ID = ',
                                 "collaborator-concentration-ngul": 'select COLLABOR_CONCENTRATION_NGUL from uss.dt_sample where SAMPLE_ID = ',
                                 "collaborator-volume-ul": 'select COLLABOR_VOLUME_UL from uss.dt_sample where SAMPLE_ID = ',
                                 "absorbance-260-280": 'select ABSORBANCE_260_280 from uss.dt_sample where SAMPLE_ID = ',
                                 "absorbance-260-230": 'select ABSORBANCE_260_230 from uss.dt_sample where SAMPLE_ID = ',
                                 "tube-plate-label": 'select TUBE_PLATE_LABEL from uss.dt_sample where SAMPLE_ID = ',
                                 "plate-location": 'select PLATE_LOCATION from uss.dt_sample where SAMPLE_ID = ',
                                 "sample-format": 'select cvTable.SAMPLE_FORMAT from uss.dt_sample sam '
                                                  'left join uss.DT_SAMPLE_FORMAT_CV cvTable on cvTable.SAMPLE_FORMAT_ID = sam.SAMPLE_FORMAT_ID where SAMPLE_ID =  ',
                                 "storage-solution": 'select cvTable.storage_solution from uss.dt_sample sam '
                                                     'left join uss.DT_storage_solution_CV cvTable on cvTable.storage_solution_ID = sam.storage_solution_ID where SAMPLE_ID = ',
                                 "shipment-storage-comments": 'select SHIPMENT_STORAGE_COMMENTS from uss.dt_sample where SAMPLE_ID = ',
                                 "dnase-treated": 'select DNA_TREATED from uss.dt_sample where SAMPLE_ID = ',
                                 "starting-culture-axenic-strain-pure": 'select STARTING_CULTURE_PURE from uss.dt_sample where SAMPLE_ID = ',
                                 "purity-evidence": 'select PURITY_EVIDENCE from uss.dt_sample where SAMPLE_ID = ',
                                 "known-or-contaminating-organisms": 'select KNOWN_OR_CONTAM_ORGANISMS from uss.dt_sample where SAMPLE_ID = ',
                                 "small-subunit-rna-sequence": 'select SMALL_SUBUNIT_RRNA_SEQ from uss.dt_sample where SAMPLE_ID = ',
                                 "spacer-sequence": 'select INTERN_TRANSCR_SPACER_SEQ from uss.dt_sample where SAMPLE_ID = ',
                                 # ?"vector-name": 'select VECTOR_NAME from uss.dt_sample where SAMPLE_ID = ',
                                 "biosafety-material-category": 'select cvTable.BIOSAFETY_MATERIAL_CATEGORY from uss.dt_sample sam '
                                                                'left join uss.DT_BIOSAFETY_MATERIAL_CATEG_CV cvTable on cvTable.BIOSAFETY_MATERIAL_CATEGORY_ID = sam.BIOSAFETY_MATERIAL_CATEGORY_ID '
                                                                'where SAMPLE_ID = ',
                                 "sample-isolation-method": 'select DNA_ISOLATION_METHOD from uss.dt_sample where SAMPLE_ID = ',
                                 "sample-collection-date": 'select COLLECTION_TIME from uss.dt_sample where SAMPLE_ID = ',
                                 "latitude-of-sample-collection": 'select COLLECTION_LATITUDE from uss.dt_sample where SAMPLE_ID = ',
                                 "longitude-of-sample-collection": 'select COLLECTION_LONGITUDE from uss.dt_sample where SAMPLE_ID = ',
                                 "altitude-or-depth-of-sample-collection": 'select COLLECTION_ALTIT_DEPTH_METERS from uss.dt_sample where SAMPLE_ID = ',
                                 "collection-isolation-site-or-growth-conditions": 'select COLLECTION_SITE_GROWTH from uss.dt_sample where SAMPLE_ID = ',
                                 "sample-isolated-from": 'select SAMPLE_ISOLATED_FROM from uss.dt_sample where SAMPLE_ID = ',
                                 "group-name": 'select GROUP_NAME from uss.dt_sample where SAMPLE_ID = ',
                                 "control-type(positive/negative)": 'select CONTROL_TYPE from uss.dt_sample where SAMPLE_ID = ',
                                 "control-organism-name": 'select CONTROL_ORGANISM_NAME from uss.dt_sample where SAMPLE_ID = ',
                                 "control-organism-tax-id": 'select CONTROL_ORGANISM_TAX_ID from uss.dt_sample where SAMPLE_ID = ',
                                 "pool-number": 'select POOL_NUMBER from uss.dt_sample where SAMPLE_ID = ',
                                 "destination-container-name": 'select DESTINATION_CONTAINER_NAME from uss.dt_sample where SAMPLE_ID = ',
                                 "destination-container-location": 'select DESTINATION_CONTAINER_LOCATION from uss.dt_sample where SAMPLE_ID = ',
                                 "internal-collaborator-sample-name": 'select INTERNAL_COLLABOR_SAMPLE_NAME from uss.dt_sample where SAMPLE_ID = ',
                                 "source-container-barcode": 'select CONTAINER_IDENTIFIER from uss.dt_sample where SAMPLE_ID = '
                                 }

        self.devServer = {
            "url": 'claritydev1.jgi-psf.org',
            "fd": 1226772,
            "sampleSubUrl" : '/pps-1/v2/sample-submission',
            "name": 'dev'
        }

        self.intServer = {
            "url": 'clarityint1.jgi-psf.org',
            "fd" : 1226772,
            "sampleSubUrl": '/pps-1/v2/sample-submission',
            "name": 'int'

        }
        self.prdServer = {
            "url": 'clarityprd1.jgi-psf.org',
            "sampleSubUrl": '/pps-1/v2/sample-submission',
            "fd": 1226772,
            "name": 'prod'

        }

        self.virtualServer = {
            "url": 'clarity-prd01.jgi.doe.gov:60966',
            "fd" : 1091346,
            "name": 'virtual',
            "sampleSubUrl" : '/v2/sample-submission'
        }

    # -------------------------------------------------------------------------------------------
    # makeSample_main
    # creates SP and sample, submits data to sample
    # inputs:fd,
    # action = tubesOnly, menu, platesOnly
    #
    # -------------------------------------------------------------------------------------------


    def makeSample_main(self,fd,action):
        # creates a sample for clarity  by first creating sp (checks udf and db),
        # then submitting data for sample (checks udf and db)
        # user can make several automatically
        self.myDB.connect(self.serverName)

        if (action=="menu"):
            self.selectedProduct = self.getProductName('pickOne')
            for key in self.selectedProduct:
                self.productNumber = int(key)


            if len(self.selectedProduct.keys()) == 1:   #got a valid selection key = productid, value = productname

                # get the stategy of  selectedProduct
                valuelist = list(self.selectedProduct.values())
                strategyIDstr = valuelist[0][1]
                strategyID = int(strategyIDstr)
                print (strategyID)

                #check stategy table if group name is required for sample -- to do

                yesNo = "n"   #input("Do you want to input jSon file for submission attributes? y/n")
                if yesNo != 'n':
                    default_jsonFile = r"C:\temp\myJson_req4all.json"
                    jsonFile = "" #input("Filename(" + default_jsonFile  +"): ")
                    if jsonFile == "":
                        jsonFile= default_jsonFile
                    try:
                        self.submissionAttributes = json.load(open(jsonFile))
                    except ValueError as detail:
                        print ('Error within file:', detail)
                        print("****ERROR - can't process  file !", jsonFile)
                        exit()
                    except IOError as e:
                        print ("I/O error({0}): {1}".format(e.errno, e.strerror))
                        print ("****ERROR - can't open file !", jsonFile)
                        exit()
                        #pass
                else:  # get from jsonInput
                    samJson = jsonInput()
                    self.submissionAttributes = samJson.samSubmissionAttributesGeneric

                #pprint(self.submissionAttributes)
                self.submitSPandSample(fd)

        elif (action=="tubesOnly"):
            self.submitProductsTubesOnly(fd)



        print(" ")
        print("---Number of Errors Found = " + str(self.errorCnt) + " ---")

    # -------------------------------------------------------------------------------------------
    # submitSPandSample
    # creates SP and sample, submits data to sample
    # inputs:  selected Product
    #
    # -------------------------------------------------------------------------------------------

    def submitSPandSample(self, fd):

        spSubmit = Stage_seqProjSubmissionWST()
        selectedProduct = self.selectedProduct   #set this as class attribute
        spID = spSubmit.getNewSPgeneric(self.server, selectedProduct, fd)  #create new sp of specific product type

        # get the sample created using query
        query = 'select sam.SAMPLE_ID from uss.dt_sequencing_project sp ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id ' \
                'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id ' \
                'where sp.SEQUENCING_PROJECT_ID = '

        query = query + str(spID)
        sampleID = self.myDB.doQuery(query)
        print('\nthe sample obtained is: ', sampleID)
        if (sampleID == 'None'):
            print('*** Warning - no Sample created for this product')
        else:
            sampleName = 'sample' + str(sampleID)
            #need to update submission json with groupname?

            #get strategy based on sp product id to find if group is required
            errs = self.submitAsample(sampleID, sampleName)

            print('\nthe sample  is: ', sampleID)
            print ("!!!!! Errors, so far = ", errs)

    # -------------------------------------------------------------------------------------------
    # submitProductsFromUserInput
    # asks user which products to create and submit samples for
    #
    #
    # -------------------------------------------------------------------------------------------

    def submitProductsFromUserInput(self):
        errs = 0
        done = False
        startProdID = input("What product ID do you want to start with?:")
        self.nextID = int(startProdID)
        stopProdID = input("What product ID do you want to end with?:")
        self.maxProductNum = int(stopProdID)
        count = 0
        while not done:
            selectedProduct = self.getProductName('pickRange')
            if len(selectedProduct.keys()) == 1:
                print("\n !=========!Start of SP and Sample submission for : ", selectedProduct, "!=========!")
                self.selectedProduct = selectedProduct  # set this as class attribute
                self.submitSPandSample()
                count += 1
            self.nextID += 1
            if self.nextID > self.maxProductNum:
                done = True
        print("========= Number of SPs /samples created:  " + str(count) + " ===============")

    # -------------------------------------------------------------------------------------------
    # submitProductsFromUserInput
    # asks user which products to create and submit samples for
    #
    #
    # -------------------------------------------------------------------------------------------

    def submitProductsTubesOnly(self,fd):
        errs = 0
        done = False
        productListForTubes = [1,3,6,9,10,14,15,16,21,23,24,25,39,40,41,42,48,49,58,60,61,64,68,69,70,71,74,75,78,79,80,81,82,83,84]
        count = 0
        for nextId in productListForTubes:
            self.nextID = nextId
            selectedProduct = self.getProductName('pickRange')
            if len(selectedProduct.keys()) == 1:
                print("\n !=========!Tubes ONLY, Start of SP and Sample submission for : ", selectedProduct, "!=========!")
                self.selectedProduct = selectedProduct  # set this as class attribute
                for key in self.selectedProduct:
                    self.productNumber = int(key)
                # get the stategy of  selectedProduct
                valuelist = list(self.selectedProduct.values())
                strategyIDstr = valuelist[0][1]
                strategyID = int(strategyIDstr)
                #print(strategyID)
                samJson = jsonInput()
                self.submissionAttributes = samJson.samSubmissionAttributesGeneric
                self.submitSPandSample(fd)
                count += 1

        self.spSampleCount += count
        print("========= Number of SPs /samples created:  " + str(count) + " ===============")
        print ("total so far = ", self.spSampleCount)






    # -------------------------------------------------------------------------------------------
    # setSampleSubURL
    # build the URL to for sample submission request
    #
    #------------------------------------------------------------------------------------------

    def setSampleSubURL(self):
        requestURL = 'http://' + self.server + self.sampleSubURL
        return requestURL


    # ------------------------------------------------------------------------------------------
    # getProductName
    # input:  runtype  = 'pickOne'  or  'pickRange'
    #     if  'pickOne'  - user picks one from menu
    #     if  'pickRange'   -  user picks a range of product ids to use to create SPs/samples
    # output:   array of [productid,productname]
    #------------------------------------------------------------------------------------------

    def getProductName(self,runType):
        #get active sequencing product names from db

        selectedProduct = {}
        query = "select SEQUENCING_PRODUCT_ID,SEQUENCING_PRODUCT_NAME,DEFAULT_SEQUENCING_STRATEGY_ID  from uss.DT_SEQUENCING_PRODUCT where active= 'Y'"
        productList = self.myDB.doQueryGetAllRowsCols(query)

        idList = []  #get the list of product ids
        for id in productList.keys():
            x = int(id)
            idList.append(x)

        idListSorted = sorted(idList)  #put in sorted list for easier viewing when displayed

        self.maxProductNum = idListSorted[-1]  #set the max product number

        if runType == 'pickOne':
            print("product names:")
            for x in idListSorted:
                key = str(x)
                if key in productList:
                    print(key + ": " ,productList[key])

            answer = input("Which Product would you like to create? \n-------> Select #: ")
            if answer in productList:
                selectedProduct[answer] = productList[answer]  #store product id, product name in dict to return
                print("\nSelection: product ID= " ,answer , ": " ,selectedProduct[answer])
            else:
                print("\n*** ERROR *** Invalid Selection: " + answer + " - No active product with that ID")
                self.errorCnt += 1

        else: #runType == 'pickRange':  # or a specific list (likefor tubes only)
            next = str(self.nextID)
            if next in productList:
                selectedProduct[next] = productList[next]  # store product id, product name in dict to return
                print("\nSelection: " + next + ": " ,selectedProduct[next])
            else:
                print("\n*** ERROR *** Invalid Selection: " + next + " - No active product with that ID")
                self.errorCnt += 1


        return selectedProduct



    # -------------------------------------------------------------------------------------------
    # sampleSubmissionPost
    #
    # inputs: requestURL

    def sampleSubmissionPost(self, requestURL):
        # need to check sample to make sure if it is a plate or tube, use query
        # if plate, need to add plate loc
        #pprint(self.submissionAttributes)
        mydata = json.dumps(self.submissionAttributes)
        headers = {"Content-Type": "application/json", 'data': mydata}
        # Call REST API
        print ("Sample Submission URL: ", requestURL)
        response = requests.post(requestURL, data=mydata, headers=headers)  # POST  request
        status = response.status_code
        if status != 200:
                print("*** Error, Unexpected Returned Status = ", status)
                self.errorCnt += 1
                prettyJson = json.loads(response.text)
                print("WS request status = " + str(status))
                print("response:" + json.dumps(prettyJson, indent=4, sort_keys=True))
        else:
                # Print Response
                prettyJson = json.loads(response.text)
                if self.printOn:
                    print(json.dumps(prettyJson, indent=4, sort_keys=True))


        return status
    # -------------------------------------------------------------------------------------------
    # getUDFs
    #
    # inputs: sampleId,
    # outputs: UDFs as a dict

    def getUDFs(self, sampleId):
        myUDFs = {}
        url = self.myUDF.setURLsample(sampleId, self.apiDevServer)
        print("sample base URL = ", url)
        if url:
            sampleUrl = self.myUDF.connectToClarityAPIsamples(url)
            print("sample  URL = ", sampleUrl)
            if sampleUrl:
                myUDFs = self.myUDF.getSampleUDFs(sampleUrl)
                #print("udfs retrieved:")
                #print(myUDFs)

        return myUDFs

    # -------------------------------------------------------------------------------------------
    # verifySampleUDFsArePopulatedByDB
    # verify that the UDFS have the value stored in DB.
    #  input:  UDFs - the dict of UDFs for sample
    #
    # ouput:  updates errorCnt (global)

    def verifySampleUDFsArePopulatedByDB(self, myUDFs, sampleId):
        if myUDFs:
             for udf, query in self.UDFdbQueryMap.items():
                #print("UDF: ",udf, "=", str(myUDFs[udf]))
                if (udf in myUDFs) and query:
                    newQuery = query + str(sampleId)
                    dbValue = self.myDB.doQuery(newQuery)
                    print("      DB value= ", dbValue)
                    if str(myUDFs[udf]) != str(dbValue):
                        print("*** UDF does not match DB !!!  UDF(" + udf + ")=" + str(myUDFs[udf]) + " !=  DB= " + str(
                            dbValue))
                        self.errorCnt += 1
                    elif self.printOn:
                        print(udf + ':  values match DB')
        else:
            print("*** Error. UDFs not retrieved !!! ")
            self.errorCnt += 1

    # -------------------------------------------------------------------------------------------
    # verifyDBisPopulatedWithRequestedAttribues
    # verify that the new sample was created with the values of the Request attributes.
    # input:
    #         wsRequest - the request Dict from WS call
    #         sampleID - the sample ID to get data from DB
    # ouput:  updates errorCnt (global)

    def verifyDBisPopulatedWithRequestedAttribues(self, wsRequest, sampleID):
        if wsRequest:
            #myDB = dbTools()
            for wsAttr, query in self.wsAttrDbQueryMap.items():
                print('----', wsAttr, '----')
                if query and wsAttr in wsRequest:
                    newQuery = query + str(sampleID)
                    dbValue = self.myDB.doQuery(newQuery)
                    print("     DB value= ", dbValue, " Request value= ", str(wsRequest[wsAttr]))
                    if str(wsRequest[wsAttr]) != str(dbValue):
                        roundedNumbersMatch = self.checkRoundedNumbers(wsAttr, wsRequest[wsAttr], dbValue)
                        if not roundedNumbersMatch:
                            dateMatch = self.checkTruncatedDate(wsAttr, wsRequest[wsAttr], dbValue)
                            if not dateMatch:
                                print("***ERROR!  DB and Request do not match!! !!!  WS attribute(" + wsAttr + ")=" + str(
                                    wsRequest[wsAttr]) + " !=  DB= " + str(
                                    dbValue))
                                self.errorCnt += 1
                    elif self.printOn:
                        print('    ' + wsAttr + ':  values match DB')
                else:
                    print("*** Warning. field not found in wsRequest  !!! ")
                    # self.errorCnt += 1
        else:
            print("*** Error. WSrequest is null !!! ")
            self.errorCnt += 1

    # -------------------------------------------------------------------------------------------
    # checkRoundedNumbers
    # compare two numbers after rounded up.  return true if they match
    # input:
    #         attribute - only some attributes are number fields, 1st check if this is one of them
    #         number1
    #         number2
    #
    # ouput:  boolean :  true if two numbers match after rounded

    def checkRoundedNumbers(self, attr, number1, number2):
        match = False
        if attr in ['latitude-of-sample-collection', 'longitude-of-sample-collection', 'absorbance-260-280',
                    'absorbance-260-230', 'collaborator-concentration-ngul', 'collaborator-volume-ul',
                    'altitude-or-depth-of-sample-collection']:

            num1 = round(float(number1), 1)
            num2 = round(float(number2), 1)

            if num1 == num2:
                print("     matches after rounded ", num1, num2)
                match = True
            else:
                print("*** Error. no match", num1, num2)
                self.errorCnt += 1
                match = False
        return match

    # -------------------------------------------------------------------------------------------
    # checkTruncatedDate
    # compare two date after truncating time piece.  return true if they match
    # input:
    #         attribute - only some attributes are date fields, 1st check if this is one of them
    #         number1
    #         number2
    #
    # ouput:  boolean :  true if two numbers match after rounded

    def checkTruncatedDate(self, attr, date1, date2):
        match = False
        #print("checking date fields")
        if attr in ['sample-collection-date']:
            print(attr, date1, date2," match!" )
            match = True

        return match


    # -------------------------------------------------------------------------------------------
    # submitAsample
    #
    # input:  sampleID, samplename
    # ouput:  err - number of errors found

    def submitAsample(self, sampleID, sampleName):
        print("")
        print("--------start: create sample submission WST tests---------")
        url = self.setSampleSubURL()
        if self.printOn:
            print("url = " + url)

        self.submissionAttributes['samples'][0]['sample-name'] = sampleName
        self.submissionAttributes['samples'][0]['tube-plate-label'] = 'label' + str(sampleID)
        self.submissionAttributes['samples'][0]['sample-id'] = int(sampleID)

        # find a sample that needs submitting


        # determine if it can be plate or tube, this query return 'plate' if only can be plate
        query = "SELECT CASE WHEN MAX(lcq.TUBE_TARGET_VOL_LIB_TRIAL_UL) IS NOT NULL AND  MAX(lcq.PLATE_TARGET_VOL_LIB_TRIAL_UL) IS  NOT NULL " \
                "THEN 'tubeORplate' " \
                "ELSE CASE WHEN MAX(lcq.TUBE_TARGET_VOL_LIB_TRIAL_UL) IS NOT NULL THEN 'tube' ELSE 'plate' " \
                "    END " \
                "END " \
                "FROM uss.dt_sample sam " \
                "left join uss.dt_m2m_samplesowitem m2sam on m2sam.sample_id = sam.sample_id " \
                "left join uss.dt_sow_item sow on sow.sow_item_id = m2sam.sow_item_id " \
                "left join uss.DT_M2M_LC_QUEUE_SPECS  m2mlq on  m2mlq.LIBRARY_CREATION_SPECS_ID = sow.LIBRARY_CREATION_SPECS_ID " \
                "left join uss.DT_LIBRARY_CREATION_QUEUE_CV lcq on m2mlq.LIBRARY_CREATION_QUEUE_ID = lcq.LIBRARY_CREATION_QUEUE_ID " \
                " WHERE sam.sample_id in " + str(sampleID)

        # myDB = dbTools()
        tubeData = self.myDB.doQuery(query)

        if tubeData == 'plate':  # null returned in field TUBE_TARGET_VOL_LIB_TRIAL_UL means it is not a tube
            print("this is plate")
            # add plate location
            self.submissionAttributes['samples'][0]['plate-location'] = "B1"
        else:
            print("this is a tube")

        self.check4specialRequiredAttibutes()

        status = self.sampleSubmissionPost(url)  # call WS POST,response in dictionary form
        #print("These are the sample attributes submitted: ")
        #pprint(self.submissionAttributes)
        print("status of sampleSubmission = ",status)
        #if status == 200:  # sample submitted, now verify data
            # verify database has been updated
            #print("\n=====verify database has been updated======  skip\n")
            #self.verifyDBisPopulatedWithRequestedAttribues(self.submissionAttributes['samples'][0], sampleID)

            # verify udfs have been updated
            #print("\n=====verify sample UDFs has been updated======  skip\n")
            #myUDFs = self.getUDFs(sampleID)  # get the udfs, return as a dict with UDF name as key, UDF value as value
            # verify that the  UDFs are populated by DB values
            #self.verifySampleUDFsArePopulatedByDB(myUDFs, sampleID)

            # self.verifyDBisPopulatedWithRequestedAttribues(self.submissionAttributes['samples'][0], sampleID)

        return self.errorCnt


    # -------------------------------------------------------------------------------------------
    # check4specialRequiredAttibutes
    #  based on product type, some additional attributes are required for submission
    #  this method will add those attributes
    # input:  self.submissionAttributes, self.productID
    # ouput:  self.errorCnt

    def check4specialRequiredAttibutes(self):

        if self.productNumber in [1,2,3]:
            self.submissionAttributes['samples'][0]['ribosomal-seq-one'] = self.ribosomalSeqOne
            self.submissionAttributes['samples'][0]['ribosomal-seq-one-type'] = "16S"
            self.submissionAttributes['samples'][0]['ribosomal-seq-one-comment'] = "prod 1,2,3 test"
        if self.productNumber in [2,14,16,18,15,62,63,73]:
            self.submissionAttributes['samples'][0]['altitude-or-depth-of-sample-collection'] = 104
            self.submissionAttributes['samples'][0]['location-of-sample-collection'] = "Tunisia"
            self.submissionAttributes['samples'][0]['latitude-of-sample-collection'] = 10
            self.submissionAttributes['samples'][0]['longitude-of-sample-collection'] = 101
        if self.productNumber in [9,10,70]:
            self.submissionAttributes['samples'][0]['ribosomal-seq-one'] = self.ribosomalSeqOne
            self.submissionAttributes['samples'][0]['ribosomal-seq-one-type'] = "Other"
            self.submissionAttributes['samples'][0]['ribosomal-seq-one-comment'] = "prod 9,10 test"
        if self.productNumber in [3,10,16,42,70]:
           self.submissionAttributes['samples'][0]['absorbance-260-230'] = 1
           self.submissionAttributes['samples'][0]['absorbance-260-280'] = 8888
        if self.productNumber in [29,31,32,33,34,42,44,45,49,50,51,52,54,57,60,61,62,65,67,72,76,77]:
           self.submissionAttributes['samples'][0]['group-name'] = "myGroup"
           self.submissionAttributes['samples'][0]['collection-isolation-site-or-growth-conditions'] = "very tall"
        if self.productNumber in [63,71]:
           self.submissionAttributes['samples'][0]['collection-isolation-site-or-growth-conditions'] = "very short"





# -------------------------------------------------------------------------------------------
# run tests
start_time = time.time()
print("Start Time: " ,start_time)
myTest = Stage_spSampleSubmission()
# tester:  you must set up the testServer by copying the correct data.
# 1. set the testServer to copy the server you are testing
# 2. choose the sp to use from the list in the servers dictionary.
# set up data based on server using
testServer = copy.deepcopy(myTest.intServer)  #choose the server to use
myTest.server = testServer["url"]
myTest.serverName = testServer["name"]
myTest.FD = testServer["fd"]
myTest.sampleSubURL = testServer["sampleSubUrl"]

print("Create SP and submit Sample, using: " + myTest.serverName)
print("-------------------------------------------\n")
#for action- use "menu" to choose 1 SP product, choose "tubesOnly" to create a sp/sample for every SP product for tubes
action = "tubesOnly" #"menu" #""tubesOnly"
for x in range(0,10):
    myTest.makeSample_main(myTest.FD, action)
    print("Total SPs/Samples created so far = ", myTest.spSampleCount)

print ("Total SPs/Samples created = ", myTest.spSampleCount)
print("---Number of Errors Found = " + str(myTest.errorCnt) + " ---")
elapsed_time = time.time() - start_time
print ("Elasped Time (H:M:S): ", time.strftime("%H:%M:%S", time.gmtime(elapsed_time)))
#print("Elasped Time: ",elapsed_time)
